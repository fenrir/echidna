package org.fenrir.echidna.youtube.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import com.google.gdata.client.Query;
import com.google.gdata.client.youtube.YouTubeQuery;
import com.google.gdata.client.youtube.YouTubeService;
import com.google.gdata.client.youtube.YouTubeQuery.SafeSearch;
import com.google.gdata.data.youtube.VideoEntry;
import com.google.gdata.data.youtube.VideoFeed;
import com.google.gdata.data.youtube.YouTubeNamespace;
import com.google.gdata.model.atom.Category;
import com.google.gdata.util.ServiceException;
import org.fenrir.echidna.common.service.ILinkSearchService;
import org.fenrir.echidna.common.dto.GenericResultSetDTO;
import org.fenrir.echidna.common.dto.ILinkDTO;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.youtube.dto.LinkDTOAdapter;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120817
 */
public class YoutubeSearchServiceImpl implements ILinkSearchService 
{
	public GenericResultSetDTO<ILinkDTO> findLinks(String term) throws BussinessException
	{
		return findLinks(term, null);
	}
	
	/**
	 * TODO Posar constants
	 * @param term
	 * @param categoryFilter
	 * @return
	 * @throws BussinessException
	 */
	protected GenericResultSetDTO<ILinkDTO> findLinks(String term, Query.CategoryFilter categoryFilter) throws BussinessException
	{
		YouTubeService youtubeService = new YouTubeService("Hydra");
		
		try{
			YouTubeQuery query = new YouTubeQuery(new URL("http://gdata.youtube.com/feeds/api/videos"));
			// Es restringeix la cerca a videos que es puguin incrustar en un reproductor extern (format 5)
			query.setFormats(5);
			// Ordenació dels resultats per relevancia
			query.setOrderBy(YouTubeQuery.OrderBy.RELEVANCE);
			// No s'excloure dels resultats el contingut restringit			
			query.setSafeSearch(SafeSearch.NONE);
			// Query
			String parsedTerm = term.replaceAll("\"", "");
			query.setFullTextQuery(parsedTerm);
			// a category filter holds a collection of categories to limit the search
			if(categoryFilter!=null){
				query.addCategoryFilter(categoryFilter);
			}
	
			List<ILinkDTO> vLinks = new ArrayList<ILinkDTO>();
			VideoFeed videoFeed = youtubeService.query(query, VideoFeed.class);
			for(VideoEntry entry:videoFeed.getEntries()){
				vLinks.add(new LinkDTOAdapter(entry));
			}
			
			return new GenericResultSetDTO<ILinkDTO>(vLinks, videoFeed.getTotalResults(), videoFeed.getStartIndex());			
		}
		catch(MalformedURLException e){
			throw new BussinessException("Error en la url de connexió a YouTube: " + e.getMessage(), e);
		}
		catch(ServiceException e){
			throw new BussinessException("Error en obtenir els resultats de la cerca a YouTube: " + e.getMessage(), e);
		}
		catch(IOException e){
			throw new BussinessException("Error en obtenir els resultats de la cerca a YouTube: " + e.getMessage(), e);
		}
	}
	
	/**
	 * @param groupName
	 * @param songName
	 * @throws Exception
	 */
	public GenericResultSetDTO<ILinkDTO> findSongLinks(String groupName, String songName) throws BussinessException
	{
		String query = groupName + " " + songName;		
		/* Restricció dels resultats a la categoria "Music
		 * Els filtres s'encadenen en condicions "AND"
		 */
		Query.CategoryFilter categoryFilter = new Query.CategoryFilter();
		categoryFilter.addCategory(new Category(YouTubeNamespace.CATEGORY_SCHEME, "Music"));
		return findLinks(query, categoryFilter);
	}
}
