package org.fenrir.echidna.youtube.dto;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.google.gdata.data.youtube.VideoEntry;
import org.fenrir.echidna.common.dto.ILinkDTO;
import org.fenrir.echidna.youtube.YoutubeConstants;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120911
 */
public class LinkDTOAdapter implements ILinkDTO 
{
	private final String THUMBNAIL_URL_BASE = "http://img.youtube.com/vi/";
	private final String THUMBNAIL_URL_DEFAULT_SUFFIX = "/default.jpg";
	
	private VideoEntry entry;
		
	public LinkDTOAdapter(VideoEntry entry)
	{
		this.entry = entry;
	}
		
	public String getId()
	{
		return getLinkSearchId();
	}
	
	public String getUrl()
	{
		return entry.getHtmlLink().getHref();
	}
	
	public String getProvider() 
	{
		return YoutubeConstants.CONTENT_PROVIDER_YOUTUBE;
	}
	
	public String getLinkSearchId()
	{
		String url = getUrl();
		Pattern pattern = Pattern.compile("http://www.youtube.com/watch\\?v=(.*)&feature=youtube_gdata");
		Matcher matcher = pattern.matcher(url);
		if(matcher.find()){
			return matcher.group(1);
		}
		
		return null;
	}
		
	public String getTitle()
	{
		return entry.getTitle().getPlainText();
	}
	
	public String getDescription()
	{
		return entry.getMediaGroup().getDescription().getPlainTextContent();
	}
	
	public String getAuthor()
	{
		return entry.getAuthors().get(0).getName();
	}
	
	public String getThumbnailUrl()
	{
		return THUMBNAIL_URL_BASE + getLinkSearchId() + THUMBNAIL_URL_DEFAULT_SUFFIX;		
	}
}