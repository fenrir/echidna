package org.fenrir.echidna.lyricwiki.dao;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import org.fenrir.echidna.lyricwiki.entity.LyricsResult;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120728
 */
public interface ILyricWikiWSDAO 
{
	public LyricsResult findLyrics(String artist, String song) throws JAXBException, TransformerException;
}
