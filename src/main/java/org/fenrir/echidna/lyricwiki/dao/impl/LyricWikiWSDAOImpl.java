package org.fenrir.echidna.lyricwiki.dao.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.ws.Service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.common.dao.AbstractWebserviceDAO;
import org.fenrir.echidna.lyricwiki.dao.ILyricWikiWSDAO;
import org.fenrir.echidna.lyricwiki.entity.LyricsResult;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120805
 */
public class LyricWikiWSDAOImpl extends AbstractWebserviceDAO implements ILyricWikiWSDAO
{
	private static final QName serviceQName = new QName("http://lyrics.wikia.com/", "Lyric Wiki Service");
    private static final QName portQName = new QName("http://lyrics.wikia.com/", "Lyric Wiki Port");	
	private static final String ENDPOINT_BASE_ADDRESS = "http://lyrics.wikia.com";
	private static final String REQUEST_PATH = "/api.php";
	
	private final Log log = LogFactory.getLog(LyricWikiWSDAOImpl.class);
	
	private static final String QUERY_STRING_PREFIX_ARTIST = "artist=";
	private static final String QUERY_STRING_PREFIX_SONG = "song=";
	private static final String QUERY_STRING_FORMAT_XML = "fmt=xml";	
	
	protected Service createService(String requestPath)
	{
		return createService(serviceQName, portQName, ENDPOINT_BASE_ADDRESS, requestPath);
	}
	
	protected Source invokeDispatch(Service service, Map<String, String> params)
	{
		return invokeDispatch(service, portQName, params);
	}
	
	protected Source invokeDispatch(Service service, String query)
	{
		return invokeDispatch(service, portQName, query);
	}
	
	public LyricsResult findLyrics(String artist, String song) throws JAXBException, TransformerException
	{
		try{
			artist = URLEncoder.encode(artist, "UTF-8");
			song = URLEncoder.encode(song, "UTF-8");
		}
		catch(UnsupportedEncodingException e){
			log.error("[LyricWikiDAOImpl::findLyrics] Error de codificació en tratar la cadena de cerca: " + e.getMessage(), e);
		}
		
		StringBuilder strQuery = new StringBuilder()
				.append(QUERY_STRING_PREFIX_ARTIST).append(artist)
				.append("&").append(QUERY_STRING_PREFIX_SONG).append(song)
				.append("&").append(QUERY_STRING_FORMAT_XML);
		
		Service service = createService(serviceQName, portQName, ENDPOINT_BASE_ADDRESS, REQUEST_PATH);
		Source source = invokeDispatch(service, strQuery.toString());
		LyricsResult result = buildResult(source, LyricsResult.class);
		
		return result;
	}
}
