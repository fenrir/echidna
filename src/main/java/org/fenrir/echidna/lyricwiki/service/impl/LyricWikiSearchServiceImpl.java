package org.fenrir.echidna.lyricwiki.service.impl;

import java.io.IOException;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.common.service.ILyricsSearchService;
import org.fenrir.echidna.lyricwiki.dao.ILyricWikiWSDAO;
import org.fenrir.echidna.lyricwiki.entity.LyricsResult;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120806
 */
public class LyricWikiSearchServiceImpl implements ILyricsSearchService 
{
	private static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13";
	private static final Pattern LYRICS_PAGE_DIV_PATTERN = Pattern.compile(
			"<div class='lyricbox'>.+</div>(.+)<!--",
			Pattern.CASE_INSENSITIVE);
//	private static final Pattern LYRICS_BYTE_CHARACTER = Pattern.compile("&#(\\d+);|<br />");
	
	private final Log log = LogFactory.getLog(LyricWikiSearchServiceImpl.class);
	
	@Inject
	private ILyricWikiWSDAO lyricWikiWSDAO;
	
	public void setLyricWikiWSDAO(ILyricWikiWSDAO lyricWikiWSDAO)
	{
		this.lyricWikiWSDAO = lyricWikiWSDAO;
	}
	
	public String findLyrics(String artistName, String songName) throws BussinessException
	{
		try{
			// Es busquen els possibles links
			LyricsResult result = lyricWikiWSDAO.findLyrics(artistName, songName);
			// Si existeix un link es recupera el text
			if(result.urlExists()){
				String url = result.getUrl();
				HttpEntity fullPage = doGetRequest(new URI(url));
				String pageCode = EntityUtils.toString(fullPage);
				Matcher matcher = LYRICS_PAGE_DIV_PATTERN.matcher(pageCode);
				if(matcher.find()){
					/* Es recupera la llista a partir del capturing group 1 i
					 * El 0 seria tota la regexp
					 */
					String lyricsBytes = matcher.group(1);
					
//					return parseText(lyricsBytes);
					return lyricsBytes;
				}
			}
		}
		catch(JAXBException e){
			throw new BussinessException("Error al configurar l'entorn per transformar la resposta del webservice: " + e.getMessage(), e);
		}
		catch(TransformerException e){
			throw new BussinessException("Error al transformat la resposta del webservice: " + e.getMessage(), e);
		}
		catch(URISyntaxException e){
			throw new BussinessException("Error en obtenir la lletra de la cançó desde la url: " + e.getMessage(), e);
		}
		catch(IOException e){
			throw new BussinessException("Error en interpretar el resultat de la url: " + e.getMessage(), e);
		}
		
		return null;
	}
	
//	private String parseText(String input)
//	{
//		List<Byte> bytes = new ArrayList<Byte>();
//
//		Matcher matcher = LYRICS_BYTE_CHARACTER.matcher(input);
//		while(matcher.find()){
//			String character = matcher.group(0);
//			if("<br />".equals(character)){
//				byte[] b = "\n".getBytes();
//				for(int i=0; i<b.length; i++){
//					bytes.add(b[i]);	
//				}
//			}
//			else{
//				character = matcher.group(1);
//				bytes.add(new Byte(character));			
//			}
//		}
//
//		byte[] array = new byte[bytes.size()];
//		for(int i=0; i<bytes.size(); i++){
//			array[i] = bytes.get(i).byteValue();
//		}
//
//		return new String(array);
//	}
	
	private HttpEntity doGetRequest(URI uri) throws BussinessException
	{
		try{
			CookieStore cookieStore = new BasicCookieStore();
			HttpContext localContext = new BasicHttpContext();
			localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

			DefaultHttpClient httpclient = new DefaultHttpClient();
			/* Configuració del proxy */
			// S'indica que agafi el proxy per defecte indicat a la JRE
			ProxySelectorRoutePlanner routePlanner = new ProxySelectorRoutePlanner(
			        httpclient.getConnectionManager().getSchemeRegistry(),
			        ProxySelector.getDefault());  
			httpclient.setRoutePlanner(routePlanner);
			
			HttpGet httpget = new HttpGet(uri);
			httpget.setHeader("User-Agent", DEFAULT_USER_AGENT);

			HttpResponse response = httpclient.execute(httpget, localContext);
			HttpEntity entity = response.getEntity();
			if(entity!=null && response.getStatusLine().getStatusCode()==200){
				return entity;
			}
			else{
				throw new BussinessException("No s'ha pogut contactar amb el servidor " + uri);
			}
		}
		catch(IOException e){
			log.error("[LyricWikiSearchServiceImpl::doGetRequest] Error descarregant desde la url " + uri + ": " + e.getMessage(), e);
			throw new BussinessException("Error descarregant desde la url " + uri + ": " + e.getMessage(), e);
		}
	}
}
