package org.fenrir.echidna.lyricwiki.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120728
 */
@XmlRootElement(name="LyricsResult")
@XmlAccessorType(XmlAccessType.FIELD)
public class LyricsResult 
{
	public static final String NOT_FOUND_VALUE = "Not found";
	
	@XmlElement
	private String artist;
	@XmlElement
	private String song;
	@XmlElement
	private String lyrics;
	@XmlElement
	private String url;
	@XmlElement(name="page_namespace")
	private String pageNamespace;
	@XmlElement(name="page_id")
	private String pageId;
	@XmlElement
	private String isOnTakedownList;
	
	public String getArtist() 
	{
		return artist;
	}
	
	public void setArtist(String artist) 
	{
		this.artist = artist;
	}
	
	public String getSong() 
	{
		return song;
	}
	
	public void setSong(String song) 
	{
		this.song = song;
	}
	
	public String getLyrics() 
	{
		return lyrics;
	}
	
	public void setLyrics(String lyrics) 
	{
		this.lyrics = lyrics;
	}
	
	public String getUrl() 
	{
		return url;
	}
	
	public void setUrl(String url) 
	{
		this.url = url;
	}
	
	public String getPageNamespace() 
	{
		return pageNamespace;
	}
	
	public void setPageNamespace(String pageNamespace) 
	{
		this.pageNamespace = pageNamespace;
	}
	
	public String getPageId() 
	{
		return pageId;
	}
	
	public void setPageId(String pageId) 
	{
		this.pageId = pageId;
	}
	
	public String getIsOnTakedownList() 
	{
		return isOnTakedownList;
	}
	
	public void setIsOnTakedownList(String isOnTakedownList) 
	{
		this.isOnTakedownList = isOnTakedownList;
	}
	
	public boolean urlExists()
	{
		return !NOT_FOUND_VALUE.equals(getLyrics());
	}
}
