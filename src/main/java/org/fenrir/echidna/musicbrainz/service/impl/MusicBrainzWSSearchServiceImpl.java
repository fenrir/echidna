package org.fenrir.echidna.musicbrainz.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import org.fenrir.echidna.common.dto.IAlbumSearchHitDTO;
import org.fenrir.echidna.common.dto.IArtistSearchHitDTO;
import org.fenrir.echidna.common.dto.GenericResultSetDTO;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.common.service.IMusicSearchService;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.musicbrainz.dao.AbstractMusicBrainzWSDAO;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzArtistWSDAO;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzRecordingWSDAO;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzReleaseGroupWSDAO;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzReleaseWSDAO;
import org.fenrir.echidna.musicbrainz.dto.AlbumSearchHitDTOAdapter;
import org.fenrir.echidna.musicbrainz.dto.ArtistSearchHitDTOAdapter;
import org.fenrir.echidna.musicbrainz.dto.RecordingSearchHitDTOAdapter;
import org.fenrir.echidna.musicbrainz.entity.ArtistResultSet;
import org.fenrir.echidna.musicbrainz.entity.ArtistSearchHit;
import org.fenrir.echidna.musicbrainz.entity.ArtistSingleResult;
import org.fenrir.echidna.musicbrainz.entity.RecordingResultSet;
import org.fenrir.echidna.musicbrainz.entity.RecordingSearchHit;
import org.fenrir.echidna.musicbrainz.entity.RecordingSingleResult;
import org.fenrir.echidna.musicbrainz.entity.ReleaseGroupResultSet;
import org.fenrir.echidna.musicbrainz.entity.ReleaseGroupSearchHit;
import org.fenrir.echidna.musicbrainz.entity.ReleaseResultSet;
import org.fenrir.echidna.musicbrainz.entity.ReleaseSearchHit;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120725
 */
public class MusicBrainzWSSearchServiceImpl implements IMusicSearchService 
{
	@Inject
	private IMusicBrainzArtistWSDAO musicBrainzArtistWSDAO;
	@Inject 
	private IMusicBrainzReleaseWSDAO musicBrainzReleaseWSDAO;
	@Inject
	private IMusicBrainzReleaseGroupWSDAO musicBrainzReleaseGroupWSDAO;
	@Inject 
	private IMusicBrainzRecordingWSDAO musicBrainzRecordingWSDAO; 
	
	public void setMusicBrainzArtistWSDAO(IMusicBrainzArtistWSDAO musicBrainzWSDAO)
	{
		this.musicBrainzArtistWSDAO = musicBrainzWSDAO;
	}
	
	public void setMusicBrainzReleaseWSDAO(IMusicBrainzReleaseWSDAO musicBrainzDAO)
	{
		this.musicBrainzReleaseWSDAO = musicBrainzDAO;
	}
	
	public void setMusicBrainzReleaseGroupWSDAO(IMusicBrainzReleaseGroupWSDAO musicBrainzDAO)
	{
		this.musicBrainzReleaseGroupWSDAO = musicBrainzDAO;
	}
	
	public void setMusicBrainzRecordingWSDAO(IMusicBrainzRecordingWSDAO musicBrainzRecordingWSDAO)
	{
		this.musicBrainzRecordingWSDAO = musicBrainzRecordingWSDAO;
	}

	public GenericResultSetDTO<IArtistSearchHitDTO> findArtistsByName(String name) throws BussinessException
	{
		try{
			ArtistResultSet resultSet = musicBrainzArtistWSDAO.findArtistsByName(name);
			List<IArtistSearchHitDTO> albums = new ArrayList<IArtistSearchHitDTO>();
			for(ArtistSearchHit artist:resultSet.getArtists()){
				albums.add(new ArtistSearchHitDTOAdapter(artist));
			}
			
			return new GenericResultSetDTO<IArtistSearchHitDTO>(albums, resultSet.getTotalCount(), resultSet.getOffset());				
		}
		catch(JAXBException e){
			throw new BussinessException("Error al configurar l'entorn per transformar la resposta del webservice: " + e.getMessage(), e);
		}
		catch(TransformerException e){
			throw new BussinessException("Error al transformat la resposta del webservice: " + e.getMessage(), e);
		}				
	}
	
	public GenericResultSetDTO<IAlbumSearchHitDTO> findAlbumsByName(String name) throws BussinessException
	{
		try{
			ReleaseGroupResultSet resultSet = musicBrainzReleaseGroupWSDAO.findReleaseGroupsByName(name);
			List<IAlbumSearchHitDTO> albums = new ArrayList<IAlbumSearchHitDTO>();
			for(ReleaseGroupSearchHit album:resultSet.getReleaseGroups()){
				albums.add(new AlbumSearchHitDTOAdapter(album));
			}
			
			return new GenericResultSetDTO<IAlbumSearchHitDTO>(albums, resultSet.getTotalCount(), resultSet.getOffset());			
		}
		catch(JAXBException e){
			throw new BussinessException("Error al configurar l'entorn per transformar la resposta del webservice: " + e.getMessage(), e);
		}
		catch(TransformerException e){
			throw new BussinessException("Error al transformat la resposta del webservice: " + e.getMessage(), e);
		}				
	}
	
	public GenericResultSetDTO<IAlbumSearchHitDTO> findAlbumsByProviderArtistId(String id) throws BussinessException
	{
		try{
			ArtistSingleResult artistResult = musicBrainzArtistWSDAO.findArtistById(id, AbstractMusicBrainzWSDAO.LOOKUP_INCLUDE_MASK_RELEASE_GROUPS);
			List<IAlbumSearchHitDTO> albums = new ArrayList<IAlbumSearchHitDTO>();
			if(artistResult.getArtist().hasReleaseGroups()){
				for(ReleaseGroupSearchHit album:artistResult.getArtist().getReleaseGroupResultSet().getReleaseGroups()){
					albums.add(new AlbumSearchHitDTOAdapter(artistResult.getArtist(), album));
				}
			}
			
			return new GenericResultSetDTO<IAlbumSearchHitDTO>(albums, albums.size(), 0);			
		}
		catch(JAXBException e){
			throw new BussinessException("Error al configurar l'entorn per transformar la resposta del webservice: " + e.getMessage(), e);
		}
		catch(TransformerException e){
			throw new BussinessException("Error al transformat la resposta del webservice: " + e.getMessage(), e);
		}				
	}
	
	public IRecordingSearchHitDTO findRecordingByProviderId(String id) throws BussinessException
	{
		try{			
			RecordingSingleResult resultSet = musicBrainzRecordingWSDAO.findRecordingById(id, AbstractMusicBrainzWSDAO.LOOKUP_INCLUDE_MASK_ARTISTS | AbstractMusicBrainzWSDAO.LOOKUP_INCLUDE_MASK_RELEASES);
			return new RecordingSearchHitDTOAdapter(resultSet.getRecording());
		}
		catch(JAXBException e){
			throw new BussinessException("Error al configurar l'entorn per transformar la resposta del webservice: " + e.getMessage(), e);
		}
		catch(TransformerException e){
			throw new BussinessException("Error al transformat la resposta del webservice: " + e.getMessage(), e);
		}
	}
	
	public GenericResultSetDTO<IRecordingSearchHitDTO> findRecordingsByName(String name) throws BussinessException
	{
		try{
			RecordingResultSet resultSet = musicBrainzRecordingWSDAO.findRecordingsByName(name);
			List<IRecordingSearchHitDTO> recordings = new ArrayList<IRecordingSearchHitDTO>();
			for(RecordingSearchHit recording:resultSet.getRecordings()){
				recordings.add(new RecordingSearchHitDTOAdapter(recording));
			}
			
			return new GenericResultSetDTO<IRecordingSearchHitDTO>(recordings, resultSet.getTotalCount(), resultSet.getOffset());			
		}
		catch(JAXBException e){
			throw new BussinessException("Error al configurar l'entorn per transformar la resposta del webservice: " + e.getMessage(), e);
		}
		catch(TransformerException e){
			throw new BussinessException("Error al transformat la resposta del webservice: " + e.getMessage(), e);
		}				
	}
	
	public GenericResultSetDTO<IRecordingSearchHitDTO> findRecordingsByProviderAlbumId(String id) throws BussinessException
	{
		try{
			/* Pot ser que l'identificador proporcionat correspongui a una release en comptes de a un grup
			 * ja que en les cerques per recording la id associada al disc es aquesta. En aquest cas,
			 * si la cerca per release-group no dóna resultats, es buscarà directament per la MBID de la release
			 */
			// Només es comproven els duplicats en el cas que es cerqui a més d'una release
			boolean checkDuplicates = false;
			ReleaseResultSet releaseResultSet = musicBrainzReleaseWSDAO.findReleasesByReleseaseGroupId(id);			
			Set<String> releaseIdSet = new HashSet<String>();
			if(releaseResultSet.getTotalCount()!=0){
				for(ReleaseSearchHit searchHit:releaseResultSet.getReleases()){
					releaseIdSet.add(searchHit.getId());
				}
				checkDuplicates = releaseIdSet.size()>1;
			}
			else{
				releaseIdSet.add(id);
			}
			
			RecordingResultSet resultSet = musicBrainzRecordingWSDAO.findRecordingsByReleaseIds(releaseIdSet);
			List<IRecordingSearchHitDTO> recordings = new ArrayList<IRecordingSearchHitDTO>();
			// S'utilitza un set per controlar els duplicats provinents de les diferents releases associades al release-group
			HashSet<String> addedTitles = new HashSet<String>();
			for(RecordingSearchHit recording:resultSet.getRecordings()){
				if(!checkDuplicates || checkDuplicates && !addedTitles.contains(recording.getTitle())){
					recordings.add(new RecordingSearchHitDTOAdapter(recording));
					addedTitles.add(recording.getTitle());
				}
			}
			
			return new GenericResultSetDTO<IRecordingSearchHitDTO>(recordings, resultSet.getTotalCount(), resultSet.getOffset());	
		}
		catch(JAXBException e){
			throw new BussinessException("Error al configurar l'entorn per transformar la resposta del webservice: " + e.getMessage(), e);
		}
		catch(TransformerException e){
			throw new BussinessException("Error al transformat la resposta del webservice: " + e.getMessage(), e);
		}
	}
}