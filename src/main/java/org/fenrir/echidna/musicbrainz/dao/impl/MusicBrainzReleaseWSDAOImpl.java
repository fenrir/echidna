package org.fenrir.echidna.musicbrainz.dao.impl;

import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.ws.Service;
import org.fenrir.echidna.musicbrainz.dao.AbstractMusicBrainzWSDAO;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzReleaseWSDAO;
import org.fenrir.echidna.musicbrainz.entity.ReleaseResultSet;
import org.fenrir.echidna.musicbrainz.entity.ResultSet;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120422
 */
public class MusicBrainzReleaseWSDAOImpl extends AbstractMusicBrainzWSDAO implements IMusicBrainzReleaseWSDAO 
{
	private static final String REQUEST_PATH_RELEASE = "/release";
	
	public ReleaseResultSet findReleasesByArtistId(String artistId) throws JAXBException, TransformerException
	{
		Service service = createService(REQUEST_PATH_RELEASE);
		Source source = invokeDispatch(service, QUERY_STRING_PREFIX + SEARCH_FIELD_ARTIST_ID + artistId);
		ResultSet resultSet = buildResultSet(source);
		if(resultSet.hasRecordings()){
			return resultSet.getReleases();
		}
		
		return ReleaseResultSet.emptyResultSet();
	}
	
	public ReleaseResultSet findReleasesByReleseaseGroupId(String groupId) throws JAXBException, TransformerException
	{
		Service service = createService(REQUEST_PATH_RELEASE);
		Source source = invokeDispatch(service, QUERY_STRING_PREFIX + SEARCH_FIELD_RELEASE_GROUP_ID + groupId);
		ResultSet resultSet = buildResultSet(source);
		if(resultSet.hasReleases()){
			return resultSet.getReleases();
		}
		
		return ReleaseResultSet.emptyResultSet();
	}
}
