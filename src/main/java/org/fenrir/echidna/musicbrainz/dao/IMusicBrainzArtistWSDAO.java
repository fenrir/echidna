package org.fenrir.echidna.musicbrainz.dao;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import org.fenrir.echidna.musicbrainz.entity.ArtistResultSet;
import org.fenrir.echidna.musicbrainz.entity.ArtistSingleResult;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120427
 */
public interface IMusicBrainzArtistWSDAO 
{
	public ArtistSingleResult findArtistById(String id) throws JAXBException, TransformerException;
	public ArtistSingleResult findArtistById(String id, int includeMask) throws JAXBException, TransformerException;
	public ArtistResultSet findArtistsByName(String name) throws JAXBException, TransformerException;
	public ArtistResultSet findArtistsByNameLike(String name, float proximityIndex)throws JAXBException, TransformerException;
}