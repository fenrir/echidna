package org.fenrir.echidna.musicbrainz.dao;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import org.fenrir.echidna.musicbrainz.entity.ReleaseResultSet;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120422
 */
public interface IMusicBrainzReleaseWSDAO 
{
	public ReleaseResultSet findReleasesByArtistId(String artistId) throws JAXBException, TransformerException;
	public ReleaseResultSet findReleasesByReleseaseGroupId(String groupId) throws JAXBException, TransformerException;
}
