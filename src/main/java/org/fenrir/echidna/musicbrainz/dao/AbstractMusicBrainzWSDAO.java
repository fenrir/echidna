package org.fenrir.echidna.musicbrainz.dao;

import java.util.Map;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.ws.Service;
import org.fenrir.echidna.common.dao.AbstractWebserviceDAO;
import org.fenrir.echidna.musicbrainz.entity.ResultSet;

/**
 * TODO v1.0 Javadoc
 * TODO Posar com a constants les cadenes de connexió al webservice i el user-agent
 * @author Antonio Archilla Nava
 * @version v0.0.20120728
 */
public abstract class AbstractMusicBrainzWSDAO extends AbstractWebserviceDAO
{
	private static final QName serviceQName = new QName("http://www.musicbrainz.org", "MusicBrainz WS 2 Service");
    private static final QName portQName = new QName("http://www.musicbrainz.org", "MusicBrainz WS 2 Port");	
	private static final String ENDPOINT_BASE_ADDRESS = "http://musicbrainz.org/ws/2";	
	
	protected static final String QUERY_STRING_PREFIX = "query=";
	protected static final String QUERY_STRING_INCLUDE = "inc";
	
	protected static final String SEARCH_FIELD_RECORDING = "recording:";
	protected static final String SEARCH_FIELD_RELEASE_ID = "reid:";
	protected static final String SEARCH_FIELD_ARTIST = "artist:";
	protected static final String SEARCH_FIELD_ARTIST_ID = "arid:";
	protected static final String SEARCH_FIELD_RELEASE_GROUP = "releasegroup:";
	protected static final String SEARCH_FIELD_RELEASE_GROUP_ID = "rgid:";
	
	protected static final String LOOKUP_INCLUDE_ARTISTS = "artists";
	protected static final String LOOKUP_INCLUDE_RELEASE_GROUPS = "release-groups";
	protected static final String LOOKUP_INCLUDE_RELEASES = "releases";
	protected static final String LOOKUP_INCLUDE_RECORDINGS = "recordings";
	
	public static final int LOOKUP_INCLUDE_MASK_NONE = 0;
	public static final int LOOKUP_INCLUDE_MASK_ARTISTS = 1;
	public static final int LOOKUP_INCLUDE_MASK_RELEASE_GROUPS = 2;
	public static final int LOOKUP_INCLUDE_MASK_RELEASES = 4;
	public static final int LOOKUP_INCLUDE_MASK_RECORDINGS = 8;

	protected Service createService(String requestPath)
	{
		return createService(serviceQName, portQName, ENDPOINT_BASE_ADDRESS, requestPath);
	}
	
	protected Source invokeDispatch(Service service, Map<String, String> params)
	{
		return invokeDispatch(service, portQName, params);
	}
	
	protected Source invokeDispatch(Service service, String query)
	{
		return invokeDispatch(service, portQName, query);
	}
	
	protected ResultSet buildResultSet(Source source) throws TransformerException, JAXBException
	{
		return buildResult(source, ResultSet.class);
	}
	
	protected <T> T buildEntity(Source source, Class<T> entityClass) throws TransformerException, JAXBException
	{
		return buildResult(source, entityClass);
	}
}