package org.fenrir.echidna.musicbrainz.dao;

import java.util.Collection;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import org.fenrir.echidna.musicbrainz.entity.RecordingResultSet;
import org.fenrir.echidna.musicbrainz.entity.RecordingSingleResult;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120623
 */
public interface IMusicBrainzRecordingWSDAO 
{
	public RecordingSingleResult findRecordingById(String id, int includeMask) throws JAXBException, TransformerException;
	public RecordingResultSet findRecordingsByName(String name) throws JAXBException, TransformerException;
	public RecordingResultSet findRecordingsByNameLike(String name, float proximityIndex)throws JAXBException, TransformerException;
	public RecordingResultSet findRecordingsByReleaseId(String releaseId) throws JAXBException, TransformerException;
	public RecordingResultSet findRecordingsByReleaseIds(Collection<String> releaseIds) throws JAXBException, TransformerException;
}
