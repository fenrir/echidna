package org.fenrir.echidna.musicbrainz.dao;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import org.fenrir.echidna.musicbrainz.entity.ReleaseGroupResultSet;
import org.fenrir.echidna.musicbrainz.entity.ReleaseGroupSingleResult;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120705
 */
public interface IMusicBrainzReleaseGroupWSDAO 
{
	public ReleaseGroupSingleResult findReleaseGroupsById(String id) throws JAXBException, TransformerException;
	public ReleaseGroupSingleResult findReleaseGroupsById(String id, int includeMask) throws JAXBException, TransformerException;
	public ReleaseGroupResultSet findReleaseGroupsByName(String name) throws JAXBException, TransformerException;
	public ReleaseGroupResultSet findReleaseGroupsByNameLike(String name, float proximityIndex)throws JAXBException, TransformerException;
	public ReleaseGroupResultSet findReleaseGroupsByArtistId(String artistId) throws JAXBException, TransformerException;
	public ReleaseGroupResultSet findReleaseGroupByReleaseId(String releaseId) throws JAXBException, TransformerException;
}