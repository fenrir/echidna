package org.fenrir.echidna.musicbrainz.dao.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.ws.Service;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.musicbrainz.dao.AbstractMusicBrainzWSDAO;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzReleaseGroupWSDAO;
import org.fenrir.echidna.musicbrainz.entity.ReleaseGroupResultSet;
import org.fenrir.echidna.musicbrainz.entity.ReleaseGroupSingleResult;
import org.fenrir.echidna.musicbrainz.entity.ResultSet;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120705
 */
public class MusicBrainzReleaseGroupWSDAOImpl extends AbstractMusicBrainzWSDAO implements IMusicBrainzReleaseGroupWSDAO 
{	
	private static final String REQUEST_PATH_RELEASE_GROUP = "/release-group";
	
	private final Log log = LogFactory.getLog(MusicBrainzReleaseGroupWSDAOImpl.class);

	public ReleaseGroupSingleResult findReleaseGroupsById(String id) throws JAXBException, TransformerException
	{
		return findReleaseGroupsById(id, LOOKUP_INCLUDE_MASK_NONE);
	}
	
	/**
	 * @param id String - ID de l'artista a buscar
	 * @param includeMask int - Màscara de les entitats associades a incloure en els resultats
	 * 		Els valors vàlids són artists, releases
	 */
	public ReleaseGroupSingleResult findReleaseGroupsById(String id, int includeMask) throws JAXBException, TransformerException
	{
		boolean append = false;
		StringBuilder strQuery = new StringBuilder();
		if(includeMask>0){
			strQuery.append(QUERY_STRING_INCLUDE).append("=");
		}
		if((includeMask & LOOKUP_INCLUDE_MASK_ARTISTS) > 0){
			strQuery.append(LOOKUP_INCLUDE_ARTISTS);
			append = true;
		}
		if((includeMask & LOOKUP_INCLUDE_MASK_RELEASES) > 0){
			if(append){
				strQuery.append("+");
			}
			else{
				append = true;
			}
			strQuery.append(LOOKUP_INCLUDE_RELEASES);
		}
		
		Service service = createService(REQUEST_PATH_RELEASE_GROUP + "/" + id);
		Source source = invokeDispatch(service, strQuery.toString());
		ReleaseGroupSingleResult result = (ReleaseGroupSingleResult)buildEntity(source, ReleaseGroupSingleResult.class);
		
		return result;
	}
	
	public ReleaseGroupResultSet findReleaseGroupsByName(String name) throws JAXBException, TransformerException
	{
		return findReleaseGroupsByNameLike(name, 1.0f);
	}
	
	/**
	 * TODO Implementar fuzzy search
	 */
	public ReleaseGroupResultSet findReleaseGroupsByNameLike(String name, float proximityIndex) throws JAXBException, TransformerException
	{
		if(proximityIndex<0 && proximityIndex>1){
			throw new IllegalArgumentException("El valor de l'index de proximitat ha d'estar entre 0.0 i 1.0");
		}

		try{
			name = URLEncoder.encode(name, "UTF-8");
		}
		catch(UnsupportedEncodingException e){
			log.error("[MusicBrainzReleaseGroupWSDAOImpl::findReleaseGroupsByNameLike] Error de codificació en tratar la cadena de cerca: " + e.getMessage(), e);
		}
		Service service = createService(REQUEST_PATH_RELEASE_GROUP);
		Source source = invokeDispatch(service, QUERY_STRING_PREFIX + SEARCH_FIELD_RELEASE_GROUP + name);
		ResultSet resultSet = buildResultSet(source);
		if(resultSet.hasReleaseGroups()){
			return resultSet.getReleaseGroups();
		}
		
		return ReleaseGroupResultSet.emptyResultSet();		
	}		
	
	public ReleaseGroupResultSet findReleaseGroupsByArtistId(String artistId) throws JAXBException, TransformerException
	{
		Service service = createService(REQUEST_PATH_RELEASE_GROUP);
		Source source = invokeDispatch(service, QUERY_STRING_PREFIX + SEARCH_FIELD_ARTIST_ID + artistId);
		ResultSet resultSet = buildResultSet(source);
		if(resultSet.hasReleaseGroups()){
			return resultSet.getReleaseGroups();
		}
		
		return ReleaseGroupResultSet.emptyResultSet();
	}
	
	public ReleaseGroupResultSet findReleaseGroupByReleaseId(String releaseId) throws JAXBException, TransformerException
	{
		Service service = createService(REQUEST_PATH_RELEASE_GROUP);
		Source source = invokeDispatch(service, QUERY_STRING_PREFIX + SEARCH_FIELD_RELEASE_ID + releaseId);
		ResultSet resultSet = buildResultSet(source);
		if(resultSet.hasReleaseGroups()){
			return resultSet.getReleaseGroups();
		}
		
		return ReleaseGroupResultSet.emptyResultSet();
	}
}
