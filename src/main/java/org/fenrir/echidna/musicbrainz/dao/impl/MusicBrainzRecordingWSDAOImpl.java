package org.fenrir.echidna.musicbrainz.dao.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.ws.Service;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.musicbrainz.dao.AbstractMusicBrainzWSDAO;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzRecordingWSDAO;
import org.fenrir.echidna.musicbrainz.entity.RecordingResultSet;
import org.fenrir.echidna.musicbrainz.entity.RecordingSingleResult;
import org.fenrir.echidna.musicbrainz.entity.ResultSet;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120623
 */
public class MusicBrainzRecordingWSDAOImpl extends AbstractMusicBrainzWSDAO implements IMusicBrainzRecordingWSDAO 
{
	private static final String REQUEST_PATH_RECORDING = "/recording";
	
	private final Log log = LogFactory.getLog(MusicBrainzRecordingWSDAOImpl.class);

	/**
	 * @param id String - ID de la cançó a buscar
	 * @param includeMask int - Màscara de les entitats associades a incloure en els resultats
	 * 		Els valors vàlids són artists, releases
	 */
	public RecordingSingleResult findRecordingById(String id, int includeMask) throws JAXBException, TransformerException
	{
		boolean append = false;
		StringBuilder strQuery = new StringBuilder();
		if(includeMask>0){
			strQuery.append(QUERY_STRING_INCLUDE).append("=");
		}
		if((includeMask & LOOKUP_INCLUDE_MASK_ARTISTS) > 0){
			strQuery.append(LOOKUP_INCLUDE_ARTISTS);
			append = true;
		}
		if((includeMask & LOOKUP_INCLUDE_MASK_RELEASES) > 0){
			if(append){
				strQuery.append("+");
			}
			else{
				append = true;
			}
			strQuery.append(LOOKUP_INCLUDE_RELEASES);
		}
		
		Service service = createService(REQUEST_PATH_RECORDING + "/" + id);
		Source source = invokeDispatch(service, strQuery.toString());
		RecordingSingleResult result = (RecordingSingleResult)buildEntity(source, RecordingSingleResult.class);
		
		return result;
	}
	
	public RecordingResultSet findRecordingsByName(String name) throws JAXBException, TransformerException
	{
		return findRecordingsByNameLike(name, 1.0f);
	}

	/**
	 * TODO Implementar fuzzy search
	 */
	public RecordingResultSet findRecordingsByNameLike(String name, float proximityIndex) throws JAXBException, TransformerException
	{
		if(proximityIndex<0 && proximityIndex>1){
			throw new IllegalArgumentException("El valor de l'index de proximitat ha d'estar entre 0.0 i 1.0");
		}
		
		Service service = createService(REQUEST_PATH_RECORDING);
		try{
			name = URLEncoder.encode(name, "UTF-8");
		}
		catch(UnsupportedEncodingException e){
			log.error("[MusicBrainzRecordingWSDAOImpl::findRecordingsByNameLike] Error de codificació en tratar la cadena de cerca: " + e.getMessage(), e);
		}
		Source source = invokeDispatch(service, QUERY_STRING_PREFIX + SEARCH_FIELD_RECORDING + name);
		ResultSet resultSet = buildResultSet(source);
		if(resultSet.hasRecordings()){
			return resultSet.getRecordings();
		}
		
		return RecordingResultSet.emptyResultSet();		
	}
	
	public RecordingResultSet findRecordingsByReleaseId(String releaseId) throws JAXBException, TransformerException
	{
		Service service = createService(REQUEST_PATH_RECORDING);
		Source source = invokeDispatch(service, QUERY_STRING_PREFIX + SEARCH_FIELD_RELEASE_ID + releaseId);
		ResultSet resultSet = buildResultSet(source);
		if(resultSet.hasRecordings()){
			return resultSet.getRecordings();
		}
		
		return RecordingResultSet.emptyResultSet();
	}
	
	public RecordingResultSet findRecordingsByReleaseIds(Collection<String> releaseIds) throws JAXBException, TransformerException
	{
		Service service = createService(REQUEST_PATH_RECORDING);
		
		StringBuilder queryBuilder = new StringBuilder();
		for(String id:releaseIds){
			if(queryBuilder.length()>0){
				queryBuilder.append(" OR ");
			}
			queryBuilder.append(SEARCH_FIELD_RELEASE_ID).append(id);
		}
		String strQuery = ""; 
		try{
			strQuery = URLEncoder.encode(queryBuilder.toString(), "UTF-8");
		}
		catch(UnsupportedEncodingException e){
			log.error("[MusicBrainzRecordingWSDAOImpl::findRecordingsByNameLike] Error de codificació en tratar la cadena de cerca: " + e.getMessage(), e);
		}
		
		Source source = invokeDispatch(service, QUERY_STRING_PREFIX + strQuery);
		ResultSet resultSet = buildResultSet(source);
		if(resultSet.hasRecordings()){
			return resultSet.getRecordings();
		}
		
		return RecordingResultSet.emptyResultSet();
	}
}
