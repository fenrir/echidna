package org.fenrir.echidna.musicbrainz.dao.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.ws.Service;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.musicbrainz.dao.AbstractMusicBrainzWSDAO;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzArtistWSDAO;
import org.fenrir.echidna.musicbrainz.entity.ArtistResultSet;
import org.fenrir.echidna.musicbrainz.entity.ArtistSingleResult;
import org.fenrir.echidna.musicbrainz.entity.ResultSet;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120728
 */
public class MusicBrainzArtistWSDAOImpl extends AbstractMusicBrainzWSDAO implements IMusicBrainzArtistWSDAO 
{		
	private static final String REQUEST_PATH_ARTIST = "/artist";
	
	private final Log log = LogFactory.getLog(MusicBrainzArtistWSDAOImpl.class);

	public ArtistSingleResult findArtistById(String id) throws JAXBException, TransformerException
	{
		return findArtistById(id, LOOKUP_INCLUDE_MASK_NONE);
	}
	
	/**
	 * @param id String - ID de l'artista a buscar
	 * @param includeMask int - Màscara de les entitats associades a incloure en els resultats
	 * 		Els valors vàlids són release-groups, releases, recordings
	 */
	public ArtistSingleResult findArtistById(String id, int includeMask) throws JAXBException, TransformerException
	{
		boolean append = false;
		StringBuilder strQuery = new StringBuilder();
		if(includeMask>0){
			strQuery.append(QUERY_STRING_INCLUDE).append("=");
		}
		if((includeMask & LOOKUP_INCLUDE_MASK_RELEASE_GROUPS) > 0){
			strQuery.append(LOOKUP_INCLUDE_RELEASE_GROUPS);
			append = true;
		}
		if((includeMask & LOOKUP_INCLUDE_MASK_RELEASES) > 0){
			if(append){
				strQuery.append("+");
			}
			else{
				append = true;
			}
			strQuery.append(LOOKUP_INCLUDE_RELEASES);
		}
		if((includeMask & LOOKUP_INCLUDE_MASK_RECORDINGS) > 0){
			if(append){
				strQuery.append("+");
			}
			strQuery.append(LOOKUP_INCLUDE_RECORDINGS);
		}
		
		Service service = createService(REQUEST_PATH_ARTIST + "/" + id);
		Source source = invokeDispatch(service, strQuery.toString());
		ArtistSingleResult result = buildEntity(source, ArtistSingleResult.class);
		
		return result;
	}
	
	public ArtistResultSet findArtistsByName(String name) throws JAXBException, TransformerException
	{
		return findArtistsByNameLike(name, 1.0f);
	}

	/**
	 * TODO Implementar fuzzy search
	 */
	public ArtistResultSet findArtistsByNameLike(String name, float proximityIndex) throws JAXBException, TransformerException
	{
		if(proximityIndex<0 && proximityIndex>1){
			throw new IllegalArgumentException("El valor de l'index de proximitat ha d'estar entre 0.0 i 1.0");
		}
		
		Service service = createService(REQUEST_PATH_ARTIST);
		try{
			name = URLEncoder.encode(name, "UTF-8");
		}
		catch(UnsupportedEncodingException e){
			log.error("[MusicBrainzArtistWSDAOImpl::findArtistsByNameLike] Error de codificació en tratar la cadena de cerca: " + e.getMessage(), e);
		}
		Source source = invokeDispatch(service, QUERY_STRING_PREFIX + SEARCH_FIELD_ARTIST + name);
		ResultSet resultSet = buildResultSet(source);
		if(resultSet.hasArtists()){
			return resultSet.getArtists();
		}
		
		return ArtistResultSet.emptyResultSet();		
	}		
}
