package org.fenrir.echidna.musicbrainz.entity;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120624
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TrackList 
{
	@XmlAttribute
	protected Integer offset;
	@XmlAttribute
	protected Integer count;
	@XmlElement(name="track")
	protected List<Track> tracks;
	
	public Integer getOffset()
	{
		return offset;
	}
	
	public void setOffset(Integer offset)
	{
		this.offset = offset;
	}
	
	public Integer getCount()
	{
		return count;
	}
	
	public void setCount(Integer count)
	{
		this.count = count;
	}
	
	public List<Track> getTracks()
	{
		return tracks;
	}
	
	public void setTracks(List<Track> tracks)
	{
		this.tracks = tracks;
	}
}
