package org.fenrir.echidna.musicbrainz.entity;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120427
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ArtistSearchHit 
{
	@XmlAttribute
    protected String id;
    @XmlAttribute
    protected String type;	
    @XmlAttribute(name="ext:score")
    protected Integer score;
	@XmlElement
    protected String name;
    @XmlElement(name = "sort-name")
    protected String sortName;
    @XmlElement
    protected String disambiguation;
    @XmlElement(name = "life-span")
    protected LifeSpan lifeSpan;
    @XmlElement(name="alias")
	@XmlElementWrapper(name="alias-list")
    protected List<String> aliasList;
    /* Entitats relacionades */
    @XmlElement(name="release-group-list")
	protected ReleaseGroupResultSet releaseGroupResultSet;
    @XmlElement(name="release-list")
	protected ReleaseResultSet releaseResultSet;
    
	public String getId() 
	{
		return id;
	}
	
	public void setId(String id) 
	{
		this.id = id;
	}
	
	public String getType() 
	{
		return type;
	}
	
	public void setType(String type) 
	{
		this.type = type;
	}
	
	public Integer getScore()
	{
		return score;
	}
	
	public void setScore(Integer score)
	{
		this.score = score;
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public String getSortName() 
	{
		return sortName;
	}
	
	public void setSortName(String sortName) 
	{
		this.sortName = sortName;
	}
	
	public String getDisambiguation() 
	{
		return disambiguation;
	}
	
	public void setDisambiguation(String disambiguation) 
	{
		this.disambiguation = disambiguation;
	}
	
	public LifeSpan getLifeSpan() 
	{
		return lifeSpan;
	}
	
	public void setLifeSpan(LifeSpan lifeSpan) 
	{
		this.lifeSpan = lifeSpan;
	}
	
	public List<String> getAliasList() 
	{
		return aliasList;
	}
	
	public void setAliasList(List<String> aliasList) 
	{
		this.aliasList = aliasList;
	}

	/** 
	 * Getter entitat relacionada ReleaseGroup. Només estarà plè per defecte quan es faci una cerca per MBID
	 * @return ReleaseGroupResultSet - Llista de release groups associats a l'artista
	 */
	public ReleaseGroupResultSet getReleaseGroupResultSet() 
	{
		return releaseGroupResultSet;
	}

	public void setReleaseGroupResultSet(ReleaseGroupResultSet releaseGroupResultSet) 
	{
		this.releaseGroupResultSet = releaseGroupResultSet;
	}
	
	public boolean hasReleaseGroups()
	{
		return releaseGroupResultSet!=null && releaseGroupResultSet.getTotalCount()>0;
	}

	/** 
	 * Getter entitat relacionada Release. Només estarà plè per defecte quan es faci una cerca per MBID
	 * @return ReleaseResultSet - Llista de releases associats a l'artista
	 */
	public ReleaseResultSet getReleaseResultSet() 
	{
		return releaseResultSet;
	}

	public void setReleaseResultSet(ReleaseResultSet releaseResultSet) 
	{
		this.releaseResultSet = releaseResultSet;
	}        
	
	public boolean hasReleases()
	{
		return releaseResultSet!=null && releaseResultSet.getTotalCount()>0;
	}
}