package org.fenrir.echidna.musicbrainz.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120430
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ReleaseGroupSearchHit 
{
	@XmlAttribute
    protected String id;
    @XmlAttribute
    protected String type;	
    @XmlAttribute(name="ext:score")
    protected Integer score;
    @XmlElement
    protected String title;
    @XmlElement
    protected String disambiguation;
    @XmlElement
    protected String comment;
    @XmlElement(name="first-release-date")
    protected String firstReleaseDate;
    @XmlElement(name="artist-credit")
    protected ArtistCredit artistCredit;
    
    public String getId() 
	{
		return id;
	}
	
	public void setId(String id) 
	{
		this.id = id;
	}
	
	public String getType() 
	{
		return type;
	}
	
	public void setType(String type) 
	{
		this.type = type;
	}
	
	public Integer getScore()
	{
		return score;
	}
	
	public void setScore(Integer score)
	{
		this.score = score;
	}	
	
	public String getDisambiguation() 
	{
		return disambiguation;
	}
	
	public void setDisambiguation(String disambiguation) 
	{
		this.disambiguation = disambiguation;
	}

	public String getTitle() 
	{
		return title;
	}

	public void setTitle(String title) 
	{
		this.title = title;
	}

	public String getComment() 
	{
		return comment;
	}

	public void setComment(String comment) 
	{
		this.comment = comment;
	}

	/**
	 * Només està present a les cerques per MBID. Si es fa una cerca lliure, serà null
	 * @return
	 */
	public String getFirstReleaseDate() 
	{
		return firstReleaseDate;
	}

	public void setFirstReleaseDate(String firstReleaseDate) 
	{
		this.firstReleaseDate = firstReleaseDate;
	}
	
	public ArtistCredit getArtistCredit()
	{
		return artistCredit;
	}
	
	public void setArtistCredit(ArtistCredit artistCredit)
	{
		this.artistCredit = artistCredit;
	}
}
