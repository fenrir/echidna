package org.fenrir.echidna.musicbrainz.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120624
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ReleaseSearchHit 
{
	@XmlAttribute
    protected String id;
    @XmlAttribute(name="ext:score")
    protected Integer score;
    @XmlElement
    protected String title;
    @XmlElement
    protected String status;
    @XmlElement(name="text-representation")
    protected TextRepresentation textRepresentation;
    @XmlElement(name="artist-credit")
    protected ArtistCredit artistCredit;
    @XmlElement(name="medium-list")
    protected MediumList mediumList;
    @XmlElement
    protected String date;
    @XmlElement
    protected String country;
    
	public String getId() 
	{
		return id;
	}
	
	public void setId(String id) 
	{
		this.id = id;
	}
	
	public Integer getScore() 
	{
		return score;
	}
	
	public void setScore(Integer score) 
	{
		this.score = score;
	}
	
	public String getTitle() 
	{
		return title;
	}
	
	public void setTitle(String title) 
	{
		this.title = title;
	}
	
	public String getStatus() 
	{
		return status;
	}
	
	public void setStatus(String status) 
	{
		this.status = status;
	}
	
	public TextRepresentation getTextRepresentation() 
	{
		return textRepresentation;
	}
	
	public void setTextRepresentation(TextRepresentation textRepresentation) 
	{
		this.textRepresentation = textRepresentation;
	}
	
	public ArtistCredit getArtistCredit() 
	{
		return artistCredit;
	}
	
	public void setArtistCredit(ArtistCredit artistCredit) 
	{
		this.artistCredit = artistCredit;
	}
	
	public MediumList getMediumList()
	{
		return mediumList;
	}
	
	public void setMediumList(MediumList mediumList)
	{
		this.mediumList = mediumList;
	}
	
	public String getDate() 
	{
		return date;
	}
	
	public void setDate(String date) 
	{
		this.date = date;
	}
	
	public String getCountry() 
	{
		return country;
	}
	
	public void setCountry(String country) 
	{
		this.country = country;
	}
}
