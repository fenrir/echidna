package org.fenrir.echidna.musicbrainz.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120406
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class LifeSpan 
{
	@XmlElement
	private String begin;
	@XmlElement
	private String end;
	
	public String getBegin() 
	{
		return begin;
	}
	
	public void setBegin(String begin) 
	{
		this.begin = begin;
	}
	
	public String getEnd() 
	{
		return end;
	}
	
	public void setEnd(String end) 
	{
		this.end = end;
	}		
}