package org.fenrir.echidna.musicbrainz.entity;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120420
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ArtistCredit 
{
	@XmlElement(name="name-credit")
	protected List<NameCredit> nameCredits;
	
	public List<NameCredit> getNameCredits()
	{
		return nameCredits;
	}
	
	public void setNameCredits(List<NameCredit> nameCredits)
	{
		this.nameCredits = nameCredits;
	}
}
