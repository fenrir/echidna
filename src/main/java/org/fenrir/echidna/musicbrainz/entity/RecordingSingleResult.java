package org.fenrir.echidna.musicbrainz.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120623
 */
@XmlRootElement(name="metadata")
@XmlAccessorType(XmlAccessType.FIELD)
public class RecordingSingleResult 
{
	@XmlElement(name="recording")
	protected RecordingSearchHit recording;
	
	public RecordingSearchHit getRecording()
	{
		return recording;
	}
	
	public void setRecorging(RecordingSearchHit recording)
	{
		this.recording = recording;
	}
}
