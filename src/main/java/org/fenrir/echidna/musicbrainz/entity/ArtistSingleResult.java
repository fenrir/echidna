package org.fenrir.echidna.musicbrainz.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120427
 */
@XmlRootElement(name="metadata")
@XmlAccessorType(XmlAccessType.FIELD)
public class ArtistSingleResult 
{
	@XmlElement(name="artist")
	protected ArtistSearchHit artist;
	
	public ArtistSearchHit getArtist()
	{
		return artist;
	}
	
	public void setArtist(ArtistSearchHit artist)
	{
		this.artist = artist;
	}
}
