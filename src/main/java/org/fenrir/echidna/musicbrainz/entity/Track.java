package org.fenrir.echidna.musicbrainz.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120624
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Track 
{
	@XmlElement
	protected Integer number;
	@XmlElement
	protected String title;
	@XmlElement
	protected Integer length;
	
	public Integer getNumber() 
	{
		return number;
	}
	
	public void setNumber(Integer number) 
	{
		this.number = number;
	}
	
	public String getTitle() 
	{
		return title;
	}
	
	public void setTitle(String title) 
	{
		this.title = title;
	}
	
	public Integer getLength() 
	{
		return length;
	}
	
	public void setLength(Integer length) 
	{
		this.length = length;
	}
}
