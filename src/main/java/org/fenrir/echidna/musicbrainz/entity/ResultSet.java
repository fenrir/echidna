package org.fenrir.echidna.musicbrainz.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120422
 */
@XmlRootElement(name="metadata")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResultSet 
{
	@XmlElement(name="artist-list")
	protected ArtistResultSet artists;
	@XmlElement(name="release-list")
	protected ReleaseResultSet releases;
	@XmlElement(name="release-group-list")
	protected ReleaseGroupResultSet releaseGroups;
	@XmlElement(name="recording-list")
	protected RecordingResultSet recordings;
	
	public ArtistResultSet getArtists() 
	{
		return artists;
	}

	public void setArtists(ArtistResultSet artists) 
	{
		this.artists = artists;
	}	
	
	public boolean hasArtists()
	{
		return artists!=null && artists.getTotalCount()>0;
	}
	
	public ReleaseResultSet getReleases()
	{
		return releases;
	}
	
	public void setReleases(ReleaseResultSet releases)
	{
		this.releases = releases;
	}
	
	public boolean hasReleases()
	{
		return releases!=null && releases.getTotalCount()>0;
	}
	
	public ReleaseGroupResultSet getReleaseGroups()
	{
		return releaseGroups;
	}
	
	public void setReleaseGroups(ReleaseGroupResultSet releaseGroups)
	{
		this.releaseGroups = releaseGroups;
	}
	
	public boolean hasReleaseGroups()
	{
		return releaseGroups!=null && releaseGroups.getTotalCount()>0;
	}
	
	public RecordingResultSet getRecordings()
	{
		return recordings;
	}
	
	public void setReleaseGroups(RecordingResultSet recordings)
	{
		this.recordings = recordings;
	}
	
	public boolean hasRecordings()
	{
		return recordings!=null && recordings.getTotalCount()>0;
	}
}
