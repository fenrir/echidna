package org.fenrir.echidna.musicbrainz.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.StringUtils;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120420
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class NameCredit 
{
	@XmlAttribute
    protected String joinphrase;
	@XmlElement
	protected ArtistSearchHit artist;
	
	public String getJoinphrase()
	{
		return joinphrase;
	}
	
	public void setJoinphrase(String joinphrase)
	{
		this.joinphrase = joinphrase;
	}
	
	public boolean hasJoinphrase()
	{
		return StringUtils.isNotBlank(joinphrase);
	}
	
	public ArtistSearchHit getArtist()
	{
		return artist;
	}
	
	public void setArtist(ArtistSearchHit artist)
	{
		this.artist = artist;
	}
}
