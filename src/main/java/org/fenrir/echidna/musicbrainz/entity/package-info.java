@javax.xml.bind.annotation.XmlSchema(
	xmlns = { 
		@javax.xml.bind.annotation.XmlNs(prefix="ext",
            namespaceURI="http://musicbrainz.org/ns/ext#-2.0")	 
	},								
	namespace = "http://musicbrainz.org/ns/mmd-2.0#", 
	elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED
)
package org.fenrir.echidna.musicbrainz.entity;
