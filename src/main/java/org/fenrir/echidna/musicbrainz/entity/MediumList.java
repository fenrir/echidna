package org.fenrir.echidna.musicbrainz.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120624
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class MediumList 
{
	@XmlElement(name="track-count")
	protected Integer trackCount;
	@XmlElement(name="medium")
	protected List<Medium> mediums;
	
	public Integer getTrackCount()
	{
		return trackCount;
	}
	
	public void setTrackCount(Integer trackCount)
	{
		this.trackCount = trackCount;
	}
	
	public List<Medium> getMediums()
	{
		return mediums;
	}
	
	public void setMediums(List<Medium> mediums)
	{
		this.mediums = mediums;
	}
}
