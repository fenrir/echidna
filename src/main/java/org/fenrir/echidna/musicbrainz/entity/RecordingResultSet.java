package org.fenrir.echidna.musicbrainz.entity;

import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120406
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class RecordingResultSet 
{
	@XmlElement(name="recording")
	protected List<RecordingSearchHit> recordings;
	@XmlAttribute(name="count")
	protected Integer totalCount;
	@XmlAttribute
	protected Integer offset;
	
	public static RecordingResultSet emptyResultSet()
	{
		RecordingResultSet resultSet = new RecordingResultSet();
		resultSet.recordings = Collections.emptyList();
		resultSet.setTotalCount(0);
		resultSet.setOffset(0);
		
		return resultSet;
	}
	
	public List<RecordingSearchHit> getRecordings() 
	{
		return recordings;
	}
	
	public void setRecordings(List<RecordingSearchHit> recordings) 
	{
		this.recordings = recordings;
	}
	
	public Integer getTotalCount() 
	{
		return totalCount;
	}
	
	public void setTotalCount(Integer totalCount) 
	{
		this.totalCount = totalCount;
	}
	
	public Integer getOffset() 
	{
		return offset;
	}
	
	public void setOffset(Integer offset) 
	{
		this.offset = offset;
	}
}
