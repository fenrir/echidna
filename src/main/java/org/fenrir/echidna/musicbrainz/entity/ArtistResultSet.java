package org.fenrir.echidna.musicbrainz.entity;

import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120406
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ArtistResultSet 
{
	@XmlElement(name="artist")
	protected List<ArtistSearchHit> artists;
	@XmlAttribute(name="count")
	protected Integer totalCount;
	@XmlAttribute
	protected Integer offset;
	
	public static ArtistResultSet emptyResultSet()
	{
		ArtistResultSet resultSet = new ArtistResultSet();
		resultSet.artists = Collections.emptyList();
		resultSet.setTotalCount(0);
		resultSet.setOffset(0);
		
		return resultSet;
	}
	
	public List<ArtistSearchHit> getArtists() 
	{
		return artists;
	}
	
	public void setArtists(List<ArtistSearchHit> artists) 
	{
		this.artists = artists;
	}
	
	public Integer getTotalCount() 
	{
		return totalCount;
	}
	
	public void setTotalCount(Integer totalCount) 
	{
		this.totalCount = totalCount;
	}
	
	public Integer getOffset() 
	{
		return offset;
	}
	
	public void setOffset(Integer offset) 
	{
		this.offset = offset;
	}
}