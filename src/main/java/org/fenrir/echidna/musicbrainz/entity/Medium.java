package org.fenrir.echidna.musicbrainz.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120624
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Medium 
{
	@XmlElement
	protected Integer position;
	@XmlElement
	protected String format;
	@XmlElement(name="track-list")
	protected TrackList trackList;
	
	public Integer getPosition() 
	{
		return position;
	}
	
	public void setPosition(Integer position) 
	{
		this.position = position;
	}
	
	public String getFormat() 
	{
		return format;
	}
	
	public void setFormat(String format) 
	{
		this.format = format;
	}
	
	public TrackList getTrackList() 
	{
		return trackList;
	}
	
	public void setTrackList(TrackList trackList) 
	{
		this.trackList = trackList;
	}
}
