package org.fenrir.echidna.musicbrainz.entity;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120420
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class RecordingSearchHit 
{
	@XmlAttribute
    protected String id;
    @XmlAttribute(name="ext:score")
    protected Integer score;
    @XmlElement
    protected String title;
    @XmlElement
    protected String length;
    @XmlElement(name="artist-credit")
    protected ArtistCredit artistCredit;
    @XmlElement(name="release-list")
    protected List<ReleaseResultSet> releases;
    
    public String getId() 
	{
		return id;
	}
	
	public void setId(String id) 
	{
		this.id = id;
	}
	
	public Integer getScore()
	{
		return score;
	}
	
	public void setScore(Integer score)
	{
		this.score = score;
	}

	public String getTitle() 
	{
		return title;
	}

	public void setTitle(String title) 
	{
		this.title = title;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length)
	{
		this.length = length;
	}
	
	public ArtistCredit getArtistCredit()
	{
		return artistCredit;
	}
	
	public void setArtistCredit(ArtistCredit artistCredit)
	{
		this.artistCredit = artistCredit;
	}

	public List<ReleaseResultSet> getReleases() 
	{
		return releases;
	}

	public void setReleases(List<ReleaseResultSet> releases) 
	{
		this.releases = releases;
	}
}
