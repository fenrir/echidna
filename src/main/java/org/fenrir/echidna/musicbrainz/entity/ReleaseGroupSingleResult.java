package org.fenrir.echidna.musicbrainz.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120424
 */
@XmlRootElement(name="metadata")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReleaseGroupSingleResult 
{
	@XmlElement(name="release-group")
	protected ReleaseGroupSearchHit releaseGroup;
	
	public ReleaseGroupSearchHit getReleaseGroup()
	{
		return releaseGroup;
	}
	
	public void setReleaseGroup(ReleaseGroupSearchHit releaseGroup)
	{
		this.releaseGroup = releaseGroup;
	}
}
