package org.fenrir.echidna.musicbrainz.entity;

import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120406
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ReleaseGroupResultSet 
{
	@XmlElement(name="release-group")
	protected List<ReleaseGroupSearchHit> releaseGroups;
	@XmlAttribute(name="count")
	protected Integer totalCount;
	@XmlAttribute
	protected Integer offset;
	
	public static ReleaseGroupResultSet emptyResultSet()
	{
		ReleaseGroupResultSet resultSet = new ReleaseGroupResultSet();
		resultSet.releaseGroups = Collections.emptyList();
		resultSet.setTotalCount(0);
		resultSet.setOffset(0);
		
		return resultSet;
	}
	
	public List<ReleaseGroupSearchHit> getReleaseGroups() 
	{
		return releaseGroups;
	}
	
	public void setReleaseGroups(List<ReleaseGroupSearchHit> releaseGroups) 
	{
		this.releaseGroups = releaseGroups;
	}
	
	public Integer getTotalCount() 
	{
		return totalCount;
	}
	
	public void setTotalCount(Integer totalCount) 
	{
		this.totalCount = totalCount;
	}
	
	public Integer getOffset() 
	{
		return offset;
	}
	
	public void setOffset(Integer offset) 
	{
		this.offset = offset;
	}
}
