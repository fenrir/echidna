package org.fenrir.echidna.musicbrainz.entity;

import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120420
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ReleaseResultSet 
{
	@XmlElement(name="release")
	protected List<ReleaseSearchHit> releases;
	@XmlAttribute(name="count")
	protected Integer totalCount;
	@XmlAttribute
	protected Integer offset;
	
	public static ReleaseResultSet emptyResultSet()
	{
		ReleaseResultSet resultSet = new ReleaseResultSet();
		resultSet.releases = Collections.emptyList();
		resultSet.setTotalCount(0);
		resultSet.setOffset(0);
		
		return resultSet;
	}
	
	public List<ReleaseSearchHit> getReleases() 
	{
		return releases;
	}
	
	public void setReleases(List<ReleaseSearchHit> releases) 
	{
		this.releases = releases;
	}
	
	public Integer getTotalCount() 
	{
		return totalCount;
	}
	
	public void setTotalCount(Integer totalCount) 
	{
		this.totalCount = totalCount;
	}
	
	public Integer getOffset() 
	{
		return offset;
	}
	
	public void setOffset(Integer offset) 
	{
		this.offset = offset;
	}
}
