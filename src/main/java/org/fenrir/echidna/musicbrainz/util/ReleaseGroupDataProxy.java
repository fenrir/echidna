package org.fenrir.echidna.musicbrainz.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzReleaseGroupWSDAO;
import org.fenrir.echidna.musicbrainz.entity.ReleaseGroupSearchHit;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120706
 */
public class ReleaseGroupDataProxy 
{
	private final Log log = LogFactory.getLog(ReleaseGroupDataProxy.class);
	
	private String id;
	private String name;
	
	private ReleaseGroupSearchHit releaseGroupData;
	
	public ReleaseGroupDataProxy(String id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public String getId()
	{
		log.debug("[ReleaseGroupDataProxy::getId] Obtenint ID de la release " + id);
		
		if(releaseGroupData==null){
			try{
				IMusicBrainzReleaseGroupWSDAO releaseGroupDAO = (IMusicBrainzReleaseGroupWSDAO)EchidnaServiceLocator.getInstance().getInstance(IMusicBrainzReleaseGroupWSDAO.class);
				// Com a mínim existeix una
				releaseGroupData = releaseGroupDAO.findReleaseGroupByReleaseId(id).getReleaseGroups().get(0);
			}
			catch(Exception e){
				log.error("[ReleaseGroupDataProxy::getId] Error en obtenir la ID del release group associat a la release " + id + ": " + e.getMessage(), e);
				
				return null;
			}
		}
		
		return releaseGroupData.getId();
	}
	
	public String getName()
	{
		log.debug("[ReleaseGroupDataProxy::getName] Obtenint Title de la release " + id);
		
		if(releaseGroupData==null){
			return name;
		}
		
		return releaseGroupData.getTitle();
	}
}
