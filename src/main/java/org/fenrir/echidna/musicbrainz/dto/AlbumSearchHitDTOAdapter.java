package org.fenrir.echidna.musicbrainz.dto;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.fenrir.echidna.common.dto.IAlbumSearchHitDTO;
import org.fenrir.echidna.musicbrainz.MusicBrainzConstants;
import org.fenrir.echidna.musicbrainz.entity.ArtistSearchHit;
import org.fenrir.echidna.musicbrainz.entity.NameCredit;
import org.fenrir.echidna.musicbrainz.entity.ReleaseGroupSearchHit;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120703
 */
public class AlbumSearchHitDTOAdapter implements IAlbumSearchHitDTO
{
	private ArtistSearchHit artistData;
	private ReleaseGroupSearchHit album;
	
	public AlbumSearchHitDTOAdapter(ReleaseGroupSearchHit album)
	{
		this.album = album;
	}
	
	public AlbumSearchHitDTOAdapter(ArtistSearchHit artistData, ReleaseGroupSearchHit album)
	{
		this.artistData = artistData;
		this.album = album;
	}
	
	public String getProvider()
	{
		return MusicBrainzConstants.CONTENT_PROVIDER_MUSICBRAINZ;
	}
	
	public String getId()
	{
		return album.getId();
	}
	
	public String getName()
	{
		return album.getTitle();
	}
	
	public String getDescription()
	{
		return album.getDisambiguation();
	}
	
	public String getFirstReleaseDate()
	{
		return album.getFirstReleaseDate();
	}
	
	public String getFirstReleaseYear()
	{
		String releaseDate = getFirstReleaseDate();
		Pattern pattern = Pattern.compile("(\\d{4})");
		Matcher matcher = pattern.matcher(releaseDate);
		if(matcher.find()){
			releaseDate = matcher.group(0);
		}
		
		return releaseDate;
	}

	/**
	 * Retorna la primera ID d'artista que figura als crèdits de la cançó
	 * @return
	 */
	public String getArtistSearchId()
	{
		if(artistData!=null){
			return artistData.getId();
		}
		else{
			if(album.getArtistCredit()!=null){
				return album.getArtistCredit().getNameCredits().get(0).getArtist().getId();
			}
		}
		
		return null;
	}
	
	public String getArtistName()
	{
		if(artistData!=null){
			return artistData.getName();
		}
		
		StringBuilder artistName = new StringBuilder();
		if(album.getArtistCredit()!=null){
			for(NameCredit credit:album.getArtistCredit().getNameCredits()){
				if(credit.hasJoinphrase()){
					StringBuilder aux = new StringBuilder();
					aux.append(credit.getArtist().getName())
						.append(" ").append(credit.getJoinphrase())
						.append(" ").append(artistName);
					
					artistName = aux;
				}
				else{
					if(artistName.length()>0){
						artistName.append(" ");
					}
					artistName.append(credit.getArtist().getName());
				}
			}
		}
		
		return artistName.toString();
	}
}
