package org.fenrir.echidna.musicbrainz.dto;

import org.apache.commons.lang.StringUtils;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.musicbrainz.MusicBrainzConstants;
import org.fenrir.echidna.musicbrainz.entity.Medium;
import org.fenrir.echidna.musicbrainz.entity.NameCredit;
import org.fenrir.echidna.musicbrainz.entity.RecordingSearchHit;
import org.fenrir.echidna.musicbrainz.entity.ReleaseResultSet;
import org.fenrir.echidna.musicbrainz.entity.ReleaseSearchHit;
import org.fenrir.echidna.musicbrainz.util.ReleaseGroupDataProxy;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120705
 */
public class RecordingSearchHitDTOAdapter implements IRecordingSearchHitDTO 
{
	private RecordingSearchHit recording;
	/* Valors corresponents a les dades del release group al que pertany la cançó
	 * Donat que el model de dades del WS de musicbraiz proporciona les releases en comptes del release-group,
	 * s'hauran de calcular en el moment que siugin necessaries.
	 */
	private ReleaseGroupDataProxy releaseGroupData;
	// Valor calculat. Es guarda per no haver de repetir el càlcul
	private Integer order = null;
	
	public RecordingSearchHitDTOAdapter(RecordingSearchHit recording)
	{		
		this.recording = recording;
		String releaseId = recording.getReleases().get(0).getReleases().get(0).getId();
		String releaseName = recording.getReleases().get(0).getReleases().get(0).getTitle();
		this.releaseGroupData = new ReleaseGroupDataProxy(releaseId, releaseName);
	}
	
	public String getProvider() 
	{
		return MusicBrainzConstants.CONTENT_PROVIDER_MUSICBRAINZ; 
	}

	public String getId()
	{
		return recording.getId();
	}
	
	public String getTitle()
	{
		return recording.getTitle();
	}
	
	/**
	 * Retorna la durada de la cançó en milisegons en el cas que es pugui obtenir del servei MusicBrainz.  
	 * @return Long - Durada de la cançó en milisegons;<br/> 
	 * 				  0 si no està disponible o no es pot interpretar
	 */
	public String getLength()
	{
		try{
			// La llargada de la pista es troba en ms
			int length = Integer.parseInt(recording.getLength()) / 1000;
			
			return StringUtils.leftPad(Integer.toString(length / 60), 2, "0") 
					+ ":" + StringUtils.leftPad(Integer.toString(length % 60), 2, "0");
		}
		catch(NumberFormatException e){
			return "--:--";
		}
	}

	/**
	 * Retorna la durada de la cançó en milisegons en el cas que es pugui obtenir del servei MusicBrainz.  
	 * @return Long - Durada de la cançó en milisegons;<br/> 
	 * 				  0 si no està disponible o no es pot interpretar
	 */
	public Long getLengthMillis()
	{
		try{
			long length = Long.parseLong(recording.getLength());
			
			return length;
		}
		catch(NumberFormatException e){
			return 0L;
		}
	}

	/**
	 * Retorna la primera ID d'artista que figura als crèdits de la cançó.
	 * Correspon a la MBID (ID del servei MusicBrainz) de l'artista
	 * @return String - El MBID del primer artista que figura als crèdits de la cançó
	 */
	public String getArtistSearchId()
	{
		if(recording.getArtistCredit()!=null){
			return recording.getArtistCredit().getNameCredits().get(0).getArtist().getId();
		}
		
		return null;
	}

	/**
	 * Retorna el nom de l'artista que figura als crèdits de la cançó. 
	 * En el cas que siguin varis, serà la composició de tots els que hi figuren
	 * @return String - Nom de l'artista o composició d'ells en cas de figurar-ne més d'un
	 */
	public String getArtistName()
	{
		StringBuilder artistName = new StringBuilder();
		if(recording.getArtistCredit()!=null){
			for(NameCredit credit:recording.getArtistCredit().getNameCredits()){
				if(credit.hasJoinphrase()){
					StringBuilder aux = new StringBuilder();
					aux.append(credit.getArtist().getName())
						.append(" ").append(credit.getJoinphrase())
						.append(" ").append(artistName);
					
					artistName = aux;
				}
				else{
					if(artistName.length()>0){
						artistName.append(" ");
					}
					artistName.append(credit.getArtist().getName());
				}
			}
		}
		
		return artistName.toString();
	}
	
	/**
	 * Retorna la ID de l'àlbum associat a la cançó.
	 * Correspon a la MBID (ID del servei MusicBrainz) del release group al que pertany la release associada
	 * @return String - El MBID del release-group associat a la cançó
	 */
	public String getAlbumSearchId()
	{
		return releaseGroupData.getId();		
	}
	
	public String getAlbumName()
	{
		return releaseGroupData.getName();
	}
	
	public Integer getOrder()
	{
		// Només es calcula la primera vegada.
		if(order==null && recording.getReleases()!=null && !recording.getReleases().isEmpty()){
			ReleaseResultSet releaseResultSet = recording.getReleases().get(0);
			if(releaseResultSet.getReleases()!=null && !releaseResultSet.getReleases().isEmpty()){
				ReleaseSearchHit releaseSearchHit = releaseResultSet.getReleases().get(0);
				if(releaseSearchHit.getMediumList()!=null 
						&& !releaseSearchHit.getMediumList().getMediums().isEmpty()){
					Medium medium = releaseSearchHit.getMediumList().getMediums().get(0);
					if(medium.getTrackList()!=null && !medium.getTrackList().getTracks().isEmpty()){
						order = medium.getTrackList().getTracks().get(0).getNumber(); 
					}
				}
			}
		}
		
		return order;
	}
}
