package org.fenrir.echidna.musicbrainz.dto;

import org.fenrir.echidna.common.dto.IArtistSearchHitDTO;
import org.fenrir.echidna.musicbrainz.MusicBrainzConstants;
import org.fenrir.echidna.musicbrainz.entity.ArtistSearchHit;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120623
 */
public class ArtistSearchHitDTOAdapter implements IArtistSearchHitDTO 
{
	private ArtistSearchHit artist;
	
	public ArtistSearchHitDTOAdapter(ArtistSearchHit artist)
	{
		this.artist = artist;
	}
	
	public String getProvider() 
	{
		return MusicBrainzConstants.CONTENT_PROVIDER_MUSICBRAINZ; 
	}
	
	public String getId()
	{
		return artist.getId();
	}
	
	public String getName()
	{
		return artist.getName();
	}
	
	public String getDescription()
	{
		return artist.getDisambiguation();
	}
}
