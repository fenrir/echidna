package org.fenrir.echidna;

import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.HttpServletRequestListener;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;
import org.fenrir.echidna.ui.EchidnaWindow;
import org.fenrir.echidna.ui.model.PlayerModel;
import org.fenrir.echidna.core.entity.User;
import org.fenrir.echidna.core.security.IAuthenticator;
import org.fenrir.echidna.core.security.exception.LoginFailureException;
import org.fenrir.echidna.core.security.listener.ILoginListener;
import org.fenrir.echidna.core.service.IUserAdministrationService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120415
 */
@SuppressWarnings("serial")
public class EchidnaApplication extends Application implements HttpServletRequestListener 
{
	private static ThreadLocal<EchidnaApplication> threadLocal = new ThreadLocal<EchidnaApplication>(); 
	
	@Inject 
	private IAuthenticator authenticator;
	@Inject
	private IUserAdministrationService userManagementService;
	
	private User currentUser;
	private List<ILoginListener> loginListeners = new LinkedList<ILoginListener>();
	
	private PlayerModel playerModel = new PlayerModel();
	
	@Override
	public void init() 
	{
		Window window = createNewWindow();
		setMainWindow(window);
	}

	public Window createNewWindow() 
	{
		final Window window = new EchidnaWindow();

		// remove window on close to avoid memory leaks
		window.addListener(new CloseListener() 
		{
			public void windowClose(CloseEvent e) 
			{
				if(getMainWindow()!=window){
					EchidnaApplication.this.removeWindow(window);
				}
			}
		});

		return window;
	}

	@Override
	public Window getWindow(String name) 
	{
		// See if the window already exists in the application
		Window window = super.getWindow(name);

		// If a dynamically created window is requested, but it does not exist yet, create it.
		if (window == null) {
			// Create the window object.
			window = createNewWindow();
			window.setName(name);

			// Add it to the application as a regular application-level window
			addWindow(window);
		}

		return window;
	}

	/**
	 * @return the current application instance	  	
	 */
	public static EchidnaApplication getInstance() 
	{ 		
		return threadLocal.get(); 	
	} 	 	 

	/**
	 * Set the current application instance 	
	 */
	public static void setInstance(EchidnaApplication application) 
	{
		threadLocal.set(application);
	}      
  
	public void onRequestStart(HttpServletRequest request, HttpServletResponse response) 
	{
		EchidnaApplication.setInstance(this);
	}

	public void onRequestEnd(HttpServletRequest request, HttpServletResponse response) 
	{
		threadLocal.remove();
	}
	
	public void setAuthenticator(IAuthenticator authenticator)
	{
		this.authenticator = authenticator;
	}
	
	public User getCurrentUser()
	{
		return currentUser;
	}
	
	public void addLoginListener(ILoginListener listener)
	{
		loginListeners.add(listener);
	}
	
	public void removeLoginListener(ILoginListener listener)
	{
		loginListeners.remove(listener);
	}
	
	public void login(String user, String password) throws LoginFailureException
    {
		if(!authenticator.authenticate(user, password)){
			throw new LoginFailureException("Usuari o contrasenya no valids");
		}

		currentUser = userManagementService.findUserByName(user);
		for(ILoginListener listener:loginListeners){
			listener.onLogin(currentUser);
		}
    }
	
	public void logout()
	{
		for(ILoginListener listener:loginListeners){
			listener.onLogout(currentUser);
		}
		currentUser = null;
	}
	
	public PlayerModel getPlayerModel()
	{
		return playerModel;
	}
}
