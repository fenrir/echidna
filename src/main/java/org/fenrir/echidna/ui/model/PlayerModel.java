package org.fenrir.echidna.ui.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.collections.map.LinkedMap;
import org.apache.commons.lang.StringUtils;
import org.fenrir.echidna.common.dto.ILinkDTO;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120911
 */
public class PlayerModel 
{
	private LinkedMap playerEntries = new LinkedMap();
	private String currentSongId;
	private int currentSongIndex = 0;
	private PlayerStatus playerStatus;
	private PlayerMode playerMode = PlayerMode.normal;
	private boolean lyricsVisible;
	private List<PlayerModel.PlayerModelListener> listeners = new LinkedList<PlayerModel.PlayerModelListener>();
	
	@SuppressWarnings("unchecked")
	public Collection<PlayerEntry> getPlayerEntries()
	{
		return playerEntries.values();
	}
	
	public int getPlayerEntriesSize()
	{
		return playerEntries.size();
	}
	
	public PlayerEntry getPlayerEntry(String id)
	{
		return (PlayerEntry)playerEntries.get(id);
	}
	
	public PlayerEntry getCurrentPlayerEntry()
	{
		if(currentSongId!=null){
			return getPlayerEntry(currentSongId);
		}
		
		return null;
	}
	
	public void addPlayerEntry(PlayerEntry entry)
	{
		playerEntries.put(entry.id, entry);
		
		for(PlayerModelListener listener:listeners){
			listener.entryAdded(entry);
		}
	}
	
	public void removePlayerEntry(String playerEntryId)
	{
		boolean currentSongChanged = false;
		if(currentSongId!=null && currentSongId.equals(playerEntryId)){
			currentSongId = getNextSongId();
			currentSongChanged = true;
		}
		PlayerEntry entry = (PlayerEntry)playerEntries.remove(playerEntryId);
		
		for(PlayerModelListener listener:listeners){
			listener.entryRemoved(entry, currentSongChanged);
		}
	}
	
	public void clearPlayerEntries()
	{
		playerEntries.clear();
		currentSongId = null;
		currentSongIndex = -1;
		
		for(PlayerModelListener listener:listeners){
			listener.modelCleared();
		}
	}
	
	public void notifyPlayerEntryModified(String id)
	{
		PlayerEntry entry = getPlayerEntry(id);
		for(PlayerModelListener listener:listeners){
			listener.entryModified(entry);
		}
	}
	
	public boolean isCurrentSongLastEntry()
	{
		return currentSongIndex!=-1 && currentSongIndex==playerEntries.size()-1; 
	}
	
	public String getCurrentSongId()
	{
		return currentSongId;
	}
	
	public void setCurrentSongId(String currentSongId)
	{
		if(!playerEntries.containsKey(currentSongId)){
			throw new IllegalArgumentException("La id especificada no existeix a la llista");
		}
		
		this.currentSongId = currentSongId;
		this.currentSongIndex = playerEntries.indexOf(currentSongId);
	}
	
	public String getFirstSongId()
	{
		return (String)playerEntries.firstKey();
	}
	
	public String getLastSongId()
	{
		return (String)playerEntries.lastKey();
	}
	
	public String getNextSongId()
	{
		if(currentSongId!=null){
			return (String)playerEntries.nextKey(currentSongId);
		}
		else if(!playerEntries.isEmpty()){
			return (String)playerEntries.firstKey();
		}
		
		return null;
	}
	
	public String getPreviousSongId()
	{
		if(currentSongId!=null){
			return (String)playerEntries.previousKey(currentSongId);
		}
		else if(!playerEntries.isEmpty()){
			return (String)playerEntries.lastKey();
		}
		
		return null;
	}
	
	public PlayerStatus getPlayerStatus()
	{
		return playerStatus;
	}
	
	public void setPlayerStatus(PlayerStatus playerStatus)
	{
		PlayerStatus oldStatus = this.playerStatus;
 		this.playerStatus = playerStatus;
		for(PlayerModelListener listener:listeners){
			listener.playerStatusChanged(playerStatus, oldStatus);
		}
	}
	
	public PlayerMode getPlayerMode()
	{
		return playerMode;
	}
	
	public void setPlayerMode(PlayerMode playerMode)
	{
		this.playerMode = playerMode;
	}
	
	public boolean isLyricsVisible()
	{
		return lyricsVisible;
	}
	
	public void setLyricsVisible(boolean lyricsVisible)
	{
		this.lyricsVisible = lyricsVisible;
	}
	
	public void addListener(PlayerModelListener listener)
	{
		listeners.add(listener);
	}
	
	public void removeListener(PlayerModelListener listener)
	{
		listeners.remove(listener);
	}
	
	public static class PlayerEntry implements Serializable
	{
		private static final long serialVersionUID = 5403052030372475719L;

		// Id de la cançó. Té la forma <provider>@<providerId>
		private String id;
		private String song;
		private String album;
		private String albumSearchId;
		private String artist;
		private String artistSearchId;
		private String length;
		private ILinkDTO link;
		private boolean linkUpvoted = false;
		
		public String getId()
		{
			return id;
		}
		
		public void setId(String id)
		{
			this.id = id;
		}
		
		public String getProvider()
		{
			return StringUtils.substringBefore(id, "@");
		}
		
		public String getProviderId()
		{
			return StringUtils.substringAfter(id, "@");
		}
		
		public String getSong() 
		{
			return song;
		}
		
		public void setSong(String song) 
		{
			this.song = song;
		}
		
		public String getAlbum() 
		{
			return album;
		}
		
		public void setAlbum(String album) 
		{
			this.album = album;
		}
		
		public String getAlbumSearchId() 
		{
			return albumSearchId;
		}

		public void setAlbumSearchId(String albumSearchId) 
		{
			this.albumSearchId = albumSearchId;
		}

		public String getArtist() 
		{
			return artist;
		}
		
		public void setArtist(String artist) 
		{
			this.artist = artist;
		}

		public String getArtistSearchId() 
		{
			return artistSearchId;
		}

		public void setArtistSearchId(String artistSearchId) 
		{
			this.artistSearchId = artistSearchId;
		}
		
		public String getLength()
		{
			return length;
		}
		
		public void setLength(String length)
		{
			this.length = length;
		}

		public ILinkDTO getLink()
		{
			return link;
		}
		
		public void setLink(ILinkDTO link)
		{
			this.link = link;
			// Si es canvia el link es reestableix la opció de fer upvote
			setLinkUpvoted(false);
		}
		
		public boolean isLinkUpvoted()
		{
			return linkUpvoted;
		}
		
		public void setLinkUpvoted(boolean linkUpvoted)
		{
			this.linkUpvoted = linkUpvoted;
		}
	}
	
	public static interface PlayerModelListener
	{
		public void entryAdded(PlayerEntry entry);
		public void entryModified(PlayerEntry entry);
		public void entryRemoved(PlayerEntry entry, boolean currentSongChanged);
		public void modelCleared();
		
		public void playerStatusChanged(PlayerStatus newStatus, PlayerStatus oldStatus);
	}
	
	public static enum PlayerStatus
	{
		stopped,
		paused,
		playing
	}
	
	public static enum PlayerMode
	{
		normal,
		loop,
		shuffle
	}
}
