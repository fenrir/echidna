package org.fenrir.echidna.ui.widget;

import com.vaadin.data.Item;
import com.vaadin.ui.Button;
import com.vaadin.ui.Table;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Runo;
import org.fenrir.echidna.ui.EchidnaWindow;
import org.fenrir.echidna.ui.view.AlbumView;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120623
 */
@SuppressWarnings("serial")
public class AlbumLinkTableColumnGenerator implements Table.ColumnGenerator 
{
	private String albumNameColumnId;
	private String albumSearchColumnId;
	
	public AlbumLinkTableColumnGenerator(String albumNameColumnId, String albumSearchColumnId)
	{
		this.albumNameColumnId = albumNameColumnId;
		this.albumSearchColumnId = albumSearchColumnId;
	}
	
	public Object generateCell(final Table source, Object itemId, Object columnId) 
	{
		Item item = source.getItem(itemId);
		String albumName = (String)item.getItemProperty(albumNameColumnId).getValue();
		Button bLink = new Button(albumName);
		bLink.addStyleName(Runo.BUTTON_LINK);
		bLink.setData(itemId);
		bLink.addListener(new Button.ClickListener()
		{
			public void buttonClick(ClickEvent event) 
			{
				String rowID = (String)event.getButton().getData();
				Item item = source.getItem(rowID);
				String provider = (String)item.getItemProperty("provider").getValue();
				String albumSearchId = (String)item.getItemProperty(albumSearchColumnId).getValue();
				String albumName = (String)item.getItemProperty(albumNameColumnId).getValue();
				AlbumView view = new AlbumView(provider, albumSearchId);
				((EchidnaWindow)source.getWindow()).openTab(view, albumName);
	        } 
		});
		
		return bLink;
	}
}
