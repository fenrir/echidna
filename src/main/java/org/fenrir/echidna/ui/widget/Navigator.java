package org.fenrir.echidna.ui.widget;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;

/**
 * TODO 1.0 javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120406
 */
public class Navigator extends CustomComponent
{
	private static final long serialVersionUID = -4832832165312322781L;

	private AbstractLayout mainLayout;
	private Map<Class<?>, Navigator.View> views = new HashMap<Class<?>, Navigator.View>();
	private Class<?> currentViewKey;
	
	private List<Navigator.NavigationListener> listeners = new LinkedList<Navigator.NavigationListener>();
	
	public Navigator()
	{
		setSizeFull();
		mainLayout = new HorizontalLayout();
		mainLayout.setSizeFull();
		// Només es pot especificar una vegada
		setCompositionRoot(mainLayout);
	}

	public void addView(Class<?> viewClass, Navigator.View view)
	{
		view.setNavigator(this);
		views.put(viewClass, view);
	}
	
	public void removeView(Class<?> viewClass)
	{
		views.remove(viewClass);
	}

	public Navigator.View getCurrentView()
	{
		if(currentViewKey!=null){
			return views.get(currentViewKey);
		}
		
		return null;
	}
	
	public void startNavigation(Class<?> view)
	{
		Navigator.View objView = views.get(view);
		views.put(view, objView);
		mainLayout.addComponent(objView);
		currentViewKey = view;
		objView.navigationEntered();
	}
	
	public void navigateTo(Class<?> nextView)
	{
		Navigator.View objCurrentView = null;
		if(currentViewKey!=null){
			objCurrentView = views.get(currentViewKey);
			objCurrentView.navigationLeft();
		}
		Navigator.View objNextView = views.get(nextView);
		mainLayout.removeAllComponents();
		mainLayout.addComponent(objNextView);
		for(NavigationListener listener:listeners){
			listener.navigatorViewChanged(objCurrentView, objNextView);
		}
		currentViewKey = nextView;
		objNextView.navigationEntered();
	}

	public void addNavigationListener(Navigator.NavigationListener listener)
	{
		listeners.add(listener);
	}
	
	public void removeNavigationListener(Navigator.NavigationListener listener)
	{
		listeners.remove(listener);
	}
	
	public static interface NavigationListener 
	{
		public void navigatorViewChanged(View oldView, View newView);
	}
	
	public static abstract class View extends CustomComponent 
	{
		private static final long serialVersionUID = 6360788293334268451L;
		
		protected Navigator navigator;
		
		public Navigator getNavigator()
		{
			return navigator;
		}
		
		void setNavigator(Navigator navigator)
		{
			this.navigator = navigator;
		}

		public abstract String getId();
		public abstract String getTitle();
		public abstract void navigationEntered();
		public abstract void navigationLeft(); 
	}
}
