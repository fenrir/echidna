package org.fenrir.echidna.ui.widget;

import org.vaadin.youtubeplayer.YouTubePlayer;
import org.vaadin.youtubeplayer.client.ui.VYouTubePlayer;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.common.dto.ILinkDTO;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;
import org.fenrir.echidna.core.service.IContentAdministrationService;
import org.fenrir.echidna.core.service.IContentSearchService;
import org.fenrir.echidna.ui.model.PlayerModel;
import org.fenrir.echidna.ui.model.PlayerModel.PlayerEntry;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120911
 */
public class Player implements YouTubePlayer.PlayerListener 
{
	private static final long serialVersionUID = -3667274234184721902L;

	private final Log log = LogFactory.getLog(Player.class);
	
	private AbstractLayout playerLayout;
	private YouTubePlayer player;
	private PlayerModel playerModel;
	
	/* Controls */
	private Label lSongTitle;
	private Label lTime;
	private Button bPlay;
	
	private IContentSearchService contentSearchService;
	private IContentAdministrationService contentAdministrationService;
	
	public Player()
	{
		playerLayout = createContents();
		player = createYoutubePlayer();
		
		contentSearchService = (IContentSearchService)EchidnaServiceLocator.getInstance().getInstance(IContentSearchService.class);
		contentAdministrationService = (IContentAdministrationService)EchidnaServiceLocator.getInstance().getInstance(IContentAdministrationService.class);
	}

	private AbstractLayout createContents()
	{
		VerticalLayout playerLayout = new VerticalLayout();
		playerLayout.setSpacing(true);
		
		/* Títol de la cançó actual */
		lSongTitle = new Label("-- --");
		lSongTitle.addStyleName("song-label");
		lSongTitle.setHeight("16px");
		playerLayout.addComponent(lSongTitle);
		playerLayout.setComponentAlignment(lSongTitle, Alignment.BOTTOM_LEFT);
		
		/* Botonera del reproductor */
		HorizontalLayout layout = new HorizontalLayout();
		layout.setSpacing(true);
		layout.setWidth("100%");		
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		// >
		bPlay = new Button(">", new Button.ClickListener()
		{
			private static final long serialVersionUID = -2714671724609993247L;

			public void buttonClick(Button.ClickEvent event) 
			{
				if(player.isPlaying()){
					player.pause();
				}
				else{
					if(player.getVideoId()==null){
						String currentSongId = playerModel.getCurrentSongId();
						if(currentSongId!=null){
							playSong(currentSongId);
						}
					}
					else{
						player.play();
					}
				}
            }
		});
		bPlay.setWidth("50px");
		buttonsLayout.addComponent(bPlay);
		// <<
		Button bRewind = new Button("<<", new Button.ClickListener() 
		{
			private static final long serialVersionUID = -2504166626556297693L;

			public void buttonClick(ClickEvent event) 
			{
				playPreviousSong();
			}
		});
		bRewind.setWidth("50px");
		buttonsLayout.addComponent(bRewind);
		// >>
		Button bForward = new Button(">>", new Button.ClickListener() 
		{
			private static final long serialVersionUID = -9177071998011263137L;

			public void buttonClick(ClickEvent event) 
			{
				playNextSong();
			}
		});
		bForward.setWidth("50px");
		buttonsLayout.addComponent(bForward);
		layout.addComponent(buttonsLayout);
		layout.setComponentAlignment(buttonsLayout, Alignment.MIDDLE_LEFT);
		//Indicador de progrés
		ProgressIndicator progressIndicator = new ProgressIndicator();
		progressIndicator.setWidth("100%");
		layout.addComponent(progressIndicator);
		layout.setComponentAlignment(progressIndicator, Alignment.MIDDLE_LEFT);
		layout.setExpandRatio(progressIndicator, 2);
		// Indicador de la durada de la cançó
		lTime = new Label("--:-- / --:--");
		// Per tal que no salti la linea al ser massa llarg
		lTime.setSizeUndefined();
		layout.addComponent(lTime);
		layout.setComponentAlignment(lTime, Alignment.MIDDLE_LEFT);
		// S'afegeix al layout principal
		playerLayout.addComponent(layout);
		
		return playerLayout;
	}
	
	private YouTubePlayer createYoutubePlayer()
	{
		YouTubePlayer player = new YouTubePlayer();
		player.setImmediate(true);
		player.addStateListener(this);
		// Mida mínima per tal que es pugui reproduir sense veure el video
		player.setWidth("1px");
		player.setHeight("1px");
		
		return player;
	}

	public void setPlayerModel(PlayerModel playerModel)
	{
		this.playerModel = playerModel;
	}
	
	public YouTubePlayer getPlayerComponent()
	{
		return player;
	}
	
	public AbstractLayout getPlayerLayout()
	{
		return playerLayout;
	}
	
	public void playerStateChanged(YouTubePlayer player, int state, int oldState) 
	{
		// Si s'ha acabat la reproducció de la cançó actual es passa a la següent
		if(VYouTubePlayer.STATE_ENDED==state){
			playNextSong();
			/* S'indica el nou estat del reproductor. El model s'encarregarà de comunicar el canvi
			 * En aquest cas es fa per forçar el refresc de la llista de reproducció
			 */
			playerModel.setPlayerStatus(PlayerModel.PlayerStatus.playing);
		}
		else if(VYouTubePlayer.STATE_PLAYING==state){
			// S'indica el nou estat del reproductor. El model s'encarregarà de comunicar el canvi
			playerModel.setPlayerStatus(PlayerModel.PlayerStatus.playing);
			bPlay.setCaption("||");
			
			PlayerEntry currentEntry = playerModel.getCurrentPlayerEntry(); 
			if(!currentEntry.isLinkUpvoted()){
				try{
					IRecordingSearchHitDTO recordingDTO = contentSearchService.findRecordingByProviderData(currentEntry.getProvider(), currentEntry.getProviderId());
					contentAdministrationService.upvoteSongLink(recordingDTO, currentEntry.getLink());
					currentEntry.setLinkUpvoted(true);
				}
				catch(BussinessException e){
					log.error("[Player::playerStateChanged] Error en promocionar el link de la cançó: " + e.getMessage());
				}
			}
		}
		else if(VYouTubePlayer.STATE_PAUSED==state){
			// S'indica el nou estat del reproductor. El model s'encarregarà de comunicar el canvi
			playerModel.setPlayerStatus(PlayerModel.PlayerStatus.paused);
			bPlay.setCaption(">");
		}
	}
	
	public void playerVolumeChanged(YouTubePlayer player, int volume) 
	{
		
	}
	
	public void playerTimeChanged(YouTubePlayer player, float secs) 
	{
		
	}
	
	public void playerError(YouTubePlayer player, int errorCode) 
	{
//				getWindow().showNotification("Error reproduir la cançó; Codi d'error " + errorCode, Notification.TYPE_ERROR_MESSAGE);
	}
	
	public boolean playSong(String songId)
	{
		if(songId==null){
			throw new IllegalArgumentException("La id de la cançó a reproduir no pot ser nula");
		}

		playerModel.setCurrentSongId(songId);
		PlayerModel.PlayerEntry playerEntry = playerModel.getCurrentPlayerEntry();
		ILinkDTO link = (ILinkDTO)playerEntry.getLink();
		if(link!=null){
			player.play(link.getLinkSearchId());
			// S'actualitzen les dades de la cançó actual al reproductor
			String strSongTitle = playerEntry.getSong();
			String strAlbum = playerEntry.getAlbum();
			String strArtist = playerEntry.getArtist();
			lSongTitle.setValue(strSongTitle + " -- " + strAlbum + " -- " + strArtist);
			lTime.setValue(player.getCurrentTime() + " / " + player.getDuration());
			
			return true;
		}
		
		return false;
	}
	
	public boolean playNextSong()
	{
		boolean playing = false;
		
		// Variable destinada a controlar els bucles
		String currentSongId = playerModel.getCurrentSongId();
		String nextSongId = null;
		if(currentSongId!=null){
			nextSongId = playerModel.getNextSongId();
		}
		else{
			nextSongId = playerModel.getFirstSongId(); 
		}
		// Es controla el cas que sigui la última cançó de la llista
		if(nextSongId==null && PlayerModel.PlayerMode.loop.equals(playerModel.getPlayerMode())){
			nextSongId = playerModel.getFirstSongId();
		}
		// S'itera fins trobar una id valida
		while(nextSongId!=null && !currentSongId.equals(nextSongId) && !playing){
			playing = playSong(nextSongId);
			nextSongId = playerModel.getNextSongId();
			if(nextSongId==null && PlayerModel.PlayerMode.loop.equals(playerModel.getPlayerMode())){
				nextSongId = playerModel.getFirstSongId();
			}
		}
		
		return playing;
	}

	public boolean playPreviousSong()
	{
		boolean playing = false;
		
		String currentSongId = playerModel.getCurrentSongId();
		String previousSongId = null;
		if(currentSongId!=null){
			previousSongId = playerModel.getPreviousSongId();
		}
		// Es controla el cas que sigui la primera cançó de la llista
		if(previousSongId==null){
			previousSongId = playerModel.getLastSongId();
		}
		// S'itera fins trobar una id valida
		while(previousSongId!=null && !currentSongId.equals(previousSongId) && !playing){
			playing = playSong(previousSongId);
			previousSongId = playerModel.getNextSongId();
			if(previousSongId==null){
				previousSongId = playerModel.getFirstSongId();
			}
		}
		
		return playing;
	}
}
