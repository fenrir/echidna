package org.fenrir.echidna.ui.widget;

import com.vaadin.data.Item;
import com.vaadin.ui.Button;
import com.vaadin.ui.Table;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Runo;
import org.fenrir.echidna.ui.EchidnaWindow;
import org.fenrir.echidna.ui.view.ArtistView;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120706
 */
@SuppressWarnings("serial")
public class ArtistLinkTableColumnGenerator implements Table.ColumnGenerator 
{
	private String artistNameColumnId;
	private String artistSearchColumnId;
	
	public ArtistLinkTableColumnGenerator(String artistNameColumnId, String artistSearchColumnId)	
	{
		this.artistNameColumnId = artistNameColumnId;
		this.artistSearchColumnId = artistSearchColumnId;
	}
	
	public Object generateCell(final Table source, Object itemId, Object columnId) 
	{
		Item item = source.getItem(itemId);
		String artistName = (String)item.getItemProperty(artistNameColumnId).getValue();
		Button bLink = new Button(artistName);
		bLink.addStyleName(Runo.BUTTON_LINK);
		bLink.setData(itemId);
		bLink.addListener(new Button.ClickListener()
		{
			public void buttonClick(ClickEvent event) 
			{
				String rowID = (String)event.getButton().getData();
				Item item = source.getItem(rowID);
				String provider = (String)item.getItemProperty("provider").getValue();
				String artistSearchId = (String)item.getItemProperty(artistSearchColumnId).getValue();
				String artistName = (String)item.getItemProperty(artistNameColumnId).getValue();
				ArtistView view = new ArtistView(provider, artistSearchId);
				((EchidnaWindow)source.getWindow()).openTab(view, artistName);
	        } 
		});
		
		return bLink;
	}
}
