package org.fenrir.echidna.ui.widget;

import java.util.ArrayList;
import java.util.List;
import com.vaadin.data.Item;
import com.vaadin.ui.Button;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.BaseTheme;
import org.fenrir.echidna.ui.EchidnaWindow;
import org.fenrir.echidna.ui.model.PlayerModel;
import org.fenrir.echidna.ui.view.LinkAdministrationView;
import org.fenrir.echidna.common.dto.ILinkDTO;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120909
 */
public class PlaylistTableEntryOptions extends VerticalLayout 
{
	private static final long serialVersionUID = 6036039861335708652L;
	
	private PlayerModel playerModel;
	private Table playlistTable;
	private Object songId;
	
	public PlaylistTableEntryOptions(PlayerModel playerModel, Table playlistTable, Object songId)
	{
		this.playerModel = playerModel;
		this.playlistTable = playlistTable;
		this.songId = songId;
		
		createContents();
	}
	
	private void createContents()
	{
		setSizeUndefined();
		
		/* Administració de links */
		Button bLinks = new Button("Administrar enllaços");
		bLinks.addStyleName(BaseTheme.BUTTON_LINK);
		bLinks.setData(songId);
		bLinks.addListener(new Button.ClickListener()
		{
			private static final long serialVersionUID = -5825447784875477130L;

			public void buttonClick(ClickEvent event) 
			{
				String rowID = (String)event.getButton().getData();
				Item item = playlistTable.getItem(rowID);
				String songTitle = (String)item.getItemProperty("song").getValue();
				// S'emplena la finestra de diàleg amb els links inicials
				ILinkDTO link = (ILinkDTO)item.getItemProperty("link").getValue();
				List<ILinkDTO> links = new ArrayList<ILinkDTO>();
				if(link!=null){
					links.add(link);
				}
//				LinkAdministrationDialog linkDialog = new LinkAdministrationDialog(links);
//				getWindow().addWindow(linkDialog);
				LinkAdministrationView view = new LinkAdministrationView(songTitle, rowID, links);
				((EchidnaWindow)playlistTable.getWindow()).openTab(view, "Video - " + songTitle);
	        } 
		});
		addComponent(bLinks);
		/* Eliminar cançó de la llista */
		Button bRemove = new Button("Esborrar de la llista");
		bRemove.addStyleName(BaseTheme.BUTTON_LINK);
		bRemove.setData(songId);
		bRemove.addListener(new Button.ClickListener()
		{
			private static final long serialVersionUID = 650159547451333726L;

			public void buttonClick(ClickEvent event) 
			{
				String rowID = (String)event.getButton().getData();
				// S'esborra la cançó sel.leccionada del model. L'event d'esborrat s'encarregarà de treure el registre de la taula
				playerModel.removePlayerEntry(rowID);
			}
		});
		addComponent(bRemove);
	}
	
	/**
	 * TODO public void setUserOptionsEnabled(boolean enabled)
	 * @param enabled
	 */
	public void setUserOptionsEnabled(boolean enabled)
	{
		
	}
}
