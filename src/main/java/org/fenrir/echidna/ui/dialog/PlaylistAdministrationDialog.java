package org.fenrir.echidna.ui.dialog;

import com.vaadin.terminal.UserError;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import org.apache.commons.lang.StringUtils;
import org.fenrir.echidna.EchidnaApplication;
import org.fenrir.echidna.core.entity.User;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;
import org.fenrir.echidna.core.service.IUserContentAdministrationService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120530
 */
@SuppressWarnings("serial")
public class PlaylistAdministrationDialog extends Window implements Button.ClickListener 
{
	private TextField inputName;
	
	public PlaylistAdministrationDialog()
	{
		AbstractLayout contentLayout = createContents();
		setContent(contentLayout);
		
		// Títol de la finestra
		setCaption("Crear una nova llista de reproducció");
		// Mida de la finestra
		setWidth("400px");
		setHeight("200px");
		setModal(true);
		// Es centra a la finestra del navegador 
		center();
	}
	
	private AbstractLayout createContents()
	{
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		mainLayout.setMargin(false, true, true, true);
		
		FormLayout formLayout = new FormLayout();
		formLayout.setSizeUndefined();
		// Input del nom de la llista
		inputName = new TextField("Nom:");
		inputName.setRequired(true);
		inputName.setRequiredError("Ha d'indicar un nom per la llista de reproducció");
		formLayout.addComponent(inputName);
		mainLayout.addComponent(formLayout);
		mainLayout.setComponentAlignment(formLayout, Alignment.MIDDLE_CENTER);
		mainLayout.setExpandRatio(formLayout, 2.0f);
		// Botó de creació de la llista
		Button bCreate = new Button("Crear", this);
		mainLayout.addComponent(bCreate);
		mainLayout.setComponentAlignment(bCreate, Alignment.BOTTOM_RIGHT);
		mainLayout.setExpandRatio(bCreate, 1.0f);
		
		return mainLayout;
	}
	
	public void buttonClick(Button.ClickEvent event)
	{
		String name = (String)inputName.getValue(); 
		User user = ((EchidnaApplication)getApplication()).getCurrentUser();
		
		/* Validacions */
		if(StringUtils.isBlank(name)){
			inputName.setComponentError(new UserError("El camp nom no es pot deixar en blanc"));
			return;
		}
		
		IUserContentAdministrationService playlistManagementService = (IUserContentAdministrationService)EchidnaServiceLocator.getInstance().getInstance(IUserContentAdministrationService.class);
		playlistManagementService.createPlaylist(name, user);
		
		// Es tanca la finestra
		getApplication().getMainWindow().removeWindow(this);
	}
}
