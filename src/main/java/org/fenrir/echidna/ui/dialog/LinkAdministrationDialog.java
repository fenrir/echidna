package org.fenrir.echidna.ui.dialog;

import java.util.List;
import com.vaadin.terminal.ClassResource;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Runo;
import org.apache.commons.lang.StringUtils;
import org.fenrir.echidna.common.dto.GenericResultSetDTO;
import org.fenrir.echidna.common.dto.ILinkDTO;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;
import org.fenrir.echidna.core.service.IContentSearchService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120820
 */
@SuppressWarnings("serial")
public class LinkAdministrationDialog extends Window 
{
	private List<ILinkDTO> links;

	private Button bSearch;
	
	public LinkAdministrationDialog(List<ILinkDTO> links)
	{
		this.links = links;
		
		AbstractLayout layout = createContents();
		// S'ha d'afegir el layout a un panel per poder tenir barres de desplaçament
		Panel pContents = new Panel();
		pContents.addStyleName(Runo.PANEL_LIGHT);
		pContents.setContent(layout);
		setContent(pContents);
		
		// Títol de la finestra
		setCaption("Administrar links de la cançó");
		// Mida de la finestra
		setWidth("600px");
		setHeight("400px");
		setModal(true);
		// Es centra a la finestra del navegador 
		center();
	}
	
	private AbstractLayout createContents()
	{		
		// El tamany inicial de la taula dependrà del nombre de links inicials + la fila pel camp de cerca
		final GridLayout mainLayout = new GridLayout(2, links.size() + 1);
		mainLayout.setSizeFull();
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		/* Input de cerca */
		HorizontalLayout searchLayout = new HorizontalLayout();
		searchLayout.setWidth("100%");
		final TextField inputSearch = new TextField();
		inputSearch.setWidth("100%");
		searchLayout.addComponent(inputSearch);
		searchLayout.setExpandRatio(inputSearch, 2);
		bSearch = new Button();
		bSearch.addListener(new Button.ClickListener() 
		{
			public void buttonClick(ClickEvent event) 
			{
				String searchTerm = (String)inputSearch.getValue();
				if(StringUtils.isNotBlank(searchTerm)){
					IContentSearchService contentSearchService = (IContentSearchService)EchidnaServiceLocator.getInstance().getInstance(IContentSearchService.class);
					try{
						GenericResultSetDTO<ILinkDTO> resultset = contentSearchService.findLinks(searchTerm);
						if(resultset.size()>0){
							// S'esborren els resultats anteriors
							for(int i=1; i<mainLayout.getRows(); i++){
								mainLayout.removeRow(i);
							}
							// S'afegeixen els resultats nous
							mainLayout.setRows(resultset.size() + 1);
							int row = 1;
							for(ILinkDTO link:resultset.getResultList()){
								// Check de sel.lecció de link
								CheckBox checkSelected = new CheckBox();
								checkSelected.setData(link);
								mainLayout.addComponent(checkSelected, 0, row);
								
								AbstractLayout linkLayout = createLinkEntryLayout(link);
								linkLayout.setWidth("100%");
								mainLayout.addComponent(linkLayout, 1, row);
								mainLayout.setRowExpandRatio(row, 1);
								
								row++;
							}
						}
					}
					catch(BussinessException e){
						// TODO v0.1 Log + mostrar error
					}
				}
			}
		});
		searchLayout.addComponent(bSearch);
		mainLayout.addComponent(searchLayout, 1, 0);
		/* Links */
		int row = 1;
		for(ILinkDTO link:links){
			// Check de sel.lecció de link
			CheckBox checkSelected = new CheckBox();
			checkSelected.setData(link);
			mainLayout.addComponent(checkSelected, 0, row);
			
			AbstractLayout linkLayout = createLinkEntryLayout(link);
			linkLayout.setWidth("100%");
			mainLayout.addComponent(linkLayout, 1, row);
			mainLayout.setRowExpandRatio(row, 1);
			
			row++;
		}
		/* Configuració mida de les columnes */
		mainLayout.setColumnExpandRatio(1, 2);
		
		return mainLayout;
	}
	
	private AbstractLayout createLinkEntryLayout(ILinkDTO link)
	{
		GridLayout layout = new GridLayout(2, 3);
		layout.setSpacing(true);
		/* Thumbnail */
		ExternalResource thumbnail = new ExternalResource(link.getThumbnailUrl());
		Embedded embedded = new Embedded();
		embedded.setType(Embedded.TYPE_IMAGE);
		embedded.setMimeType("/image/jpg");
		embedded.setSource(thumbnail);
		layout.addComponent(embedded, 0, 0, 0, 1);
		/* Títol */
		Label lTitle = new Label(link.getTitle());
		layout.addComponent(lTitle, 1, 0);
		/* Descripció */
		Label lDescription = new Label(link.getDescription());
		layout.addComponent(lDescription, 1, 1);
		/* Separador */
		layout.addComponent(new Label("<hr/>",Label.CONTENT_XHTML), 0, 2, 1, 2);
		/* Configuració mida de les columnes */
		layout.setColumnExpandRatio(1, 2);

		return layout;
	}
	
	@Override
	public void attach() 
	{
		super.attach();
		
		bSearch.setIcon(new ClassResource("/org/fenrir/echidna/web/images/search_16.png", getApplication()));
	}
}
