package org.fenrir.echidna.ui;

import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.themes.Runo;
import org.fenrir.echidna.EchidnaApplication;
import org.fenrir.echidna.ui.widget.Navigator;
import org.fenrir.echidna.ui.widget.Navigator.View;
import org.fenrir.echidna.ui.widgetset.SplitButton;
import org.fenrir.echidna.ui.view.BrowserView;
import org.fenrir.echidna.ui.view.CreateUserView;
import org.fenrir.echidna.ui.view.ExplorerView;
import org.fenrir.echidna.ui.view.LoginView;
import org.fenrir.echidna.ui.view.PlayerView;
import org.fenrir.echidna.ui.view.WelcomeView;
import org.fenrir.echidna.core.entity.User;
import org.fenrir.echidna.core.security.listener.ILoginListener;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120721
 */
@SuppressWarnings("serial")
public class EchidnaWindow extends Window implements Navigator.NavigationListener, ILoginListener
{
	private HorizontalLayout headerLayout;
	private HorizontalLayout loginPanel;
	private HorizontalLayout userPanel;
	private AbstractLayout viewLayout;

	private Label lUserName;
	private Navigator navigator;
	private TabSheet tabPanel;
	private Tab navigatorTab;
	private ExplorerView explorerView;
	
    public EchidnaWindow() 
    {
    	// Vista principal 
    	setSizeFull();
		AbstractLayout mainLayout = createContents();
        setContent(mainLayout);
    }
    
    @Override
    public void attach() 
	{
		// S'ha de cridar sempre
        super.attach();
        // Obtenint la referència dins el mètode attach ens assegurem que la vista ja ha obtingut la referència
        EchidnaApplication applicationInstance = (EchidnaApplication)getApplication();
        // S'especifica el tema
        applicationInstance.setTheme("echidna");    
        
        applicationInstance.addLoginListener(this);
        /* La inicialització dels components que depènen de la consulta de l'usuari actual
         * s'ha de fer un cop lligada la vista a l'aplicació, moment en que ja s'ha obtingut la referència
         */
        boolean isUserLogged = ((EchidnaApplication)getApplication()).getCurrentUser()!=null;
        /* Login / Opcions de l'usuari */
 		if(isUserLogged){
 			userPanel = createUserPanel();
 			headerLayout.addComponent(userPanel);
 			headerLayout.setComponentAlignment(userPanel, Alignment.MIDDLE_RIGHT);
 		}
 		else{
 			loginPanel = createLoginPanel();
 			headerLayout.addComponent(loginPanel);
 			headerLayout.setComponentAlignment(loginPanel, Alignment.MIDDLE_RIGHT);
 		}
 		/* UI principal */
		if(isUserLogged){
			viewLayout = new HorizontalSplitPanel();
			viewLayout.setWidth("100.0%");
			viewLayout.setHeight("100.0%");
			viewLayout.setMargin(false);
			viewLayout.addStyleName(Runo.SPLITPANEL_SMALL);
			((HorizontalSplitPanel)viewLayout).setSplitPosition(17);
			/* Menú lateral */
			explorerView = new ExplorerView();
			explorerView.setWidth("100%");
			explorerView.setHeight("100%");
			viewLayout.addComponent(explorerView);
		}
		else{
			viewLayout = new HorizontalLayout();
			viewLayout.setWidth("100.0%");
			viewLayout.setHeight("100.0%");
			viewLayout.setMargin(false);
		}
		tabPanel = createTabPanel();
		viewLayout.addComponent(tabPanel);
		VerticalLayout mainLayout = (VerticalLayout)getContent();
		mainLayout.addComponent(viewLayout, 1);
		mainLayout.setExpandRatio(viewLayout, 2);

		/* Inicialització de la vista de navegació */
		navigator.addNavigationListener(this);
		navigator.addView(LoginView.class, new LoginView());
		navigator.addView(CreateUserView.class, new CreateUserView());
		navigator.addView(WelcomeView.class, new WelcomeView());
		// Inicialment es mostrarà la pantalla de login
		if(isUserLogged){
			navigator.startNavigation(WelcomeView.class);
		}
		// En cas contrari es procedeix a la pantalla de 
		else{
			navigator.startNavigation(LoginView.class);
		}
		navigatorTab.setCaption(navigator.getCurrentView().getTitle());
    }
    
    private AbstractLayout createContents()
	{
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setWidth("100%");
		mainLayout.setHeight("100%");
		mainLayout.setMargin(false);

		/* Header panel */
		headerLayout = new HorizontalLayout();
		headerLayout.setWidth("100.0%");
		headerLayout.setHeight("40px");
		headerLayout.setMargin(false);
		// Nom de l'aplicació
		Label appName = new Label();
		appName.setWidth("100px");
		appName.setHeight("100.0%");
		appName.setValue("Echidna");
		appName.addStyleName(Runo.LABEL_H1);
		headerLayout.addComponent(appName);
		headerLayout.setComponentAlignment(appName, Alignment.MIDDLE_LEFT);
		// S'afegeix el panell a la composició
		mainLayout.addComponent(headerLayout);
		mainLayout.setComponentAlignment(headerLayout, Alignment.TOP_CENTER);
		/* La inicialització dels components que depènen de la consulta de l'usuari actual
         * s'ha de fer un cop lligada la vista a l'aplicació, moment en que ja s'ha obtingut la referència
         * veure mètode attach()
         */
		
		/* Footer Panel */
		HorizontalLayout footerLayout = new HorizontalLayout();
		footerLayout.setWidth("100%");
		footerLayout.setHeight("35px");
		footerLayout.setMargin(true, true, false, true);
		// Detalls de l'aplicació
		Label appDetails = new Label("Echidna 0.1 Vox Stellarum - 2012 Fenrir Software ;)");
		appDetails.addStyleName("right-align");
		footerLayout.addComponent(appDetails);
		mainLayout.addComponent(footerLayout);
		
		return mainLayout;
	}
	
	private HorizontalLayout createLoginPanel()
	{
		/* Botonera abans de fer login */
		HorizontalLayout newLoginPanel = new HorizontalLayout();
		newLoginPanel.setHeight("100.0%");
		newLoginPanel.setMargin(false, true, false, true);
		newLoginPanel.setSpacing(true);
		// Botó login
		Button bLogin = new Button("Login");
		bLogin.addStyleName(Runo.BUTTON_LINK);
		bLogin.addListener(new Button.ClickListener() 
		{
			public void buttonClick(ClickEvent event) 
			{
				navigator.navigateTo(LoginView.class);
			}
		});
		newLoginPanel.addComponent(bLogin);
		newLoginPanel.setComponentAlignment(bLogin, Alignment.MIDDLE_RIGHT);
		newLoginPanel.setExpandRatio(bLogin, 2);
		// Botó crear usuari
		Button bCreateUser = new Button("Crear usuari");
		bCreateUser.addStyleName(Runo.BUTTON_LINK);
		bCreateUser.addListener(new Button.ClickListener() 
		{
			public void buttonClick(ClickEvent event) 
			{
				navigator.navigateTo(CreateUserView.class);
			}
		});
		newLoginPanel.addComponent(bCreateUser);
		newLoginPanel.setComponentAlignment(bCreateUser, Alignment.MIDDLE_RIGHT);
		
		return newLoginPanel;
	}
	
	private HorizontalLayout createUserPanel()
	{
		HorizontalLayout newUserPanel = new HorizontalLayout();
		newUserPanel.setHeight("100.0%");
		newUserPanel.setMargin(false, true, false, true);
		newUserPanel.setSpacing(true);
		// Nom d'usuari
		lUserName = new Label();
		newUserPanel.addComponent(lUserName);
		newUserPanel.setComponentAlignment(lUserName, Alignment.MIDDLE_RIGHT);
		// Opcions
		SplitButton splitButton = new SplitButton("Sortir");
		splitButton.addStyleName(SplitButton.STYLE_CHAMELEON);
		splitButton.setComponent(createOptionMenu());
		splitButton.addClickListener(new SplitButton.SplitButtonClickListener() 
		{
	        public void splitButtonClick(SplitButton.SplitButtonClickEvent event) 
	        {
	        	((EchidnaApplication)getApplication()).logout();
	        }
		});
		newUserPanel.addComponent(splitButton);
		newUserPanel.setComponentAlignment(splitButton, Alignment.MIDDLE_RIGHT);
		
		return newUserPanel;
	}

	private AbstractLayout createOptionMenu()
	{
		VerticalLayout layout = new VerticalLayout();
		layout.setSizeUndefined();
		/* Preferències */
		Button bPreferences = new Button("Preferències");
		bPreferences.addStyleName(Runo.BUTTON_LINK);
		bPreferences.setSizeUndefined();
		layout.addComponent(bPreferences);
		
		return layout;
	}
	
	private TabSheet createTabPanel()
	{
		TabSheet tabPanel = new TabSheet();
		tabPanel.addStyleName(Runo.TABSHEET_SMALL);
		tabPanel.setWidth("100.0%");
		tabPanel.setHeight("100.0%");
		/* Vista del navegador */
		navigator = new Navigator();
		navigatorTab = tabPanel.addTab(navigator, "", null);
//		navigatorTab.setClosable(true);
		/* Vista del reproductor */
		PlayerView playerView = new PlayerView();
		tabPanel.addTab(playerView, "Reproductor", null);
		/* Vista de cerca */
		BrowserView browserView = new BrowserView();
		tabPanel.addTab(browserView, "Cerca");
		
		return tabPanel;
	}
	
	public void openTab(CustomComponent view, String caption)
	{
		Tab tab = tabPanel.addTab(view, caption);
		tab.setClosable(true);
		// Es fa visible la pestanya afegida
		tabPanel.setSelectedTab(view);
	}
	
	public void navigatorViewChanged(View oldView, View newView)
	{
		navigatorTab.setCaption(newView.getTitle());
		// SI la vista de la pestanya de navegació canvia, es fa visible
		tabPanel.setSelectedTab(navigator);
	}
	
	public void onLogin(User user)
	{
		// Si encara és la primera vegada que es fa login, es crea la botonera d'opcions de l'usuari
		if(userPanel==null){
			userPanel = createUserPanel();			
		}
		headerLayout.removeComponent(loginPanel);
		headerLayout.addComponent(userPanel);
		headerLayout.setComponentAlignment(userPanel, Alignment.MIDDLE_RIGHT);
		// Es important fer-ho en darrer lloc per evitar problemes de repintat
		lUserName.setCaption(user.getName());
		
		/* UI Principal */
		VerticalLayout mainLayout = ((VerticalLayout)getContent());
		HorizontalSplitPanel viewLayoutAux = new HorizontalSplitPanel();
		viewLayoutAux.setWidth("100.0%");
		viewLayoutAux.setHeight("100.0%");
		viewLayoutAux.setMargin(false);
		viewLayoutAux.addStyleName(Runo.SPLITPANEL_SMALL);
		viewLayoutAux.setSplitPosition(17);
		/* Menú lateral */
		explorerView = new ExplorerView();
		explorerView.setWidth("100%");
		explorerView.setHeight("100%");
		viewLayoutAux.addComponent(explorerView);
		/* TabPanel */
		viewLayoutAux.moveComponentsFrom(viewLayout);
		
		mainLayout.removeComponent(viewLayout);
		mainLayout.addComponent(viewLayoutAux, 1);
		mainLayout.setExpandRatio(viewLayoutAux, 2);
		viewLayout = viewLayoutAux;
	}
	
	public void onLogout(User user)
	{
		if(loginPanel==null){
			loginPanel = createLoginPanel();
		}
		headerLayout.removeComponent(userPanel);
		headerLayout.addComponent(loginPanel);
		headerLayout.setComponentAlignment(loginPanel, Alignment.MIDDLE_RIGHT);
		
		/* UI Principal */
		VerticalLayout mainLayout = ((VerticalLayout)getContent());
		HorizontalLayout viewLayoutAux = new HorizontalLayout();
		viewLayoutAux.setWidth("100.0%");
		viewLayoutAux.setHeight("100.0%");
		viewLayoutAux.setMargin(false);
		viewLayout.removeComponent(tabPanel);
		viewLayoutAux.addComponent(tabPanel);
		mainLayout.removeComponent(viewLayout);
		mainLayout.addComponent(viewLayoutAux, 1);
		mainLayout.setExpandRatio(viewLayoutAux, 2);
		viewLayout = viewLayoutAux;
		
		// En fer logout es torna el navegador a la pantalla de login
		navigator.navigateTo(LoginView.class);
	}
}