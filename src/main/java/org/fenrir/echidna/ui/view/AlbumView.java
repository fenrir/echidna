package org.fenrir.echidna.ui.view;

import java.util.Collection;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.terminal.ClassResource;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;
import com.vaadin.ui.themes.Runo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.EchidnaApplication;
import org.fenrir.echidna.common.dto.GenericResultSetDTO;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;
import org.fenrir.echidna.core.service.IContentSearchService;
import org.fenrir.echidna.ui.model.PlayerModel;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120706
 */
@SuppressWarnings("serial")
public class AlbumView extends AbstractTabSheetView
{
	private final Log log = LogFactory.getLog(AlbumView.class);

	private String albumProvider;
	private String albumProviderId;

	private Embedded embeddedCDCase;
	private Table recordingTable;
	
	private IContentSearchService contentSearchService;
	
	public AlbumView(String albumProvider, String albumProviderId)
	{
		super("org.fenrir.echidna.i18n.views");
		
		this.albumProvider = albumProvider;
		this.albumProviderId = albumProviderId;
		
		contentSearchService = (IContentSearchService)EchidnaServiceLocator.getInstance().getInstance(IContentSearchService.class);
	}
	
	protected AbstractLayout createContents()
	{
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		GridLayout contentLayout = new GridLayout(2, 2);
		contentLayout.setHeight("100%");
		contentLayout.setRowExpandRatio(1, 2);
		
		/* Capçaleres */
		HorizontalLayout toolbarLayout = new HorizontalLayout();
		Button bAddAll = new Button(getInternacionalizedString("artistView.buttonAddAll"));
		bAddAll.addListener(new Button.ClickListener() 
		{
			public void buttonClick(Button.ClickEvent event) 
			{
				@SuppressWarnings("unchecked")
				Collection<String> itemIds = (Collection<String>)recordingTable.getItemIds();
				for(String rowID:itemIds){
					Item item = recordingTable.getItem(rowID);
					String id = (String)item.getItemProperty("id").getValue();
					String provider = (String)item.getItemProperty("provider").getValue();
					String songName = (String)item.getItemProperty("title").getValue();
					String albumSearchId = (String)item.getItemProperty("albumSearchId").getValue();
					String albumName = (String)item.getItemProperty("albumName").getValue();
					String artistSearchId = (String)item.getItemProperty("artistSearchId").getValue();
					String artistName = (String)item.getItemProperty("artistName").getValue();
					String length = (String)item.getItemProperty("length").getValue();
					
					PlayerModel playerModel = ((EchidnaApplication)getApplication()).getPlayerModel();
					PlayerModel.PlayerEntry entry = new PlayerModel.PlayerEntry();
					entry.setId(provider + "@" + id);
					entry.setSong(songName);
					entry.setAlbum(albumName);
					entry.setAlbumSearchId(albumSearchId);
					entry.setArtist(artistName);
					entry.setArtistSearchId(artistSearchId);
					entry.setLength(length);
					playerModel.addPlayerEntry(entry);
				}
			}
		});
		toolbarLayout.addComponent(bAddAll);
		contentLayout.addComponent(toolbarLayout, 1, 0);
		/* Llistes de cançóns i albums */
		embeddedCDCase = new Embedded();
		embeddedCDCase.setType(Embedded.TYPE_IMAGE);
		embeddedCDCase.setMimeType("/image/jpg");
		embeddedCDCase.setWidth("186px");
		embeddedCDCase.setHeight("200px");
		contentLayout.addComponent(embeddedCDCase, 0, 1);
		contentLayout.setComponentAlignment(embeddedCDCase, Alignment.TOP_CENTER);
		/* Lista de cançons de l'àlbum sel.leccionat */
		BeanContainer<String, IRecordingSearchHitDTO> songBeans = new BeanContainer<String, IRecordingSearchHitDTO>(IRecordingSearchHitDTO.class);
		songBeans.setBeanIdProperty("id");
		recordingTable = new Table(null, songBeans);
		recordingTable.addStyleName(Runo.TABLE_BORDERLESS);
		recordingTable.setHeight("100%");
		recordingTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);
		recordingTable.setVisibleColumns(new Object[]{"title", "length"});
		contentLayout.addComponent(recordingTable, 1, 1);
		// Cas que la vista mostri més d'un àlbum
		mainLayout.addComponent(contentLayout);
		mainLayout.setComponentAlignment(contentLayout, Alignment.TOP_CENTER);
		
		return mainLayout;
	}
	
	public void attach()
	{
		super.attach();
		
		embeddedCDCase.setSource(new ClassResource("/org/fenrir/echidna/web/images/cd_case.jpg", getApplication()));
		
		// S'emplena la taula d'àlbums amb els resultats obtinguts
		showRecordingList();
	}

	@SuppressWarnings("unchecked")
	private void showRecordingList()
	{
		try{
			GenericResultSetDTO<IRecordingSearchHitDTO> resultset = contentSearchService.findRecordingsByProviderAlbumData(albumProvider, albumProviderId);
			BeanContainer<String, IRecordingSearchHitDTO> datasource = (BeanContainer<String, IRecordingSearchHitDTO>)recordingTable.getContainerDataSource();
			datasource.removeAllItems();
			datasource.addAll(resultset.getResultList());
			datasource.sort(new Object[]{"order"}, new boolean[]{true});
		}
		catch(BussinessException e){
			log.error("[AlbumView::showRecordingList] Error en realitzar la cerca de cançons per l'album " + albumProviderId + ": " + e.getMessage(), e);			
			getWindow().showNotification(getInternacionalizedString("albumView.songsSearchError"), Notification.TYPE_ERROR_MESSAGE);
		}
	}
}
