package org.fenrir.echidna.ui.view;

import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TabSheet;
import org.fenrir.echidna.EchidnaApplication;
import org.fenrir.echidna.core.CoreConstants;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120706
 */
@SuppressWarnings("serial")
public abstract class AbstractTabSheetView extends CustomComponent 
{
	protected TabSheet tabSheet;
	protected boolean closeable;
	protected ResourceBundle resourceBundle;
	
	public AbstractTabSheetView()
	{
		setSizeFull();
		setCompositionRoot(createContents());
	}
	
	public AbstractTabSheetView(String resourceBundleBaseName)
	{
		/* Per obtenir la referència a l'aplicació s'ha d'obtenir a través de la pròpia classe
		 * ja que si es fa a través del mètode getApplication encara no estarà disponible
		 * al no haver-se executat el mètode attach.
		 */
		EchidnaApplication application = EchidnaApplication.getInstance();
		
		String language = null;
		if(application.getCurrentUser()!=null){
			language = application.getCurrentUser().getLanguage();
		}
		else{
			Properties applicationProperties = (Properties)EchidnaServiceLocator.getInstance()
					.getInstance(Properties.class, CoreConstants.NAMED_INJECTION_APPLICATION_PROPERTIES);
			language = applicationProperties.getProperty(CoreConstants.CONFIGURATION_USER_LANGUAGE);
		}
		
		Locale locale = new Locale(language);
		this.resourceBundle = ResourceBundle.getBundle(resourceBundleBaseName, locale);
		
		/* Una vegada s'ha recuperat el resource bundle es pot crear la interficie. 
		 * En cas de fer-ho en ordre invers donaria error en recuperar els missatges en el mètode createContents()
		 */
		setSizeFull();
		setCompositionRoot(createContents());
	}
	
	protected abstract AbstractLayout createContents();
	
	public void attach()
	{
		Component parent = getParent();
		if(parent instanceof TabSheet){
			tabSheet = (TabSheet)parent;
		}
		else{
			throw new IllegalStateException("La vista no pot ser afegida a un element diferent a TabSheet");
		}
	}
	
	public boolean isClosable()
	{
		TabSheet.Tab tab = tabSheet.getTab(this);
		return tab.isClosable();
	}
	
	public void setClosable(boolean closable)
	{
		TabSheet.Tab tab = tabSheet.getTab(this);
		tab.setClosable(closable);
	}
	
	public void closeView()
	{
		if(isClosable()){
			tabSheet.removeComponent(this);
		}
		else{
			throw new IllegalStateException("La vista no pot ser tancada");
		}
	}
	
	protected String getInternacionalizedString(String msgKey)
	{
		if(resourceBundle==null){
			throw new IllegalStateException("No s'ha inicialitzat el bundle de missatges per la vista");
		}
		
		return resourceBundle.getString(msgKey);
	}
}
