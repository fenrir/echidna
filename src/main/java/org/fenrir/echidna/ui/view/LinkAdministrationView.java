package org.fenrir.echidna.ui.view;

import java.util.ArrayList;
import java.util.List;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.terminal.ClassResource;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.themes.Runo;
import org.vaadin.youtubeplayer.YouTubePlayer;
import org.vaadin.youtubeplayer.client.ui.VYouTubePlayer;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.EchidnaApplication;
import org.fenrir.echidna.common.dto.GenericResultSetDTO;
import org.fenrir.echidna.common.dto.ILinkDTO;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.core.entity.User;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;
import org.fenrir.echidna.core.service.IContentAdministrationService;
import org.fenrir.echidna.core.service.IContentSearchService;
import org.fenrir.echidna.ui.model.PlayerModel;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120820
 */
@SuppressWarnings("serial")
public class LinkAdministrationView extends AbstractTabSheetView  
{
	private final Log log = LogFactory.getLog(LinkAdministrationView.class);
	
	// ID de la cançó que s'està tractant. Tindrà la forma <provider>@<providerId>
	private String id;
	private String title;
	
	private VerticalLayout listLayout;
	private Button bSearch;
	private AbstractLayout playerLayout;
	private Embedded playerReplacement;
	private YouTubePlayer player;
	private Label lTitle;
	private Label lDescription;
	private boolean playerInitialized = false;
	
	private List<CheckBox> vLinkCheckBoxes = new ArrayList<CheckBox>();

	private IContentSearchService contentSearchService;
	private IContentAdministrationService contentAdministrationService; 
	
	public LinkAdministrationView(String title, String id, List<ILinkDTO> links)
	{
		this.id = id;		
		this.title = title;
		
		/* Links */
		for(ILinkDTO link:links){
			AbstractLayout linkLayout = createLinkEntryLayout(link);
			listLayout.addComponent(linkLayout);
		}
		if(!links.isEmpty()){
			// El reproductor s'inicialitzarà amb la id del primer video
			initializePlayer(links.get(0).getLinkSearchId(), false);
			// Es marca el check del link actual
			vLinkCheckBoxes.get(0).setValue(true);
		} 
		
		contentAdministrationService = (IContentAdministrationService)EchidnaServiceLocator.getInstance().getInstance(IContentAdministrationService.class);
		contentSearchService = (IContentSearchService)EchidnaServiceLocator.getInstance().getInstance(IContentSearchService.class);
	}
	
	protected AbstractLayout createContents()
	{
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		/* Header */
		AbstractLayout headerLayout = createHeaderLayout();
		headerLayout.setWidth("100%");
		headerLayout.setMargin(true);
		mainLayout.addComponent(headerLayout);

		HorizontalLayout contentLayout = new HorizontalLayout();
		contentLayout.setSizeFull();
		/* Reproductor */
		playerLayout = createPlayerLayout();
		playerLayout.setMargin(true);
		playerLayout.setHeight("100%");
		contentLayout.addComponent(playerLayout);
		/* Link list */
		listLayout = new VerticalLayout();
		listLayout.setMargin(true, true, true, false);
		Panel panel = new Panel();
		panel.setHeight("100%");
		panel.addStyleName(Runo.PANEL_LIGHT);
		panel.setContent(listLayout);
		contentLayout.addComponent(panel);
		mainLayout.addComponent(contentLayout);
		mainLayout.setExpandRatio(contentLayout, 2);
		
		return mainLayout;
	}
	
	private AbstractLayout createHeaderLayout()
	{
		HorizontalLayout headerLayout = new HorizontalLayout();
		headerLayout.setSpacing(true);
		/* Títol de la cançó a la que pertanyen els links */
		Label lTitle = new Label(title);
		lTitle.setWidth("100%");
		headerLayout.addComponent(lTitle);
		headerLayout.setExpandRatio(lTitle, 2);
		/* Camp de cerca */
		final TextField inputSearch = new TextField();
		inputSearch.addListener(new Property.ValueChangeListener() 
		{
			public void valueChange(ValueChangeEvent event) 
			{
				String strSearch = (String)event.getProperty().getValue();
				search(strSearch);
			}
		});
		inputSearch.setWidth("400px");
		headerLayout.addComponent(inputSearch);
		bSearch = new Button();
		bSearch.addListener(new Button.ClickListener() 
		{
			public void buttonClick(ClickEvent event) 
			{
				String searchTerm = (String)inputSearch.getValue();
				search(searchTerm);
			}
		});
		headerLayout.addComponent(bSearch);
		
		return headerLayout;
	}
	
	private AbstractLayout createPlayerLayout()
	{
		VerticalLayout layout = new VerticalLayout();
		/* Reproductor flash */
		// L'etiqueta sustituirà el reproductor fins que no hi hagi un video sel.leccionat per reproduir
		playerReplacement = new Embedded();
		playerReplacement.setType(Embedded.TYPE_IMAGE);
		playerReplacement.setMimeType("/image/jpg");
		playerReplacement.setWidth("500px");
		playerReplacement.setHeight("300px");
		layout.addComponent(playerReplacement);
		/* Títol i descripció */
		lTitle = new Label();
		layout.addComponent(lTitle);
		lDescription = new Label();
		layout.addComponent(lDescription);
		
		return layout;
	}
	
	private void initializePlayer(String id, boolean playImmediately)
	{
		player = new YouTubePlayer(id, false);
		if(playImmediately){
			player.addStateListener(new YouTubePlayer.PlayerListener() 
			{
				public void playerStateChanged(YouTubePlayer player, int state, int oldState)
				{
					if(state==VYouTubePlayer.STATE_CUED && oldState==VYouTubePlayer.STATE_UNSTARTED){
						player.play();
					}
				}
				
				public void playerVolumeChanged(YouTubePlayer player, int volume)
				{
					
				}
				
				public void playerTimeChanged(YouTubePlayer player, float secs)
				{
					
				}
				
				public void playerError(YouTubePlayer player, int errorCode) 
				{
					
				}
			});
		}
		player.setWidth("500px");
		player.setHeight("300px");
		playerLayout.removeComponent(playerReplacement);
		((VerticalLayout)playerLayout).addComponent(player, 0);
		
		playerInitialized = true;
	}
	
	private AbstractLayout createLinkEntryLayout(ILinkDTO link)
	{
		GridLayout layout = new GridLayout(2, 2);
		layout.setMargin(false, false, true, false);
		layout.setSpacing(true);
		/* Thumbnail */
		ExternalResource thumbnail = new ExternalResource(link.getThumbnailUrl());
		Button embedded = new Button();
		embedded.addStyleName(BaseTheme.BUTTON_LINK);
		embedded.setIcon(thumbnail);
		embedded.setData(link.getLinkSearchId());
		embedded.addListener(new Button.ClickListener()
		{
			public void buttonClick(ClickEvent event) 
			{
				String videoId = (String)event.getButton().getData();
				if(!playerInitialized){
					initializePlayer(videoId, true);
				}
				else{
					player.play(videoId);
				}
			}
		});
		layout.addComponent(embedded, 0, 0, 0, 1);
		/* Títol */
		Button bTitle = new Button(link.getTitle());
		bTitle.setData(link.getLinkSearchId());
		bTitle.addListener(new Button.ClickListener()
		{
			public void buttonClick(ClickEvent event) 
			{
				String videoId = (String)event.getButton().getData();
				if(!playerInitialized){
					initializePlayer(videoId, true);
				}
				else{
					player.play(videoId);
				}
			}
		});
		bTitle.addStyleName(BaseTheme.BUTTON_LINK);
		layout.addComponent(bTitle, 1, 0);
		/* Botonera */
		final CheckBox checkDefault = new CheckBox("Assignar enllaç a la cançó");
		checkDefault.setImmediate(true);
		checkDefault.setData(link);
		checkDefault.addListener(new Button.ClickListener()
		{
			public void buttonClick(Button.ClickEvent event)
			{
				if((Boolean)checkDefault.getValue()){
					ILinkDTO defaultLink = (ILinkDTO)checkDefault.getData();
					
					// Es desmarca el check anterior
					for(CheckBox check:vLinkCheckBoxes){
						ILinkDTO link = (ILinkDTO)check.getData();
						if(defaultLink!=link){
							check.setValue(false);
						}
					}
					
					// Només es guarda l'associació predefinida per l'usuari si s'ha fet login
					User currentUser = ((EchidnaApplication)getApplication()).getCurrentUser();				
					if(currentUser!=null){
						
					}
					// Es canvia l'associació de la cançó amb el link especificat al model de dades del reproductor
					PlayerModel playerModel = ((EchidnaApplication)getApplication()).getPlayerModel();
					PlayerModel.PlayerEntry playerEntry = playerModel.getPlayerEntry(id);
					playerEntry.setLink(defaultLink);
					
					try{
						IRecordingSearchHitDTO recordingDTO = contentSearchService.findRecordingByProviderData(playerEntry.getProvider(), playerEntry.getProviderId());
						contentAdministrationService.upvoteSongLink(recordingDTO, defaultLink);
					}
					catch(BussinessException e){
						// Només es deixa constancia al log. No es llança excepció per no trencar la interacció al poder-se reproduir igualment
						log.error("[LinkAdministrationView::buttonClick] Error en promocionar el link sel.leccionat: " + e.getMessage(), e);
					}
					
					// Es notifica el canvi de link
					playerModel.notifyPlayerEntryModified(id);
				}
				// No està permès desmarcar
				else{
					checkDefault.setValue(true);
				}
			}
		});
		layout.addComponent(checkDefault, 1, 1);
		// Es guarda la referència per comunicar quan s'ha de desmarcar els checks
		vLinkCheckBoxes.add(checkDefault);
		/* Configuració mida de les columnes i files */
		layout.setColumnExpandRatio(1, 2);
		layout.setRowExpandRatio(1, 2);
		
		return layout;
	}
	
	@Override
	public void attach() 
	{
		super.attach();
		
		bSearch.setIcon(new ClassResource("/org/fenrir/echidna/web/images/search_16.png", getApplication()));
		playerReplacement.setSource(new ThemeResource("images/video_500.png"));
	}
	
	public String getTitle()
	{
		return "Video " + title;
	}
	
	private void search(String searchTerm)
	{
		if(StringUtils.isNotBlank(searchTerm)){
			IContentSearchService contentSearchService = (IContentSearchService)EchidnaServiceLocator.getInstance().getInstance(IContentSearchService.class);
			try{
				GenericResultSetDTO<ILinkDTO> resultset = contentSearchService.findLinks(searchTerm);
				// S'esborren els resultats anteriors
				listLayout.removeAllComponents();
				// S'eliminen les referències al checks antincs
				vLinkCheckBoxes.clear();
				if(resultset.size()>0){
					// S'afegeixen els resultats nous
					for(ILinkDTO link:resultset.getResultList()){
						AbstractLayout linkLayout = createLinkEntryLayout(link);
						linkLayout.setWidth("100%");
						listLayout.addComponent(linkLayout);
					}
				}
				else{
					Label label = new Label("No s'ha trobat resultats!");
					listLayout.addComponent(label);
					listLayout.setComponentAlignment(label, Alignment.TOP_CENTER);
				}
			}
			catch(BussinessException e){
				// TODO v0.1 Log + mostrar error
			}
		}
	}
}
