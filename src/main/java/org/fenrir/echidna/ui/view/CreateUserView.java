package org.fenrir.echidna.ui.view;

import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window.Notification;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.EchidnaApplication;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;
import org.fenrir.echidna.core.service.IUserAdministrationService;
import org.fenrir.echidna.ui.widget.Navigator;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120506
 */
public class CreateUserView extends Navigator.View
{
	private static final long serialVersionUID = -3022090552732703904L;
	
	public static final String VIEW_ID = "org.fenrir.echidna.ui.view.createUserView";

	private final Log log = LogFactory.getLog(CreateUserView.class);
	
	private IUserAdministrationService userManagementService;
	
	public CreateUserView()
	{
		setSizeFull();
		AbstractLayout mainLayout = createContents();
		setCompositionRoot(mainLayout);
		
		userManagementService = (IUserAdministrationService)EchidnaServiceLocator.getInstance().getInstance(IUserAdministrationService.class);
	}
	
	@SuppressWarnings("serial")
	private AbstractLayout createContents()
	{
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		
		Label label = new Label("Creació d'usuari");
		label.addStyleName("v-label-h1");
        label.setSizeUndefined();
        mainLayout.addComponent(label);
        mainLayout.setComponentAlignment(label, Alignment.TOP_CENTER);
        mainLayout.setExpandRatio(label, 1);
        
        VerticalLayout formLayout = new VerticalLayout();
        formLayout.setWidth("160px");
        final TextField inputName = new TextField("Nom");
        inputName.setWidth("150px");
        formLayout.addComponent(inputName);
        final PasswordField inputPassword = new PasswordField("Contrasenya");
        inputPassword.setWidth("150px");
        formLayout.addComponent(inputPassword);
        final PasswordField confirmPassword = new PasswordField("Confirmar contrasenya");
        confirmPassword.setWidth("150px");
        formLayout.addComponent(confirmPassword);
        Button bCreateUser = new Button("Crear");
        bCreateUser.addListener(new Button.ClickListener() 
        {
			public void buttonClick(ClickEvent event) 
			{
				String username = (String)inputName.getValue();
				String password = (String)inputPassword.getValue();
				String password2 = (String)confirmPassword.getValue();
				
				if(StringUtils.isBlank(username)){
					getWindow().showNotification("Error", "El camp nom d'usuari no pot estar buit!", Notification.TYPE_WARNING_MESSAGE);
					return;
				}
				if(StringUtils.isBlank(password)){
					getWindow().showNotification("Error", "El camp contrasenya no pot estar buit!", Notification.TYPE_WARNING_MESSAGE);
					return;
				}
				if(!password.equals(password2)){
					getWindow().showNotification("Error", "En camp confirmació no pot estar buit!", Notification.TYPE_WARNING_MESSAGE);
					return;
				}
				try{
					userManagementService.createUser(username, password);
					// Una vegada creat l'usuari, es fa el login
					EchidnaApplication.getInstance().login(username, password);
		        	// Si el login s'ha fet correctament es passa a la vista de benvinguda
		        	navigator.navigateTo(WelcomeView.class);
				}
				catch(Exception e){
					log.error("[CreateUserView::buttonClick] Error al crear usuari: " + e.getMessage(), e);
					getWindow().showNotification("Error al crear usuari: " + e.getMessage(), Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
        formLayout.addComponent(bCreateUser);
        mainLayout.addComponent(formLayout);
        mainLayout.setExpandRatio(formLayout, 2);
        mainLayout.setComponentAlignment(formLayout, Alignment.TOP_CENTER);
		
		return mainLayout;
	}
	
	@Override
	public String getId()
	{
		return VIEW_ID;
	}
	
	@Override
	public String getTitle()
	{
		return "Creació d'usuari";
	}
	
	@Override
	public void navigationEntered()
	{
		
	}
	
	@Override
	public void navigationLeft()
	{
		
	}
}
