package org.fenrir.echidna.ui.view;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.terminal.ClassResource;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window.Notification;
import com.vaadin.ui.themes.Runo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.EchidnaApplication;
import org.fenrir.echidna.ui.widget.AlbumLinkTableColumnGenerator;
import org.fenrir.echidna.ui.widget.ArtistLinkTableColumnGenerator;
import org.fenrir.echidna.ui.widgetset.SplitButton;
import org.fenrir.echidna.ui.model.PlayerModel;
import org.fenrir.echidna.common.dto.GenericResultSetDTO;
import org.fenrir.echidna.common.dto.IAlbumSearchHitDTO;
import org.fenrir.echidna.common.dto.IArtistSearchHitDTO;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;
import org.fenrir.echidna.core.service.IContentSearchService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120706
 */
@SuppressWarnings("serial")
public class BrowserView extends AbstractTabSheetView 
{
	private static final String TABLE_KEY_ARTISTS = "artists";
	private static final String TABLE_KEY_ALBUMS = "albums";
	private static final String TABLE_KEY_SONGS = "songs";
	
	private final Log log = LogFactory.getLog(BrowserView.class);
	
	private ComboBox comboSearchOptions;
	private Table table;
	private Button bSearch;
	
	private IContentSearchService contentSearchService;
	
	public BrowserView()
	{
		contentSearchService = (IContentSearchService)EchidnaServiceLocator.getInstance().getInstance(IContentSearchService.class);
	}
	
	@Override
	public void attach() 
	{
		super.attach();
		
		bSearch.setIcon(new ClassResource("/org/fenrir/echidna/web/images/search_16.png", getApplication()));
	}

	protected AbstractLayout createContents()
	{
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		
		/* Camp de cerca */
		HorizontalLayout searchLayout = new HorizontalLayout();
		searchLayout.setMargin(true, true, false, true);
		searchLayout.setSpacing(true);
		final ComboBox inputSearch = new ComboBox();
		inputSearch.setWidth("400px");
		inputSearch.setInputPrompt("-- Introduiu la vostra cerca --");
		inputSearch.setFilteringMode(AbstractSelect.Filtering.FILTERINGMODE_CONTAINS);
		inputSearch.setImmediate(true);
		inputSearch.setNewItemsAllowed(true);
		inputSearch.setNewItemHandler(new AbstractSelect.NewItemHandler() 
		{
			public void addNewItem(String newItemCaption) 
			{
				if(!inputSearch.containsId(newItemCaption)){
		            inputSearch.addItem(newItemCaption);
		            inputSearch.setValue(newItemCaption);
		        }
			}
		});
		inputSearch.addListener(new Property.ValueChangeListener() 
		{
			public void valueChange(ValueChangeEvent event) 
			{
				log.debug("[BrowserView::valueChange] Detectat canvi al comboBox; Iniciant cerca...");
				
				String strSearch = (String)event.getProperty().getValue();
				search(strSearch);
			}
		});
		searchLayout.addComponent(inputSearch);
		searchLayout.setComponentAlignment(inputSearch, Alignment.MIDDLE_CENTER);
		bSearch = new Button();
		bSearch.addListener(new Button.ClickListener() 
		{
			public void buttonClick(ClickEvent event) 
			{
				String strSearch = (String)inputSearch.getValue();
				search(strSearch);
			}
		});
		searchLayout.addComponent(bSearch);
		comboSearchOptions = new ComboBox();
		comboSearchOptions.setNullSelectionAllowed(false);
		comboSearchOptions.addItem(TABLE_KEY_ARTISTS);
		comboSearchOptions.setItemCaption(TABLE_KEY_ARTISTS, "Intèrprets");
		comboSearchOptions.addItem(TABLE_KEY_ALBUMS);
		comboSearchOptions.setItemCaption(TABLE_KEY_ALBUMS, "Àlbums");
		comboSearchOptions.addItem(TABLE_KEY_SONGS);
		comboSearchOptions.setItemCaption(TABLE_KEY_SONGS, "Cançons");
		// Es sel.lecciona per defecte la cerca per intèrpret
		comboSearchOptions.select("artists");
		searchLayout.addComponent(comboSearchOptions);
		searchLayout.setComponentAlignment(comboSearchOptions, Alignment.MIDDLE_CENTER);
		mainLayout.addComponent(searchLayout);
		mainLayout.setComponentAlignment(searchLayout, Alignment.TOP_CENTER);
		
		return mainLayout;
	}
	
	private void search(String strSearch)
	{
		String searchOption = (String)comboSearchOptions.getValue();

		VerticalLayout mainLayout = (VerticalLayout)getCompositionRoot();
		try{
			if(table!=null){
				mainLayout.removeComponent(table);
			}
			
			if(TABLE_KEY_ARTISTS.equals(searchOption)){
				GenericResultSetDTO<IArtistSearchHitDTO> resultset = contentSearchService.findArtistsByName(strSearch);
				table = buildArtistResultTable(resultset);
			}
			else if(TABLE_KEY_ALBUMS.equals(searchOption)){
				GenericResultSetDTO<IAlbumSearchHitDTO> resultset = contentSearchService.findAlbumsByName(strSearch);
				table = buildAlbumResultTable(resultset);
			}
			else if(TABLE_KEY_SONGS.equals(searchOption)){
				GenericResultSetDTO<IRecordingSearchHitDTO> resultSet = contentSearchService.findRecordingsByName(strSearch);
				table = buildSongResultTable(resultSet);
			}
			
			if(table!=null){
				mainLayout.addComponent(table);
				mainLayout.setComponentAlignment(table, Alignment.TOP_RIGHT);
			}
		}
		catch(BussinessException e){
			log.error("[BrowserView::search] Error en cercar: " + e.getMessage(), e);
			getWindow().showNotification("Error en realitzar la cerca", Notification.TYPE_ERROR_MESSAGE);
		}
	}
	
	private Table buildArtistResultTable(GenericResultSetDTO<IArtistSearchHitDTO> resultset)
	{
		BeanContainer<String, IArtistSearchHitDTO> beans = new BeanContainer<String, IArtistSearchHitDTO>(IArtistSearchHitDTO.class);
		beans.setBeanIdProperty("id");
		final Table table = new Table(null, beans);
		table.setWidth("95%");
		// Es defineix les propietats de les dades que contindran les columnes
		table.addStyleName(Runo.TABLE_BORDERLESS);
		// Es defineix les propietats de les dades que contindran les columnes
		table.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);
		table.addGeneratedColumn("artistSearchButton", new ArtistLinkTableColumnGenerator("name", "id"));
		table.setVisibleColumns(new Object[]{"artistSearchButton", "description"});
		// S'emplena amb els resultats
		beans.addAll(resultset.getResultList());
		
		return table;
	}
	
	private Table buildAlbumResultTable(GenericResultSetDTO<IAlbumSearchHitDTO> resultset)
	{
		BeanContainer<String, IAlbumSearchHitDTO> beans = new BeanContainer<String, IAlbumSearchHitDTO>(IAlbumSearchHitDTO.class);
		beans.setBeanIdProperty("id");
		final Table table = new Table(null, beans);
		table.setWidth("95%");
		// Es defineix les propietats de les dades que contindran les columnes
		table.addStyleName(Runo.TABLE_BORDERLESS);
		// Es defineix les propietats de les dades que contindran les columnes
		table.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);
		table.addGeneratedColumn("albumSearchButton", new AlbumLinkTableColumnGenerator("name", "id"));
		table.addGeneratedColumn("artistSearchButton", new ArtistLinkTableColumnGenerator("artistName", "artistSearchId"));
		table.setVisibleColumns(new Object[]{"albumSearchButton", "artistSearchButton", "firstReleaseDate"});
		// S'emplena amb els resultats
		beans.addAll(resultset.getResultList());
		
		return table;
	}
	
	private Table buildSongResultTable(GenericResultSetDTO<IRecordingSearchHitDTO> resultset)
	{
		BeanContainer<String, IRecordingSearchHitDTO> beans = new BeanContainer<String, IRecordingSearchHitDTO>(IRecordingSearchHitDTO.class);
		beans.setBeanIdProperty("id");
		final Table table = new Table(null, beans);
		table.setWidth("95%");
		table.addStyleName(Runo.TABLE_BORDERLESS);
		// S'envien els canvis en la sel.lecció immediatament al servidor
		table.setSelectable(true);
		// Es defineix les propietats de les dades que contindran les columnes
		table.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);
		table.addGeneratedColumn("addButton", new Table.ColumnGenerator() 
		{
			public Object generateCell(Table source, Object itemId, Object columnId) 
			{
				SplitButton splitButton = new SplitButton();
				splitButton.addStyleName(SplitButton.STYLE_CHAMELEON);
				splitButton.setWidth("65px");
				splitButton.setIcon(new ClassResource("/org/fenrir/echidna/web/images/add_16.png", getApplication()));
				splitButton.setComponent(createOptionMenu());
				splitButton.setData(itemId);
				splitButton.addClickListener(new SplitButton.SplitButtonClickListener() 
				{
			        public void splitButtonClick(SplitButton.SplitButtonClickEvent event)
					{
						String rowID = (String)event.getSplitButton().getData();
						Item item = table.getItem(rowID);
						String id = (String)item.getItemProperty("id").getValue();
						String provider = (String)item.getItemProperty("provider").getValue();
						String songName = (String)item.getItemProperty("title").getValue();
						String albumSearchId = (String)item.getItemProperty("albumSearchId").getValue();
						String albumName = (String)item.getItemProperty("albumName").getValue();
						String artistSearchId = (String)item.getItemProperty("artistSearchId").getValue();
						String artistName = (String)item.getItemProperty("artistName").getValue();
						String length = (String)item.getItemProperty("length").getValue();
						
						PlayerModel playerModel = ((EchidnaApplication)getApplication()).getPlayerModel();
						PlayerModel.PlayerEntry entry = new PlayerModel.PlayerEntry();
						entry.setId(provider + "@" + id);
						entry.setSong(songName);
						entry.setAlbum(albumName);
						entry.setAlbumSearchId(albumSearchId);
						entry.setArtist(artistName);
						entry.setArtistSearchId(artistSearchId);
						entry.setLength(length);
						playerModel.addPlayerEntry(entry);
			        } 
				});
				
				return splitButton;
			}
			
			private AbstractLayout createOptionMenu()
			{
				VerticalLayout layout = new VerticalLayout();
				layout.setSizeUndefined();				
				
				return layout;
			}
		});
		table.addGeneratedColumn("artistSearchButton", new ArtistLinkTableColumnGenerator("artistName", "artistSearchId"));
		table.addGeneratedColumn("albumSearchButton", new AlbumLinkTableColumnGenerator("albumName", "albumSearchId"));
		table.setVisibleColumns(new Object[]{"addButton", "title", "artistSearchButton", "albumSearchButton", "length"});
		table.setColumnWidth("addButton", 70);
		// S'emplena amb els resultats
		beans.addAll(resultset.getResultList());
		
		return table;
	}
}
