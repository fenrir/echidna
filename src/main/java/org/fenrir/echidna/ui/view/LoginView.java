package org.fenrir.echidna.ui.view;

import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.LoginForm;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window.Notification;
import com.vaadin.ui.themes.Runo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.EchidnaApplication;
import org.fenrir.echidna.core.security.exception.LoginFailureException;
import org.fenrir.echidna.ui.widget.Navigator;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120409
 */
public class LoginView  extends Navigator.View implements LoginForm.LoginListener 
{
	private static final long serialVersionUID = 2392772602616775362L;
	
	public static final String VIEW_ID = "org.fenrir.echidna.ui.view.loginView";
	
	private final Log log = LogFactory.getLog(LoginView.class);
	
	private LoginForm loginForm;

	public LoginView()
	{
		setSizeFull();
		AbstractLayout mainLayout = createContents();
		setCompositionRoot(mainLayout);
	}
	
	private AbstractLayout createContents()
	{
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		
		Label label = new Label("Login");
		label.addStyleName("v-label-h1");
        label.setSizeUndefined();
        mainLayout.addComponent(label);
        mainLayout.setComponentAlignment(label, Alignment.TOP_CENTER);
        mainLayout.setExpandRatio(label, 1);
        
        loginForm = new LoginForm();
        loginForm.setWidth("170px");
        loginForm.setUsernameCaption("Usuari");
        loginForm.setPasswordCaption("Contrasenya");
        loginForm.setLoginButtonCaption("Entrar");
        loginForm.addListener(this);
        mainLayout.addComponent(loginForm);
        mainLayout.setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);
        mainLayout.setExpandRatio(loginForm, 1);
        
        Button bCreateUser = new Button("Crear usuari");
        bCreateUser.addStyleName(Runo.BUTTON_LINK);
        bCreateUser.addListener(new Button.ClickListener() 
        {
			private static final long serialVersionUID = -1978193118970845888L;

			public void buttonClick(ClickEvent event) 
			{
				getNavigator().navigateTo(CreateUserView.class);
			}
		});
        mainLayout.addComponent(bCreateUser);
        mainLayout.setComponentAlignment(bCreateUser, Alignment.TOP_CENTER);
        mainLayout.setExpandRatio(bCreateUser, 2);
        
        return mainLayout;
	}

    public void onLogin(LoginForm.LoginEvent event) 
	{
        String username = event.getLoginParameter("username");
        String password = event.getLoginParameter("password");

        try{
        	EchidnaApplication.getInstance().login(username, password);
        	// Si el login s'ha fet correctament es passa a la vista de benvinguda
        	navigator.navigateTo(WelcomeView.class);
        }
        catch(LoginFailureException e){
        	log.info("[LoginView::onLogin] L'usuari " + username + " no existeix!");
        	getWindow().showNotification("L'usuari " + username + " no existeix", Notification.TYPE_ERROR_MESSAGE);
        }
    }

    @Override
	public String getId()
	{
		return VIEW_ID;
	}
	
	@Override
	public String getTitle()
	{
		return "Login";
	}
    
    @Override
	public void navigationEntered()
	{
		
	}
	
	@Override
	public void navigationLeft()
	{
		
	}
}
