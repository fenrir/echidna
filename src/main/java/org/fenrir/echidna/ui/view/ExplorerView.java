package org.fenrir.echidna.ui.view;

import java.util.List;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.themes.Runo;
import org.fenrir.echidna.EchidnaApplication;
import org.fenrir.echidna.ui.dialog.PlaylistAdministrationDialog;
import org.fenrir.echidna.core.entity.Playlist;
import org.fenrir.echidna.core.entity.User;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;
import org.fenrir.echidna.core.service.IUserContentAdministrationService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120616
 */
public class ExplorerView extends CustomComponent implements Button.ClickListener 
{
	private static final long serialVersionUID = 1563514828263730930L;

	private Table playlistTable;

	public ExplorerView()
	{
		setSizeFull();
		AbstractLayout mainLayout = createContents();
		setCompositionRoot(mainLayout);
	}
	
	private AbstractLayout createContents()
	{
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setWidth("100%");
		mainLayout.setHeight("100%");
		mainLayout.setMargin(false);

		Accordion accordion = new Accordion();
		accordion.setWidth("100%");
		accordion.setHeight("100%");
		/* TODO Cerques recents */
		accordion.addTab(new VerticalLayout(), "Cerques recents");
		/* TODO Favorits */
		accordion.addTab(new VerticalLayout(), "Favorits");
		/* Llistes de reproducció */
		AbstractLayout playlistsLayout = createPlaylistTab();
		accordion.addTab(playlistsLayout, "Llistes de reproducció");
		mainLayout.addComponent(accordion);
		
		return mainLayout;
	}
	
	private AbstractLayout createPlaylistTab()
	{
		VerticalLayout layout = new VerticalLayout();

		Button bCreatePlaylist = new Button("Crear una nova llista", this);
		bCreatePlaylist.addStyleName(BaseTheme.BUTTON_LINK);
		bCreatePlaylist.addStyleName("playlist-create");
		layout.addComponent(bCreatePlaylist);

		/* Llistes de reproducció de l'usuari */
		BeanContainer<String, Playlist> beans = new BeanContainer<String, Playlist>(Playlist.class);
		beans.setBeanIdProperty("id");
		playlistTable = new Table(null, beans);
		playlistTable.setSizeFull();
		// S'indica que es comuniqui els canvis immediatament al servidor (per la sel.lecció)
		playlistTable.setImmediate(true);
		playlistTable.setSelectable(true);
		playlistTable.addStyleName(Runo.TABLE_BORDERLESS);
		// Es defineix les propietats de les dades que contindran les columnes
		playlistTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);
		playlistTable.setVisibleColumns(new Object[]{"name"});
		layout.addComponent(playlistTable);
		
		return layout;
	}
	
	public void attach()
	{
		loadPlaylists();
	}
	
	public void buttonClick(Button.ClickEvent event) 
	{
		// S'obre la finestra de creació d'una nova llista de reproducció
		PlaylistAdministrationDialog playlistDialog = new PlaylistAdministrationDialog();
		// S'escolten els events de tancament de la finestra de creació
		playlistDialog.addListener(new Window.CloseListener()
		{
			private static final long serialVersionUID = 4120959621144907129L;

			public void windowClose(Window.CloseEvent event) 
			{
				// Refrescar llistes de reproducció
				loadPlaylists();
			}
		});
		getWindow().addWindow(playlistDialog);
	}
	
	@SuppressWarnings("unchecked")
	private void loadPlaylists()
	{
		IUserContentAdministrationService playlistAdministrationService = (IUserContentAdministrationService)EchidnaServiceLocator.getInstance().getInstance(IUserContentAdministrationService.class);
		
		User user = ((EchidnaApplication)getApplication()).getCurrentUser();
		List<Playlist> userPlaylists = playlistAdministrationService.findPlaylistsByUser(user);
		playlistTable.getContainerDataSource().removeAllItems();
		((BeanContainer<String, Playlist>)playlistTable.getContainerDataSource()).addAll(userPlaylists);
	}
}
