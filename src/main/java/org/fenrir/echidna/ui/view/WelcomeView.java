package org.fenrir.echidna.ui.view;

import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;

import org.fenrir.echidna.ui.widget.Navigator;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120406
 */
@SuppressWarnings("serial")
public class WelcomeView extends Navigator.View
{
	public static final String VIEW_ID = "org.fenrir.echidna.ui.view.welcomeView";
	
    public WelcomeView() 
    {
        AbstractLayout layout = createContents();
        setCompositionRoot(layout);
    }
    
    private AbstractLayout createContents()
    {
    	VerticalLayout layout = new VerticalLayout();
    	
    	layout.setMargin(true);
        layout.setSizeFull();
        Label label = new Label(
                "<h1 class=\"v-label-h1\" style=\"text-align: center;\">Welcome</h1> Select an entity type from the left side menu to begin",
                Label.CONTENT_XHTML);
        label.setSizeUndefined();
        label.addStyleName(Runo.LABEL_SMALL);
        layout.addComponent(label);
        layout.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
    	
    	return layout;
    }
    
    @Override
	public String getId()
	{
		return VIEW_ID;
	}
	
	@Override
	public String getTitle()
	{
		return "Benvingut!";
	}
    
    @Override
	public void navigationEntered()
	{
		
	}
	
	@Override
	public void navigationLeft()
	{
		
	}
}