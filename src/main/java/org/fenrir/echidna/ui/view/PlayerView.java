package org.fenrir.echidna.ui.view;

import com.vaadin.terminal.ClassResource;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.AbstractSelect.ItemDescriptionGenerator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.themes.Runo;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.event.ItemClickEvent;
import org.vaadin.hene.popupbutton.PopupButton;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.EchidnaApplication;
import org.fenrir.echidna.common.dto.GenericResultSetDTO;
import org.fenrir.echidna.common.dto.ILinkDTO;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.common.service.ILyricsSearchService;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;
import org.fenrir.echidna.core.service.IContentSearchService;
import org.fenrir.echidna.ui.model.PlayerModel;
import org.fenrir.echidna.ui.model.PlayerModel.PlayerEntry;
import org.fenrir.echidna.ui.model.PlayerModel.PlayerStatus;
import org.fenrir.echidna.ui.widget.AlbumLinkTableColumnGenerator;
import org.fenrir.echidna.ui.widget.ArtistLinkTableColumnGenerator;
import org.fenrir.echidna.ui.widget.Player;
import org.fenrir.echidna.ui.widget.PlaylistTableEntryOptions;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.0.20120909
 */
@SuppressWarnings("serial")
public class PlayerView extends CustomComponent implements PlayerModel.PlayerModelListener  
{
	private final Log log = LogFactory.getLog(PlayerView.class);
	
	private Embedded embedded;
	private Table playlistTable;
	private Button bMode;
	private Button bRemoveAll;
	private Label lyricsArea;
	private Panel pLyricsArea;

	private Player player;
	private PlayerModel playerModel;
	
	private IContentSearchService contentSearchService;
	
	public PlayerView()
	{
		setSizeFull();
		AbstractLayout mainLayout = createContents();
		setCompositionRoot(mainLayout);
		
		contentSearchService = (IContentSearchService)EchidnaServiceLocator.getInstance().getInstance(IContentSearchService.class);
	}
	
	private AbstractLayout createContents()
	{
		HorizontalLayout mainLayout = new HorizontalLayout();
		mainLayout.setSizeFull();
		
		player = new Player();
		
		/* Layout de la part esquerre */
		VerticalLayout leftLayout = new VerticalLayout();
		leftLayout.setMargin(true, false, true, false);
		leftLayout.setWidth("250px");
		leftLayout.addComponent(player.getPlayerComponent());		
		// Caràtula
		embedded = new Embedded();
		embedded.setType(Embedded.TYPE_IMAGE);
		embedded.setMimeType("/image/jpg");
		embedded.setWidth("186px");
		embedded.setHeight("200px");
		leftLayout.addComponent(embedded);
		leftLayout.setComponentAlignment(embedded, Alignment.TOP_CENTER);
		mainLayout.addComponent(leftLayout);
		
		/* Layout de la part dreta */
		VerticalLayout rightLayout = new VerticalLayout();
		rightLayout.setMargin(false, true, false, false);
		rightLayout.setSizeFull();
		// Reproductor
		AbstractLayout playerLayout = player.getPlayerLayout();
		playerLayout.setWidth("100%");
		playerLayout.setHeight("120px");
		rightLayout.addComponent(playerLayout);
		// Playlist
		AbstractLayout playlistLayout = createPlaylistLayout();
		playlistLayout.setSizeFull();
		rightLayout.addComponent(playlistLayout);
		rightLayout.setExpandRatio(playlistLayout, 2);
		mainLayout.addComponent(rightLayout);
		mainLayout.setExpandRatio(rightLayout, 2);
		
		return mainLayout;
	}

	@SuppressWarnings("unchecked")
	public void attach()
	{
		super.attach();
		
		playerModel = ((EchidnaApplication)getApplication()).getPlayerModel();
		
		/* S'inicialitzen les imatges després que la vista s'hagi creat. 
		 * S'ha de fer així en els casos que es requereixi el playerModel
		 */
		embedded.setSource(new ClassResource("/org/fenrir/echidna/web/images/cd_case.jpg", getApplication()));
		if(PlayerModel.PlayerMode.normal.equals(playerModel.getPlayerMode())){
			bMode.setIcon(new ThemeResource("images/loop_disabled_20.png"));
		}
		else{
			bMode.setIcon(new ThemeResource("images/loop_enabled_20.png"));
		}
		
		player.setPlayerModel(playerModel);
		// S'indica que es volen rebre els events de canvi en la llista de reproducció actual
		playerModel.addListener(this);
		// S'afegeixen les entrades inicials de la vista presents a sessió
		((BeanContainer<String, PlayerModel.PlayerEntry>)playlistTable.getContainerDataSource()).addAll(playerModel.getPlayerEntries());
	}
	
	private AbstractLayout createPlaylistLayout()
	{
		VerticalLayout layout = new VerticalLayout();
		layout.setSpacing(true);
		
		/* Botonera de la llista de reproducció */
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setWidth("100%");		
		buttonsLayout.setHeight("22px");
		// Loop
		bMode = new Button();
		bMode.addStyleName(Runo.BUTTON_LINK);
		bMode.addListener(new Button.ClickListener()
		{
			public void buttonClick(Button.ClickEvent event)
			{
				if(PlayerModel.PlayerMode.normal.equals(playerModel.getPlayerMode())){
					bMode.setIcon(new ThemeResource("images/loop_enabled_20.png"));
					playerModel.setPlayerMode(PlayerModel.PlayerMode.loop);
				}
				else{
					bMode.setIcon(new ThemeResource("images/loop_disabled_20.png"));
					playerModel.setPlayerMode(PlayerModel.PlayerMode.normal);
				}
			}
		});
		buttonsLayout.addComponent(bMode);
		buttonsLayout.setComponentAlignment(bMode, Alignment.TOP_LEFT);
		// Remove
		bRemoveAll = new Button();		
		bRemoveAll.addStyleName(Runo.BUTTON_LINK);
		bRemoveAll.setIcon(new ThemeResource("images/remove_all_20.png"));
		bRemoveAll.addListener(new Button.ClickListener()
		{
			public void buttonClick(Button.ClickEvent event)
			{
				playerModel.clearPlayerEntries();
			}
		});
		buttonsLayout.addComponent(bRemoveAll);
		buttonsLayout.setComponentAlignment(bRemoveAll, Alignment.TOP_LEFT);
		buttonsLayout.setExpandRatio(bRemoveAll, 2);
		layout.addComponent(buttonsLayout);
		// Lyrics
		Button bLyrics = new Button();
		bLyrics.addStyleName(BaseTheme.BUTTON_LINK);
		bLyrics.setIcon(new ThemeResource("images/lyrics_20.png"));		
		bLyrics.setDescription("Mostrar lletra de la cançó");
		bLyrics.addListener(new Button.ClickListener()
		{
			public void buttonClick(Button.ClickEvent event)
			{
				boolean lyricsVisible = !playerModel.isLyricsVisible();
				playerModel.setLyricsVisible(lyricsVisible);
				
				if(lyricsVisible){
					// Es mostra la zona de la lletra de la cançó
					pLyricsArea.setVisible(true);
					
					try{
						ILyricsSearchService lyricsSearchService = (ILyricsSearchService)EchidnaServiceLocator.getInstance().getInstance(ILyricsSearchService.class);
						PlayerEntry currentEntry = playerModel.getCurrentPlayerEntry();
						if(currentEntry!=null){
							String result = lyricsSearchService.findLyrics(currentEntry.getArtist(), currentEntry.getSong());
							if(StringUtils.isNotBlank(result)){
								lyricsArea.setValue(result);
							}
							else{
								lyricsArea.setValue("-- Lletra de la cançó no disponible --");
							}
						}
					}
					catch(BussinessException e){
						log.error("[PlayerView::createPlaylistLayout] Error en mostrar la lletra de la cançó: " + e.getMessage(), e);
						getWindow().showNotification("Error en mostrar la lletra de la cançó", Notification.TYPE_ERROR_MESSAGE);
					}
				}
				else{
					// S'amaga la zona de la lletra de la cançó
					lyricsArea.setValue(null);
					pLyricsArea.setVisible(false);
				}
				
			}
		});
		buttonsLayout.addComponent(bLyrics);
		buttonsLayout.setComponentAlignment(bLyrics, Alignment.TOP_LEFT);
		
		HorizontalLayout midLayout = new HorizontalLayout();
		midLayout.setSizeFull();
		midLayout.setSpacing(true);
		/* Taula amb les cançons de la llista */
		BeanContainer<String, PlayerModel.PlayerEntry> beans = new BeanContainer<String, PlayerModel.PlayerEntry>(PlayerModel.PlayerEntry.class);
		beans.setBeanIdProperty("id");
		playlistTable = new Table(null, beans);
		playlistTable.setWidth("100%");
		playlistTable.setHeight("100%");
		playlistTable.setSelectable(true);
		// Es defineix les propietats de les dades que contindran les columnes
		playlistTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);
		playlistTable.addGeneratedColumn("status", new Table.ColumnGenerator()
		{
			public Object generateCell(final Table source, Object itemId, Object columnId) 
			{
				Label lStatus = new Label();
				lStatus.setContentMode(Label.CONTENT_XHTML);
				lStatus.setWidth("20px");
				lStatus.setHeight("20px");
				
				String currentSongId = playerModel.getCurrentSongId();
				if(itemId.equals(currentSongId)){
					if(PlayerModel.PlayerStatus.paused.equals(playerModel.getPlayerStatus())){
						String url = getApplication().getURL() + "VAADIN/themes/" + getApplication().getTheme() + "/" + "images/pause_20.jpg";
						lStatus.setValue("<img src='" + url  + "'/>");
					}
					else if(PlayerModel.PlayerStatus.playing.equals(playerModel.getPlayerStatus())){
						String url = getApplication().getURL() + "VAADIN/themes/" + getApplication().getTheme() + "/" + "images/play_20.jpg";
						lStatus.setValue("<img src='" + url  + "'/>");
					}
				}				
				
				return lStatus;
			}
		});
		playlistTable.addGeneratedColumn("artistSearchButton", new ArtistLinkTableColumnGenerator("artist", "artistSearchId"));
		playlistTable.addGeneratedColumn("albumSearchButton", new AlbumLinkTableColumnGenerator("album", "albumSearchId"));
		playlistTable.addGeneratedColumn("optionsButton", new Table.ColumnGenerator()
		{
			public Object generateCell(final Table source, Object itemId, Object columnId) 
			{
				ThemeResource icon = new ThemeResource("images/options_20.jpg");
				PopupButton bOptions = new PopupButton();
				bOptions.setDescription("Click per mostrar les opcions sobre la cançó");
				bOptions.addStyleName(BaseTheme.BUTTON_LINK);
				bOptions.setIcon(icon);
				bOptions.setComponent(new PlaylistTableEntryOptions(playerModel, source, itemId));
				
				return bOptions;
			}
		});
		// Propietats de les col.lumnes
		playlistTable.setColumnWidth("status", 22);
		playlistTable.setColumnWidth("optionsButton", 42);
		playlistTable.setVisibleColumns(new String[]{"status", "song", "artistSearchButton", "albumSearchButton", "length", "optionsButton"});
		// Estil de les files / columnes de la taula
		playlistTable.setCellStyleGenerator(new Table.CellStyleGenerator() 
		{
			public String getStyle(Object itemId, Object propertyId) 
		    {
				// El CSS de la fila es determina quan propertyId==null
				if(propertyId==null){
					ILinkDTO link = (ILinkDTO)playlistTable.getContainerDataSource().getContainerProperty(itemId, "link").getValue();
					if(link==null){
						return "disabled";
					}
				}
				
				return null;
		    }
		});
		// Doble click per reproduir la cançó
		// Per no perdré la sel.lecció en el segon click...
		playlistTable.setNullSelectionAllowed(false);
		playlistTable.addListener(new ItemClickEvent.ItemClickListener()    
		{
            public void itemClick(ItemClickEvent event) 
            {
                if(event.isDoubleClick()){
                    String itemId = (String)event.getItemId();
                    if(itemId!=null){
                    	if(!playSong(itemId)){
                    		// TODO Mostrar avís en cas d'error
                    	}
    				}
                }
            }
        });
		// Tooltip de les files
		playlistTable.setItemDescriptionGenerator(new ItemDescriptionGenerator() 
		{                            
		    public String generateDescription(Component source, Object itemId, Object propertyId) 
		    {
		        if(propertyId==null){
		            return "Doble click per reproduir la cançó";
		        } 
		        		      
		        return null;
		    }
		});
		// S'habilita el drag and drop de les files
		playlistTable.setDragMode(TableDragMode.ROW);		
		midLayout.addComponent(playlistTable);
		midLayout.setExpandRatio(playlistTable, 2);
		
		/* Lyrics */
		pLyricsArea = new Panel();
		pLyricsArea.addStyleName(Runo.PANEL_LIGHT);
		pLyricsArea.setWidth("300px");
		pLyricsArea.setHeight("100%");
		lyricsArea = new Label();
		lyricsArea.setContentMode(Label.CONTENT_XHTML);
		lyricsArea.setSizeFull();
		pLyricsArea.addComponent(lyricsArea);
		midLayout.addComponent(pLyricsArea);
		pLyricsArea.setVisible(false);
		
		layout.addComponent(midLayout);
		layout.setExpandRatio(midLayout, 2);
		
		return layout;
	}

	private boolean playSong(String songId)
	{
		boolean result = player.playSong(songId);
		
		// Es carrega la lletra de la cançó si la vista està habilitada
		if(playerModel.isLyricsVisible()){
			try{
				ILyricsSearchService lyricsSearchService = (ILyricsSearchService)EchidnaServiceLocator.getInstance().getInstance(ILyricsSearchService.class);
				PlayerEntry currentEntry = playerModel.getCurrentPlayerEntry();
				if(currentEntry!=null){
					String lyrics = lyricsSearchService.findLyrics(currentEntry.getArtist(), currentEntry.getSong());
					if(StringUtils.isNotBlank(lyrics)){
						lyricsArea.setValue(lyrics);
					}
					else{
						lyricsArea.setValue("-- Lletra de la cançó no disponible --");
					}
				}
			}
			catch(BussinessException e){
				log.error("[PlayerView::playSong] Error en mostrar la lletra de la cançó: " + e.getMessage(), e);
				getWindow().showNotification("Error en mostrar la lletra de la cançó", Notification.TYPE_ERROR_MESSAGE);
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public void entryAdded(PlayerEntry entry)
	{
		if(entry.getLink()==null){
			try{
				GenericResultSetDTO<ILinkDTO> resultset = contentSearchService.findSongLinksBySongProviderData(entry.getProvider(), entry.getProviderId());
				if(resultset.getTotalCount()>0){
					ILinkDTO link = resultset.getResultList().get(0);
					entry.setLink(link);
					log.debug("[PlayerView::entryAdded] Afegida entrada " + entry.getSong() + "; obtingut " + link.getLinkSearchId());
				}
				else{
					log.debug("[PlayerView::entryAdded] No s'han obtingut resultats per " + entry.getSong());
				}
			}
			catch(BussinessException e){
				log.error("[PlayerView::entryAdded] Error en obtenir els links de la cançó " + entry.getSong() + "/" + entry.getArtist());
			}
		}
		((BeanContainer<String, PlayerModel.PlayerEntry>)playlistTable.getContainerDataSource()).addBean(entry);
	}
	
	public void entryModified(PlayerEntry entry)
	{
		String currentSongId = playerModel.getCurrentSongId();
		// Es reinicia la reproducció si s'ha modificat la cançó actual
		if(currentSongId!=null && currentSongId.equals(entry.getId())){
			playSong(currentSongId);
		}
		else{
			playlistTable.refreshRowCache();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void entryRemoved(PlayerEntry entry, boolean currentSongChanged)
	{
		((BeanContainer<String, PlayerModel.PlayerEntry>)playlistTable.getContainerDataSource()).removeItem(entry.getId());
		
		if(currentSongChanged && PlayerModel.PlayerStatus.playing.equals(playerModel.getPlayerStatus())){
			String songId = playerModel.getCurrentSongId();
			// Variable destinada a controlar els bucles
			String originalSongId = songId;
			// Es controla el cas que sigui la última cançó de la llista
			if(songId==null && PlayerModel.PlayerMode.loop.equals(playerModel.getPlayerMode())){
				songId = playerModel.getFirstSongId(); 
			}
			// S'itera fins trobar una id valida
			boolean stop = false;
			while(songId!=null && !stop){
				stop = playSong(songId);
				songId = playerModel.getNextSongId();
				if(songId==null && PlayerModel.PlayerMode.loop.equals(playerModel.getPlayerMode())){
					songId = playerModel.getFirstSongId();
				}
				stop = stop || songId!=null && songId.equals(originalSongId);				
			}
			
			playlistTable.refreshRowCache();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void modelCleared()
	{
		((BeanContainer<String, PlayerModel.PlayerEntry>)playlistTable.getContainerDataSource()).removeAllItems();
	}

	public void playerStatusChanged(PlayerStatus newStatus,	PlayerStatus oldStatus) 
	{
		playlistTable.refreshRowCache();
	}
}