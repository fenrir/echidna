package org.fenrir.echidna.ui.view;

import java.util.Collection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.terminal.ClassResource;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;
import com.vaadin.ui.themes.Runo;
import org.fenrir.echidna.EchidnaApplication;
import org.fenrir.echidna.common.dto.GenericResultSetDTO;
import org.fenrir.echidna.common.dto.IAlbumSearchHitDTO;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;
import org.fenrir.echidna.core.service.IContentSearchService;
import org.fenrir.echidna.ui.model.PlayerModel;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120706
 */
@SuppressWarnings("serial")
public class ArtistView extends AbstractTabSheetView 
{
	private final Log log = LogFactory.getLog(ArtistView.class);

	private String artistProvider;
	private String artistProviderId;

	private Embedded embeddedCDCase;
	private Table albumTable;
	private Table recordingTable;
	
	private IContentSearchService contentSearchService;
	
	public ArtistView(String artistProvider, String artistProviderId)
	{
		super("org.fenrir.echidna.i18n.views");
		
		this.artistProvider = artistProvider;
		this.artistProviderId = artistProviderId;
		
		contentSearchService = (IContentSearchService)EchidnaServiceLocator.getInstance().getInstance(IContentSearchService.class);
	}
	
	protected AbstractLayout createContents()
	{
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		GridLayout contentLayout = new GridLayout(3, 2);
		contentLayout.setHeight("100%");
		contentLayout.setRowExpandRatio(1, 2);
		
		/* Capçaleres */
		HorizontalLayout toolbarLayout = new HorizontalLayout();
		Button bAddAll = new Button(getInternacionalizedString("artistView.buttonAddAll"));
		bAddAll.addListener(new Button.ClickListener() 
		{
			public void buttonClick(Button.ClickEvent event) 
			{
				@SuppressWarnings("unchecked")
				Collection<String> itemIds = (Collection<String>)recordingTable.getItemIds();
				for(String rowID:itemIds){
					Item item = recordingTable.getItem(rowID);
					String id = (String)item.getItemProperty("id").getValue();
					String provider = (String)item.getItemProperty("provider").getValue();
					String songName = (String)item.getItemProperty("title").getValue();
					String albumSearchId = (String)item.getItemProperty("albumSearchId").getValue();
					String albumName = (String)item.getItemProperty("albumName").getValue();
					String artistSearchId = (String)item.getItemProperty("artistSearchId").getValue();
					String artistName = (String)item.getItemProperty("artistName").getValue();
					String length = (String)item.getItemProperty("length").getValue();
					
					PlayerModel playerModel = ((EchidnaApplication)getApplication()).getPlayerModel();
					PlayerModel.PlayerEntry entry = new PlayerModel.PlayerEntry();
					entry.setId(provider + "@" + id);
					entry.setSong(songName);
					entry.setAlbum(albumName);
					entry.setAlbumSearchId(albumSearchId);
					entry.setArtist(artistName);
					entry.setArtistSearchId(artistSearchId);
					entry.setLength(length);
					playerModel.addPlayerEntry(entry);
				}
			}
		});
		toolbarLayout.addComponent(bAddAll);
		contentLayout.addComponent(toolbarLayout, 1, 0);
		/* Llistes de cançóns i albums */
		embeddedCDCase = new Embedded();
		embeddedCDCase.setType(Embedded.TYPE_IMAGE);
		embeddedCDCase.setMimeType("/image/jpg");
		embeddedCDCase.setWidth("186px");
		embeddedCDCase.setHeight("200px");
		contentLayout.addComponent(embeddedCDCase, 0, 1);
		contentLayout.setComponentAlignment(embeddedCDCase, Alignment.TOP_CENTER);
		/* Lista de cançons de l'àlbum sel.leccionat */
		BeanContainer<String, IRecordingSearchHitDTO> songBeans = new BeanContainer<String, IRecordingSearchHitDTO>(IRecordingSearchHitDTO.class);
		songBeans.setBeanIdProperty("id");
		recordingTable = new Table(null, songBeans);
		recordingTable.addStyleName(Runo.TABLE_BORDERLESS);
		recordingTable.setHeight("100%");
		recordingTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);
		recordingTable.setVisibleColumns(new Object[]{"title", "length"});
		contentLayout.addComponent(recordingTable, 1, 1);

		// Definició de la taula que contindrà els àlbums
		BeanContainer<String, IAlbumSearchHitDTO> albumBeans = new BeanContainer<String, IAlbumSearchHitDTO>(IAlbumSearchHitDTO.class);
		albumBeans.setBeanIdProperty("id");
		albumTable = new Table(null, albumBeans);
		albumTable.setHeight("100%");
		// S'indica que es comuniqui els canvis immediatament al servidor (per la sel.lecció)
		albumTable.setImmediate(true);
		albumTable.setSelectable(true);
		albumTable.addStyleName(Runo.TABLE_BORDERLESS);
		// Es defineix les propietats de les dades que contindran les columnes
		albumTable.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);
		albumTable.setVisibleColumns(new Object[]{"name", "firstReleaseYear"});
		// Listener de sel.lecció
		albumTable.addListener(new Property.ValueChangeListener() 
		{
		    public void valueChange(ValueChangeEvent event) 
		    {
		    	// S'emplena la taula de l'album sel.leccionat amb les seves cançons
		    	String selectedAlbumId = (String)albumTable.getValue();
				if(selectedAlbumId!=null){
					@SuppressWarnings("unchecked")
					BeanContainer<String, IAlbumSearchHitDTO> beans = ((BeanContainer<String, IAlbumSearchHitDTO>)albumTable.getContainerDataSource());
					IAlbumSearchHitDTO selectedAlbum = beans.getItem(selectedAlbumId).getBean();
					String provider = selectedAlbum.getProvider();
					String albumId = selectedAlbum.getId();
					showRecordingList(provider, albumId);
				}
		    }
		});
		contentLayout.addComponent(albumTable, 2, 1);
		mainLayout.addComponent(contentLayout);
		mainLayout.setComponentAlignment(contentLayout, Alignment.TOP_CENTER);
		
		return mainLayout;
	}
	
	@SuppressWarnings("unchecked")
	public void attach()
	{
		super.attach();
		
		embeddedCDCase.setSource(new ClassResource("/org/fenrir/echidna/web/images/cd_case.jpg", getApplication()));
		
		String selectedAlbumId = null;
		// S'emplena la taula d'àlbums amb els resultats obtinguts
		try{
			GenericResultSetDTO<IAlbumSearchHitDTO> resultset = contentSearchService.findAlbumsByProviderArtistData(artistProvider, artistProviderId);
			BeanContainer<String, IAlbumSearchHitDTO> beans = ((BeanContainer<String, IAlbumSearchHitDTO>)albumTable.getContainerDataSource());
			beans.addAll(resultset.getResultList());
			beans.sort(new Object[]{"firstReleaseYear"}, new boolean[]{false});
			if(beans.size()>0){
				// Inicialment es mostrarà el primer àlbum de la llista
				selectedAlbumId = beans.getIdByIndex(0);					
				// L'event de sel.lecció s'encarregarà de mostrar les cançons associades a l'àlbum	
				albumTable.select(selectedAlbumId);
			}
		}
		catch(BussinessException e){
			log.error("[ArtistView::attach] Error en realitzar la cerca d'àlbums: " + e.getMessage(), e);
			getWindow().showNotification(getInternacionalizedString("artistView.albumSearchError"), Notification.TYPE_ERROR_MESSAGE);
		}
	}

	@SuppressWarnings("unchecked")
	private void showRecordingList(String provider, String albumId)
	{
		try{
			GenericResultSetDTO<IRecordingSearchHitDTO> resultset = contentSearchService.findRecordingsByProviderAlbumData(provider, albumId);
			BeanContainer<String, IRecordingSearchHitDTO> datasource = (BeanContainer<String, IRecordingSearchHitDTO>)recordingTable.getContainerDataSource();
			datasource.removeAllItems();
			datasource.addAll(resultset.getResultList());
			datasource.sort(new Object[]{"order"}, new boolean[]{true});
		}
		catch(BussinessException e){
			log.error("[ArtistView::showRecordingList] Error en realitzar la cerca de cançons per l'album " + albumId + ": " + e.getMessage(), e);			
			getWindow().showNotification(getInternacionalizedString("artistView.songsSearchError"), Notification.TYPE_ERROR_MESSAGE);
		}
	}
}
