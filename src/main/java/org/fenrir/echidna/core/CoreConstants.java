package org.fenrir.echidna.core;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120706
 */
public class CoreConstants 
{
	public static final String NAMED_INJECTION_APPLICATION_PROPERTIES = "applicationProperties";
	
	public static final String CONFIGURATION_WORKSPACE_FOLDER = "workspace.dir";
	public static final String CONFIGURATION_HTTP_PROXY_HOST = "http.proxyHost";
	public static final String CONFIGURATION_HTTP_PROXY_PORT = "http.proxyPort";
	public static final String CONFIGURATION_HTTPS_PROXY_HOST = "https.proxyHost";
	public static final String CONFIGURATION_HTTPS_PROXY_PORT = "https.proxyPort";	
	public static final String CONFIGURATION_USER_LANGUAGE = "user.language";
	
	public static final String WORKSPACE_IMAGE_FOLDER = "images";
	
	public static final String HTTP_PROXY_PORT_DEFAULT = "80";
	public static final String HTTPS_PROXY_PORT_DEFAULT = "80";
	
	public static final String CONTENT_PROVIDER_DATABASE = "DB";
}
