package org.fenrir.echidna.core.service;

import org.fenrir.echidna.core.entity.User;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120530
 */
public interface IUserAdministrationService 
{
	public User findUserByName(String username);
	public boolean userExists(String username);
	public void createUser(String name, String password) throws Exception;
}
