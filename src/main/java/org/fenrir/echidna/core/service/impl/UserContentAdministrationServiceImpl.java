package org.fenrir.echidna.core.service.impl;

import java.util.List;
import javax.inject.Inject;
import org.fenrir.echidna.core.service.IUserContentAdministrationService;
import org.fenrir.echidna.core.dao.IPlayslistDAO;
import org.fenrir.echidna.core.dao.IProviderRegistryDAO;
import org.fenrir.echidna.core.dao.IUserBookmarkDAO;
import org.fenrir.echidna.core.dao.IUserProviderRegistryDAO;
import org.fenrir.echidna.core.entity.Album;
import org.fenrir.echidna.core.entity.Artist;
import org.fenrir.echidna.core.entity.BookmarkType;
import org.fenrir.echidna.core.entity.Playlist;
import org.fenrir.echidna.core.entity.User;
import org.fenrir.echidna.core.entity.UserBookmark;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120721
 */
public class UserContentAdministrationServiceImpl implements IUserContentAdministrationService 
{
	@Inject
	private IPlayslistDAO playlistDAO;
	@Inject
	private IUserBookmarkDAO userBookmarkDAO;
	@Inject
	private IProviderRegistryDAO providerRegistryDAO;
	@Inject
	private IUserProviderRegistryDAO userProviderRegistryDAO;
	
	public void setplaylistDAO(IPlayslistDAO playlistDAO)
	{
		this.playlistDAO = playlistDAO;
	}
	
	public void setUserBookmarkDAO(IUserBookmarkDAO userBookmarkDAO)
	{
		this.userBookmarkDAO = userBookmarkDAO;
	}
	
	public void setProviderRegistryDAO(IProviderRegistryDAO providerRegistryDAO)
	{
		this.providerRegistryDAO = providerRegistryDAO;
	}
	
	public void setUserProviderRegistryDAO(IUserProviderRegistryDAO userProviderRegistryDAO)
	{
		this.userProviderRegistryDAO = userProviderRegistryDAO;
	}
	
	/* PLAYLISTS */
	public List<Playlist> findPlaylistsByUser(User user)
	{
		return playlistDAO.findPlaylistsByUser(user);
	}
	
	public void createPlaylist(String name, User user)
	{
		if(user==null){
			throw new IllegalArgumentException("La llista de reproducció ha d'estar associada a un usuari");
		}
		
		Playlist playlist = new Playlist();
		playlist.setName(name);
		playlist.setUser(user);
		
		playlistDAO.createPlaylist(playlist);
	}
	
	/* BOOKMARKS */
	public List<UserBookmark> findBookmarksByUser(User user)
	{
		return userBookmarkDAO.findBookmarksByUser(user);
	}
	
	public void createBookmark(Artist artist, User user)
	{
		UserBookmark bookmark = new UserBookmark();
		bookmark.setUser(user);
		bookmark.setType(BookmarkType.artist);
		bookmark.setEntityId(artist.getId());
		
		userBookmarkDAO.createBookmark(bookmark);
	}
	
	public void createBookmark(Album album, User user)
	{
		UserBookmark bookmark = new UserBookmark();
		bookmark.setUser(user);
		bookmark.setType(BookmarkType.album);
		bookmark.setEntityId(album.getId());
		
		userBookmarkDAO.createBookmark(bookmark);
	}
	
	/* REGISTRES DE PROVIDERS */
//	public void setPreferredLink(Song song, ILinkDTO link, User user)
//	{
//		// Es busca si ja existeix un registre de la cançó per l'usuari
//		providerRegistryDAO.findProviderRegistryByProviderData(ProviderRegistryType.link, link.getProvider(), link.getLinkId());
//	}
}
