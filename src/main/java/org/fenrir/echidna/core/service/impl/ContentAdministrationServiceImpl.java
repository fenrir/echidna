package org.fenrir.echidna.core.service.impl;

import java.util.List;
import javax.inject.Inject;
import com.google.inject.persist.Transactional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.common.dto.IAlbumSearchHitDTO;
import org.fenrir.echidna.common.dto.IArtistSearchHitDTO;
import org.fenrir.echidna.common.dto.ILinkDTO;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.core.CoreConstants;
import org.fenrir.echidna.core.dao.IAlbumDAO;
import org.fenrir.echidna.core.dao.IArtistDAO;
import org.fenrir.echidna.core.dao.IEntityRelationshipDAO;
import org.fenrir.echidna.core.dao.ILinkDAO;
import org.fenrir.echidna.core.dao.IProviderRegistryDAO;
import org.fenrir.echidna.core.dao.ISongDAO;
import org.fenrir.echidna.core.entity.Album;
import org.fenrir.echidna.core.entity.Artist;
import org.fenrir.echidna.core.entity.EntityRelationship;
import org.fenrir.echidna.core.entity.EntityRelationshipType;
import org.fenrir.echidna.core.entity.Link;
import org.fenrir.echidna.core.entity.LinkedEntityStatus;
import org.fenrir.echidna.core.entity.ProviderRegistry;
import org.fenrir.echidna.core.entity.ProviderRegistryType;
import org.fenrir.echidna.core.entity.Song;
import org.fenrir.echidna.core.service.IContentAdministrationService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120911
 */
public class ContentAdministrationServiceImpl implements IContentAdministrationService 
{
	private final Log log = LogFactory.getLog(ContentAdministrationServiceImpl.class); 

	@Inject
	private IArtistDAO artistDAO;
	@Inject
	private IAlbumDAO albumDAO;
	@Inject
	private ISongDAO songDAO;
	@Inject
	private ILinkDAO linkDAO;
	@Inject
	private IProviderRegistryDAO providerRegistryDAO;
	@Inject
	private IEntityRelationshipDAO entityRelationshipDAO;
	
	public void setArtistDAO(IArtistDAO artistDAO)
	{
		this.artistDAO = artistDAO;
	}
	
	public void setAlbumDAO(IAlbumDAO albumDAO)
	{
		this.albumDAO = albumDAO;
	}
	
	public void setSongDAO(ISongDAO songDAO)
	{
		this.songDAO = songDAO;
	}
	
	public void setLinkDAO(ILinkDAO linkDAO)
	{
		this.linkDAO = linkDAO;
	}
	
	public void setProviderRegistryDAO(IProviderRegistryDAO providerRegistryDAO)
	{
		this.providerRegistryDAO = providerRegistryDAO;
	}
	
	public void setEntityRelationshipDAO(IEntityRelationshipDAO entityRelationshipDAO)
	{
		this.entityRelationshipDAO = entityRelationshipDAO;
	}

	@Transactional
	public Artist createOrUpdateArtist(IArtistSearchHitDTO artist)
	{
		return createOrUpdateArtist(artist.getProvider(), artist.getId(), artist.getName()); 
	}
	
	@Transactional
	public Artist createOrUpdateArtist(String provider, String providerId, String name)
	{
		Artist artist = null;
		if(CoreConstants.CONTENT_PROVIDER_DATABASE.equals(provider)){
			log.warn("[ContentAdministrationServiceImpl::createOrUpdateArtist] Atenció!!! S'intenta crear un artista amb proveidor " + CoreConstants.CONTENT_PROVIDER_DATABASE);
			artist = artistDAO.findArtistById(new Long(providerId));
		}
		else{
			// Cerca a BDD per la parella provider - providerId
			ProviderRegistry artistRegistry = providerRegistryDAO.findProviderRegistryByProviderData(ProviderRegistryType.artist, provider, providerId);
			if(artistRegistry!=null){
				log.info("[ContentAdministrationServiceImpl::createOrUpdateSong] Registre ja existent a BDD provider:" + provider + "; providerId:" + providerId);
				artist = artistDAO.findArtistById(artistRegistry.getEntityId());
			}
		}
		
		/* Create */
		if(artist==null){
			artist = new Artist();
			artist.setName(name);
			artist.setAlbumsStatus(LinkedEntityStatus.empty);
			artist = artistDAO.createArtist(artist);
			
			ProviderRegistry providerRegistry = new ProviderRegistry();
			providerRegistry.setType(ProviderRegistryType.artist);
			providerRegistry.setEntityId(artist.getId());
			providerRegistry.setProvider(provider);
			providerRegistry.setProviderId(providerId);
			providerRegistryDAO.createProviderRegistry(providerRegistry);
		}
		/* Update */
		else{
			boolean updated = false;
			if(name!=null && !name.equals(artist.getName())){
				artist.setName(name);
				updated = true;
			}			
					
			if(updated){
				artistDAO.updateArtist(artist);
			}
		}
		
		return artist;
	}
	
	@Transactional
	public void updateArtistContent(Artist artist, List<IAlbumSearchHitDTO> albums, LinkedEntityStatus status)
	{
		/* Es creen o s'actualitzen els àlbums de l'artista */
		for(IAlbumSearchHitDTO album:albums){
			// S'inclouen els resultats obtinguts a la BDD per posteriors cerques
			createOrUpdateAlbum(artist, album);
		}
		
		/* S'actualitza l'estat dels àlbums de l'artista
		 * Només s'actualitza si és un estat diferent i l'estat actual != complet
		 */
		if(status.equals(artist.getAlbumsStatus()) && !LinkedEntityStatus.complete.equals(artist.getAlbumsStatus())){
			log.info("[ContentAdministrationServiceImpl::updateArtistContent] Descartada actualització de l'estat dels àlbums de l'artista=" + artist.getId() + "; status=" + status );
		}
		else{
			artist.setAlbumsStatus(status);
			artistDAO.updateArtist(artist);
		}
	}
	
	@Transactional
	public Album createOrUpdateAlbum(Artist artist, IAlbumSearchHitDTO album)
	{
		return createOrUpdateAlbum(album.getProvider(), album.getId(), artist, album.getName(), album.getFirstReleaseDate()); 
	}
	
	@Transactional
	public Album createOrUpdateAlbum(String provider, String providerId, Artist artist, String name, String firstReleaseDate)
	{
		Album album = null;
		if(CoreConstants.CONTENT_PROVIDER_DATABASE.equals(provider)){
			log.warn("[ContentAdministrationServiceImpl::createOrUpdateAlbum] Atenció!!! S'intenta crear un àlbum amb proveidor " + CoreConstants.CONTENT_PROVIDER_DATABASE);
			album = albumDAO.findAlbumById(new Long(providerId));
		}
		else{
			// Cerca a BDD per la parella provider - providerId
			ProviderRegistry albumRegistry = providerRegistryDAO.findProviderRegistryByProviderData(ProviderRegistryType.album, provider, providerId);
			if(albumRegistry!=null){
				log.info("[ContentAdministrationServiceImpl::createOrUpdateSong] Registre ja existent a BDD provider:" + provider + "; providerId:" + providerId);
				album = albumDAO.findAlbumById(albumRegistry.getEntityId());
			}
		}
		
		/* Create */
		if(album==null){
			album = new Album();
			album.setArtist(artist);
			album.setName(name);
			album.setFirstReleaseDate(firstReleaseDate);
			album.setSongsStatus(LinkedEntityStatus.empty);
			album = albumDAO.createAlbum(album);
			
			ProviderRegistry providerRegistry = new ProviderRegistry();
			providerRegistry.setType(ProviderRegistryType.album);
			providerRegistry.setEntityId(album.getId());
			providerRegistry.setProvider(provider);
			providerRegistry.setProviderId(providerId);
			providerRegistryDAO.createProviderRegistry(providerRegistry);
		}
		/* Update */
		else{
			boolean updated = false;
			if(artist!=null && !artist.equals(album.getArtist())){ 
				album.setArtist(artist);
				updated = true;
			}
			if(name!=null && !name.equals(album.getName())){
				album.setName(name);
				updated = true;
			}
			if(firstReleaseDate!=null && !firstReleaseDate.equals(album.getFirstReleaseDate())){
				album.setFirstReleaseDate(firstReleaseDate);
				updated = true;
			}
					
			if(updated){
				albumDAO.updateAlbum(album);
			}
		}
		
		return album;
	}
	
	@Transactional
	public void updateAlbumContent(Album album, List<IRecordingSearchHitDTO> songs, LinkedEntityStatus status)
	{
		/* Es creen o s'actualitzen les cançons de l'àlbum */
		for(IRecordingSearchHitDTO song:songs){
			// S'inclouen els resultats obtinguts a la BDD per posteriors cerques
			createOrUpdateSong(album, song);
		}
		
		/* S'actualitza l'estat de les cançons de l'àlbum
		 * Només s'actualitza si és un estat diferent i l'estat actual != complet
		 */
		if(status.equals(album.getSongsStatus()) && !LinkedEntityStatus.complete.equals(album.getSongsStatus())){
			log.info("[ContentAdministrationServiceImpl::updateAlbumContent] Descartada actualització de l'estat de les cançons de l'àlbum=" + album.getId() + "; status=" + status );
		}
		else{
			album.setSongsStatus(status);
			albumDAO.updateAlbum(album);
		}
	}
	
	@Transactional
	public Song createOrUpdateSong(Album album, IRecordingSearchHitDTO recording)
	{
		return createOrUpdateSong(recording.getProvider(), recording.getId(), album, recording.getTitle(), recording.getLengthMillis(), recording.getOrder()); 
	}
	
	@Transactional
	public Song createOrUpdateSong(String provider, String providerId, Album album,	String name, Long length, Integer order)
	{
		Song song = null;
		if(CoreConstants.CONTENT_PROVIDER_DATABASE.equals(provider)){
			log.warn("[ContentAdministrationServiceImpl::createOrUpdateSong] Atenció!!! S'intenta crear una cançó amb proveidor " + CoreConstants.CONTENT_PROVIDER_DATABASE);
			song = songDAO.findSongById(new Long(providerId));
		}
		else{
			// Cerca a BDD per la parella provider - providerId
			ProviderRegistry songRegistry = providerRegistryDAO.findProviderRegistryByProviderData(ProviderRegistryType.song, provider, providerId);
			if(songRegistry!=null){
				log.info("[ContentAdministrationServiceImpl::createOrUpdateSong] Registre ja existent a BDD provider:" + provider + "; providerId:" + providerId);
				song = songDAO.findSongById(songRegistry.getEntityId());
			}
		}
		
		/* Create */
		if(song==null){
			song = new Song();
			song.setAlbum(album);
			song.setName(name);
			song.setLength(length);
			song.setOrder(order);
			song = songDAO.createSong(song);
			
			ProviderRegistry providerRegistry = new ProviderRegistry();
			providerRegistry.setType(ProviderRegistryType.song);
			providerRegistry.setEntityId(song.getId());
			providerRegistry.setProvider(provider);
			providerRegistry.setProviderId(providerId);
			providerRegistryDAO.createProviderRegistry(providerRegistry);
		}
		/* Update */
		else{
			boolean updated = false;
			if(album!=null && !album.equals(song.getAlbum())){ 
				song.setAlbum(album);
				updated = true;
			}
			if(name!=null && !name.equals(song.getName())){
				song.setName(name);
				updated = true;
			}
			if(length!=null && length.equals(song.getLength())){
				song.setLength(length);
				updated = true;
			}
			if(order!=null && order.equals(song.getOrder())){
				song.setOrder(order);
				updated = true;
			}
			
			if(updated){
				songDAO.updateSong(song);
			}
		}
		
		return song;
	}

	@Transactional
	public Link createOrUpdateLink(ILinkDTO linkDTO)
	{
		Link link = null;
		if(CoreConstants.CONTENT_PROVIDER_DATABASE.equals(linkDTO.getProvider())){
			log.warn("[ContentAdministrationServiceImpl::createOrUpdateLink] Atenció!!! S'intenta crear una link amb proveidor " + CoreConstants.CONTENT_PROVIDER_DATABASE);
			link = linkDAO.findLinkById(new Long(linkDTO.getId()));
		}
		else{
			// Cerca a BDD per la parella provider - providerId
			ProviderRegistry linkRegistry = providerRegistryDAO.findProviderRegistryByProviderData(ProviderRegistryType.link, linkDTO.getProvider(), linkDTO.getId());
			if(linkRegistry!=null){
				log.info("[ContentAdministrationServiceImpl::createOrUpdateLink] Registre ja existent a BDD provider:" + linkDTO.getProvider() + "; providerId:" + linkDTO.getId());
				link = linkDAO.findLinkById(linkRegistry.getEntityId());
			}
		}
		
		/* Create */
		if(link==null){
			link = new Link();
			link.setAuthor(linkDTO.getAuthor());
			link.setDescription(linkDTO.getDescription());
			link.setThumbnail(linkDTO.getThumbnailUrl());
			link.setTitle(linkDTO.getTitle());
			link.setUrl(linkDTO.getUrl());
			link.setLinkSearchId(linkDTO.getLinkSearchId());
			link = linkDAO.createLink(link);
			
			ProviderRegistry providerRegistry = new ProviderRegistry();
			providerRegistry.setType(ProviderRegistryType.link);
			providerRegistry.setEntityId(link.getId());
			providerRegistry.setProvider(linkDTO.getProvider());
			providerRegistry.setProviderId(linkDTO.getId());
			providerRegistryDAO.createProviderRegistry(providerRegistry);
		}
		/* Update */
		else{
			boolean updated = false;
			if(linkDTO.getAuthor()!=null && !linkDTO.getAuthor().equals(link.getAuthor())){ 
				link.setAuthor(linkDTO.getAuthor());
				updated = true;
			}
			if(linkDTO.getDescription()!=null && !linkDTO.getDescription().equals(link.getDescription())){
				link.setDescription(linkDTO.getDescription());
				updated = true;
			}
			if(linkDTO.getTitle()!=null && !linkDTO.getTitle().equals(link.getTitle())){
				link.setTitle(linkDTO.getTitle());
				updated = true;
			}
			if(linkDTO.getUrl()!=null && !linkDTO.getUrl().equals(link.getUrl())){
				link.setUrl(linkDTO.getUrl());
				updated = true;
			}
			if(linkDTO.getLinkSearchId()!=null && !linkDTO.getLinkSearchId().equals(link.getLinkSearchId())){
				link.setLinkSearchId(linkDTO.getLinkSearchId());
				updated = true;
			}
			if(linkDTO.getThumbnailUrl()!=null && !linkDTO.getThumbnailUrl().equals(link.getThumbnail())){
				link.setThumbnail(linkDTO.getThumbnailUrl());
				updated = true;
			}
			
			if(updated){
				linkDAO.updateLink(link);
			}
		}
		
		return link;
	}
	
	@Transactional
	public void upvoteSongLink(IRecordingSearchHitDTO recordingDTO, ILinkDTO linkDTO) throws BussinessException
	{
		Song song = null;
		if(CoreConstants.CONTENT_PROVIDER_DATABASE.equals(recordingDTO.getProvider())){
			song = songDAO.findSongById(new Long(recordingDTO.getId()));
		}
		else{
			// Cerca a BDD per la parella provider - providerId
			ProviderRegistry songRegistry = providerRegistryDAO.findProviderRegistryByProviderData(ProviderRegistryType.song, recordingDTO.getProvider(), recordingDTO.getId());
			if(songRegistry!=null){
				song = songDAO.findSongById(songRegistry.getEntityId());
			}
		}
		
		Link link = null;
		if(CoreConstants.CONTENT_PROVIDER_DATABASE.equals(linkDTO.getProvider())){
			link = linkDAO.findLinkById(new Long(linkDTO.getId()));
		}
		else{
			// Cerca a BDD per la parella provider - providerId
			ProviderRegistry linkRegistry = providerRegistryDAO.findProviderRegistryByProviderData(ProviderRegistryType.link, linkDTO.getProvider(), linkDTO.getId());
			if(linkRegistry!=null){
				link = linkDAO.findLinkById(linkRegistry.getEntityId());
			}
		}
		
		if(song==null){
			throw new BussinessException("No es pot relacionar la cançó amb el link perquè la cançó no existeix a base de dades");
		}
		// En cas que no existeixi el link a base de dades es crea
		if(link==null){
			link = createOrUpdateLink(linkDTO);
			associateLinkToSong(song, link);
		}
		upvoteSongLink(song, link);
	}
	
	@Transactional
	public void upvoteSongLink(Song song, Link link) throws BussinessException
	{
		// Es comproba si existeix la relació i es crea en cas de no existir
		EntityRelationship relationship = entityRelationshipDAO.findRelationshipByEntitiesType(EntityRelationshipType.songLinks, song.getId(), link.getId());
		if(relationship==null){
			log.info("[ContentAdministrationServiceImpl::rateSongLink] Registre inexistent a BDD song:" + song.getId() + "; link:" + link.getId() + ". Es crea una nova relació");
			relationship = associateLinkToSong(song, link);
		}
		// TODO Mirar una manera més centralitzada de decidir l'score a associar
		long currentScore = relationship.getScore();
		relationship.setScore(currentScore + 1L);
		entityRelationshipDAO.updateRelationship(relationship);
	}
	
	@Transactional
	public EntityRelationship associateLinkToSong(Song song, Link link) throws BussinessException
	{
		// Es comproba si existeix la relació i es crea en cas de no existir
		EntityRelationship relationship = entityRelationshipDAO.findRelationshipByEntitiesType(EntityRelationshipType.songLinks, song.getId(), link.getId());
		if(relationship!=null){
			throw new BussinessException("Relació ja existent entre song:" + song.getId() + "; link:" + link.getId());
		}
		else{
			relationship = new EntityRelationship();
			relationship.setEntity1(song.getId());
			relationship.setEntity2(link.getId());
			relationship.setType(EntityRelationshipType.songLinks);
			// TODO Mirar una manera més centralitzada de decidir l'score a associar
			relationship.setScore(1L);
			entityRelationshipDAO.createRelationship(relationship);
		}
		
		return relationship;
	}
	
	@Transactional
	public void disassociateLinkToSong(Song song, Link link) throws BussinessException
	{
		EntityRelationship relationship = entityRelationshipDAO.findRelationshipByEntitiesType(EntityRelationshipType.songLinks, song.getId(), link.getId());
		if(relationship==null){
			log.info("[ContentAdministrationServiceImpl::disassociateLinkToSong] El registre no existeix a BDD song:" + song.getId() + "; link:" + link.getId());
			throw new BussinessException("Relació no existent entre song:" + song.getId() + "; link:" + link.getId());
		}
		else{
			entityRelationshipDAO.deleteRelationship(relationship);
		}
	}
}
