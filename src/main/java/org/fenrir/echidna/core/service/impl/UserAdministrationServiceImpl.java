package org.fenrir.echidna.core.service.impl;

import javax.inject.Inject;
import com.google.inject.persist.Transactional;
import org.fenrir.echidna.core.dao.IUserDAO;
import org.fenrir.echidna.core.entity.User;
import org.fenrir.echidna.core.security.KeyStoreUtils;
import org.fenrir.echidna.core.service.IUserAdministrationService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20130530
 */
public class UserAdministrationServiceImpl implements IUserAdministrationService 
{
	@Inject
	private IUserDAO userDAO; 

	@Transactional
	public User findUserByName(String username)
	{
		return userDAO.findUserByName(username);
	}
	
	@Transactional
	public boolean userExists(String username)
	{
		return userDAO.findUserByName(username)!=null;
	}
	
	@Transactional	
	public void createUser(String name, String password) throws Exception
	{
		// Si no existeix la clau simètrica per xifrar els password, es crea
		if(!KeyStoreUtils.existsKeyStoreFile()){
			KeyStoreUtils.createKeyStore();
		}
		// Es crea el registre de l'usuari amb el password xifrat
		User user = new User();
		user.setName(name);
		String encryptedPassword = KeyStoreUtils.encrypt(password);
		user.setPassword(encryptedPassword);
		user = userDAO.createUser(user);	
	}
}
