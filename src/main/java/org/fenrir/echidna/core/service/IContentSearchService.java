package org.fenrir.echidna.core.service;

import org.fenrir.echidna.common.dto.GenericResultSetDTO;
import org.fenrir.echidna.common.dto.IAlbumSearchHitDTO;
import org.fenrir.echidna.common.dto.IArtistSearchHitDTO;
import org.fenrir.echidna.common.dto.ILinkDTO;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.common.exception.BussinessException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120820
 */
public interface IContentSearchService 
{
	public GenericResultSetDTO<IArtistSearchHitDTO> findArtistsByName(String name) throws BussinessException;
	
	public GenericResultSetDTO<IAlbumSearchHitDTO> findAlbumsByName(String name) throws BussinessException; 
	public GenericResultSetDTO<IAlbumSearchHitDTO> findAlbumsByProviderArtistData(String provider, String providerId) throws BussinessException;
	
	public GenericResultSetDTO<IRecordingSearchHitDTO> findRecordingsByName(String name) throws BussinessException; 
	public GenericResultSetDTO<IRecordingSearchHitDTO> findRecordingsByProviderAlbumData(String provider, String providerId) throws BussinessException;
	public IRecordingSearchHitDTO findRecordingByProviderData(String provider, String providerId) throws BussinessException;
	
	public GenericResultSetDTO<ILinkDTO> findLinks(String term) throws BussinessException;
	public GenericResultSetDTO<ILinkDTO> findSongLinksBySongProviderData(String provider, String providerId) throws BussinessException;
}
