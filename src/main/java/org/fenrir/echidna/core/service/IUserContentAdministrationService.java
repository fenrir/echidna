package org.fenrir.echidna.core.service;

import java.util.List;
import org.fenrir.echidna.core.entity.Album;
import org.fenrir.echidna.core.entity.Artist;
import org.fenrir.echidna.core.entity.Playlist;
import org.fenrir.echidna.core.entity.User;
import org.fenrir.echidna.core.entity.UserBookmark;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120721
 */
public interface IUserContentAdministrationService 
{
	/* PLAYLISTS */
	public List<Playlist> findPlaylistsByUser(User user);
	
	public void createPlaylist(String name, User user);
	
	/* BOOKMARKS */
	public List<UserBookmark> findBookmarksByUser(User user);
	
	public void createBookmark(Artist artist, User user);
	public void createBookmark(Album album, User user);
}
