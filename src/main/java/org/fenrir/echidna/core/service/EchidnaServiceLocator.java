package org.fenrir.echidna.core.service;

import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120706
 */
public class EchidnaServiceLocator 
{
	private static EchidnaServiceLocator instance;
	
	private Injector injector;
	
	private EchidnaServiceLocator()
	{
		
	}
	
	public static EchidnaServiceLocator getInstance()
	{
		if(instance==null){
			instance = new EchidnaServiceLocator();
		}
		
		return instance;
	}
	
	public void setInjector(Injector injector)
	{
		this.injector = injector;
	}
	
	public Object getInstance(Class<?> clazz)
	{
		return injector.getInstance(clazz);
	}
	
	public Object getInstance(Class<?> clazz, String name)
	{
		Key<?> key = Key.get(clazz, Names.named(name));
		return injector.getInstance(key);
	}
	
	public void injectMembers(Object object)
	{
		injector.injectMembers(object);
	}
}
