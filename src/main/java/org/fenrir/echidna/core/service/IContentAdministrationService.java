package org.fenrir.echidna.core.service;

import java.util.List;
import org.fenrir.echidna.common.dto.IAlbumSearchHitDTO;
import org.fenrir.echidna.common.dto.IArtistSearchHitDTO;
import org.fenrir.echidna.common.dto.ILinkDTO;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.core.entity.Album;
import org.fenrir.echidna.core.entity.Artist;
import org.fenrir.echidna.core.entity.EntityRelationship;
import org.fenrir.echidna.core.entity.Link;
import org.fenrir.echidna.core.entity.LinkedEntityStatus;
import org.fenrir.echidna.core.entity.Song;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120908
 */
public interface IContentAdministrationService 
{
	public Artist createOrUpdateArtist(IArtistSearchHitDTO artist);
	public Artist createOrUpdateArtist(String provider, String providerId, String name);
	public void updateArtistContent(Artist artist, List<IAlbumSearchHitDTO> albums, LinkedEntityStatus status);
	
	public Album createOrUpdateAlbum(Artist artist, IAlbumSearchHitDTO album);
	public Album createOrUpdateAlbum(String provider, String providerId, Artist artist, String name, String firstReleaseDate);
	public void updateAlbumContent(Album album, List<IRecordingSearchHitDTO> songs, LinkedEntityStatus status);

	public Song createOrUpdateSong(Album album, IRecordingSearchHitDTO recording);
	public Song createOrUpdateSong(String provider, String providerId, Album album,	String name, Long length, Integer order);

	public Link createOrUpdateLink(ILinkDTO link);
	
	/**
	 * Mètode que s'encarrega d'augmentar l'score de l'associació del link i la cançó especificats.
	 * Tant en el cas del link com de la cançó, es farà una cerca a base de dades per trobar el registre
	 * de Link i Song a través de la ID si el proveidor es BDD o a través del ProviderRegistry associat
	 * al proveidor en cas de ser un registre provinent directament d'aquest. En cas de no trobar el
	 * Link es creará associant-lo a la cançó especificada. Si no es troba el registre de la cançó es 
	 * llançarà una excepció en no tenir totes les dades necessaries per registrar-la.  
	 * @param recordingDTO IRecordingSearchHitDTO - Cançó que es vol associar al link
	 * @param linkDTO ILinkDTO - Link que es vol associar a la cançó
	 * @throws BussinessException - Si el registre de la cançó no existeix a la base de dades
	 */
	public void upvoteSongLink(IRecordingSearchHitDTO recordingDTO, ILinkDTO linkDTO) throws BussinessException;
	
	/**
	 * Mètode que s'encarrega d'augmentar l'score de l'associació del link i la cançó especificats.
	 * Tant el registre de la cançó com el del link ja han d'estar registrats a la base de dades.
	 * @param song Song - Cançó que es vol associar al link
	 * @param link Link - Link que es vol associar a la cançó
	 * @throws BussinessException - Si succeeix un error en l'operació
	 */
	public void upvoteSongLink(Song song, Link link) throws BussinessException;
	
	/**
	 * Mètode que associa la cançó i el link especificats.  
	 * @param song Song - Cançó especificada en la relació que es vol crear 
	 * @param link Link - Link especificat en la relació que es vol crear
	 * @throws BussinessException - Si la relació entre la cançó i el link existeix
	 */
	public EntityRelationship associateLinkToSong(Song song, Link link) throws BussinessException;
	
	/**
	 * Mètode que desassocia la cançó i el link especificats. Només s'elimina la relació entre les
	 * 2 entitats. Tant el registre de la cançó com el de l'enllaç es mantenen a la base de dades. 
	 * @param song Song - Cançó especificada en la relació que es vol eliminar 
	 * @param link Link - Link especificat en la relació que es vol eliminar
	 * @throws BussinessException - Si la relació entre la cançó i el link no existeix
	 */
	public void disassociateLinkToSong(Song song, Link link) throws BussinessException;
}
