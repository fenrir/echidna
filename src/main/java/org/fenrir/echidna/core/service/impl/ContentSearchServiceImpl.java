package org.fenrir.echidna.core.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import com.google.inject.persist.Transactional;
import org.apache.commons.lang.text.StrTokenizer;
import org.apache.commons.lang.text.StrMatcher;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.common.dto.GenericResultSetDTO;
import org.fenrir.echidna.common.dto.IAlbumSearchHitDTO;
import org.fenrir.echidna.common.dto.IArtistSearchHitDTO;
import org.fenrir.echidna.common.dto.ILinkDTO;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.common.service.ILinkSearchService;
import org.fenrir.echidna.common.service.IMusicSearchService;
import org.fenrir.echidna.core.CoreConstants;
import org.fenrir.echidna.core.dao.IAlbumDAO;
import org.fenrir.echidna.core.dao.IArtistDAO;
import org.fenrir.echidna.core.dao.IEntityRelationshipDAO;
import org.fenrir.echidna.core.dao.ILinkDAO;
import org.fenrir.echidna.core.dao.IProviderRegistryDAO;
import org.fenrir.echidna.core.dao.ISongDAO;
import org.fenrir.echidna.core.dto.AlbumDTOAdapter;
import org.fenrir.echidna.core.dto.LinkDTOAdapter;
import org.fenrir.echidna.core.dto.SongDTOAdapter;
import org.fenrir.echidna.core.entity.Album;
import org.fenrir.echidna.core.entity.Artist;
import org.fenrir.echidna.core.entity.EntityRelationship;
import org.fenrir.echidna.core.entity.EntityRelationshipType;
import org.fenrir.echidna.core.entity.Link;
import org.fenrir.echidna.core.entity.LinkedEntityStatus;
import org.fenrir.echidna.core.entity.ProviderRegistry;
import org.fenrir.echidna.core.entity.ProviderRegistryType;
import org.fenrir.echidna.core.entity.Song;
import org.fenrir.echidna.core.service.IContentAdministrationService;
import org.fenrir.echidna.core.service.IContentSearchService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120903
 */
public class ContentSearchServiceImpl implements IContentSearchService 
{
	private final Log log = LogFactory.getLog(ContentSearchServiceImpl.class);
	
	@Inject
	private IMusicSearchService musicSearchService;
	@Inject 
	private ILinkSearchService linkSearchService;
	@Inject
	private IContentAdministrationService contentAdministrationService;
	@Inject
	private IArtistDAO artistDAO;
	@Inject
	private IAlbumDAO albumDAO;
	@Inject
	private ISongDAO songDAO;
	@Inject
	private ILinkDAO linkDAO;
	@Inject
	private IProviderRegistryDAO providerRegistryDAO;
	@Inject
	private IEntityRelationshipDAO entityRelationshipDAO;
	
	public void setMusicSearchService(IMusicSearchService musicSearchService)
	{
		this.musicSearchService = musicSearchService;
	}
	
	public void setLinkSearchService(ILinkSearchService linkSearchService)
	{
		this.linkSearchService = linkSearchService;
	}
	
	public void setContentAdministrationService(IContentAdministrationService contentAdministrationService)
	{
		this.contentAdministrationService = contentAdministrationService;
	}
	
	public void setArtistDAO(IArtistDAO artistDAO)
	{
		this.artistDAO = artistDAO;
	}
	
	public void setAlbumDAO(IAlbumDAO albumDAO)
	{
		this.albumDAO = albumDAO;
	}
	
	public void setSongDAO(ISongDAO songDAO)
	{
		this.songDAO = songDAO;
	}
	
	public void setLinkDAO(ILinkDAO linkDAO)
	{
		this.linkDAO = linkDAO;
	}
	
	public void setProviderRegistryDAO(IProviderRegistryDAO providerRegistryDAO)
	{
		this.providerRegistryDAO = providerRegistryDAO;
	}
	
	public void setEntityRelationshipDAO(IEntityRelationshipDAO entityRelationshipDAO)
	{
		this.entityRelationshipDAO = entityRelationshipDAO;
	}

	/* ARTISTS */
	public GenericResultSetDTO<IArtistSearchHitDTO> findArtistsByName(String name) throws BussinessException 
	{
		return musicSearchService.findArtistsByName(name);
	}

	/* ALBUMS */
	public GenericResultSetDTO<IAlbumSearchHitDTO> findAlbumsByName(String name) throws BussinessException 
	{
		return musicSearchService.findAlbumsByName(name);
	}

	@Transactional
	public GenericResultSetDTO<IAlbumSearchHitDTO> findAlbumsByProviderArtistData(String provider, String providerId) throws BussinessException 
	{
		Artist artist = null;
		// Cerca directament a la BDD si el proveïdor es BDD
		if(CoreConstants.CONTENT_PROVIDER_DATABASE.equals(provider)){
			artist = artistDAO.findArtistById(new Long(providerId));
		}
		else{
			// Cerca a BDD per la parella provider - providerId
			ProviderRegistry providerRegistry = providerRegistryDAO.findProviderRegistryByProviderData(ProviderRegistryType.artist, provider, providerId);
			if(providerRegistry!=null){
				artist = artistDAO.findArtistById(providerRegistry.getEntityId());
			}
		}
			
		// Si la BDD conté tots els àlbums de l'artista, no es consultarà el WS
		if(artist!=null && LinkedEntityStatus.complete.equals(artist.getAlbumsStatus())){
			log.info("[ContentSearchServiceImpl::findAlbumsByProviderArtistData] Utilitzant cerca de cache: artist=" + artist.getId());
			List<Album> albums = albumDAO.findAlbumsByArtist(artist);
			if(albums.isEmpty()){
				return GenericResultSetDTO.emptyResultSet();
			}
			else{
				List<IAlbumSearchHitDTO> resultList = new ArrayList<IAlbumSearchHitDTO>();
				for(Album album:albums){
					resultList.add(new AlbumDTOAdapter(album));					
				}
				
				return new GenericResultSetDTO<IAlbumSearchHitDTO>(resultList, resultList.size(), 0);
			}
		}
		// Si no s'ha trobat a base de dades es cerca al WS i s'inclouen els registres a la BDD
		else{
			log.info("[ContentSearchServiceImpl::findAlbumsByProviderArtistData] Utilitzant cerca a WS: artist=" + providerId);
			// Si el provider de l'àlbum és BDD, s'haurà d'obtenir el provider ID relatiu al WS
			if(CoreConstants.CONTENT_PROVIDER_DATABASE.equals(provider)){
				List<ProviderRegistry> artistRegistries = providerRegistryDAO.findProviderRegistriesByProviderTypeEntity(ProviderRegistryType.artist, new Long(providerId));
				providerId = artistRegistries.get(0).getProviderId();
			}
			GenericResultSetDTO<IAlbumSearchHitDTO> wsResultset = musicSearchService.findAlbumsByProviderArtistId(providerId);
			if(wsResultset.size()>0){
				IAlbumSearchHitDTO album = wsResultset.getResultList().get(0);
				artist = contentAdministrationService.createOrUpdateArtist(album.getProvider(), album.getArtistSearchId(), album.getArtistName());
				/* S'actualitza l'estat del sàlbums al registre de l'artista a LinkedEntityStatus.complete
				 * A partir d'ara les cerques sobre els àlbums de l'artista podràn fer-se sobre BDD
				 */
				contentAdministrationService.updateArtistContent(artist, wsResultset.getResultList(), LinkedEntityStatus.complete);
			}
			
			return wsResultset;
		}
	}

	/* RECORDINGS */
	public GenericResultSetDTO<IRecordingSearchHitDTO> findRecordingsByName(String name) throws BussinessException 
	{
		return musicSearchService.findRecordingsByName(name);
	}

	@Transactional
	public GenericResultSetDTO<IRecordingSearchHitDTO> findRecordingsByProviderAlbumData(String provider, String providerId) throws BussinessException 
	{
		Album album = null;
		// Cerca directament a la BDD si el proveïdor es BDD
		if(CoreConstants.CONTENT_PROVIDER_DATABASE.equals(provider)){
			album = albumDAO.findAlbumById(new Long(providerId));
		}
		else{
			// Cerca a BDD per la parella provider - providerId
			ProviderRegistry providerRegistry = providerRegistryDAO.findProviderRegistryByProviderData(ProviderRegistryType.album, provider, providerId);
			if(providerRegistry!=null){
				album = albumDAO.findAlbumById(providerRegistry.getEntityId());
			}
		}
			
		// Si la BDD conté totes les cançons de l'àlbum, no es consultarà el WS
		if(album!=null && LinkedEntityStatus.complete.equals(album.getSongsStatus())){
			log.info("[ContentSearchServiceImpl::findRecordingsByProviderArtistData] Utilitzant cerca de cache: album=" + album.getId());
			List<Song> songs = songDAO.findSongsByAlbum(album);
			if(songs.isEmpty()){
				return GenericResultSetDTO.emptyResultSet();
			}
			else{
				List<IRecordingSearchHitDTO> resultList = new ArrayList<IRecordingSearchHitDTO>();
				for(Song song:songs){
					resultList.add(new SongDTOAdapter(song));
				}
				
				return new GenericResultSetDTO<IRecordingSearchHitDTO>(resultList, resultList.size(), 0);
			}
		}
		// Si no s'ha trobat a base de dades es cerca al WS i s'inclouen els registres a la BDD
		else{
			log.info("[ContentSearchServiceImpl::findRecordingsByProviderArtistData] Utilitzant cerca a WS: album=" + providerId);
			// Si el provider de l'àlbum és BDD, s'haurà d'obtenir el provider ID relatiu al WS
			if(CoreConstants.CONTENT_PROVIDER_DATABASE.equals(provider)){
				List<ProviderRegistry> albumRegistries = providerRegistryDAO.findProviderRegistriesByProviderTypeEntity(ProviderRegistryType.album, new Long(providerId));
				providerId = albumRegistries.get(0).getProviderId();
			}
			GenericResultSetDTO<IRecordingSearchHitDTO> wsResultset = musicSearchService.findRecordingsByProviderAlbumId(providerId);
			if(wsResultset.size()>0){
				IRecordingSearchHitDTO song = wsResultset.getResultList().get(0);
				Artist artist = contentAdministrationService.createOrUpdateArtist(song.getProvider(), song.getArtistSearchId(), song.getArtistName());
				album = contentAdministrationService.createOrUpdateAlbum(song.getProvider(), song.getAlbumSearchId(), artist, song.getAlbumName(), null);
				/* S'actualitza l'estat de les cançons a l'album a LinkedEntityStatus.complete
				 * A partir d'ara les cerques sobre les cançons de l'album podràn fer-se sobre BDD
				 */
				contentAdministrationService.updateAlbumContent(album, wsResultset.getResultList(), LinkedEntityStatus.complete);
			}
			
			return wsResultset;
		}
	}
	
	@Transactional
	public IRecordingSearchHitDTO findRecordingByProviderData(String provider, String providerId) throws BussinessException
	{
		// Cerca directament a la BDD si el proveidor es BDD
		if(CoreConstants.CONTENT_PROVIDER_DATABASE.equals(provider)){
			Song song = songDAO.findSongById(new Long(providerId));
			return new SongDTOAdapter(song);
		}
		else{
			// Cerca a BDD per la parella provider - providerId
			ProviderRegistry providerRegistry = providerRegistryDAO.findProviderRegistryByProviderData(ProviderRegistryType.song, provider, providerId);
			if(providerRegistry!=null){
				Long songId = providerRegistry.getEntityId();
				Song song = songDAO.findSongById(songId);
				return new SongDTOAdapter(song);
			}
			// Si no s'ha trobat a base de dades es cerca al WS i s'inclouen els registres a la BDD
			else{
				IRecordingSearchHitDTO recording = musicSearchService.findRecordingByProviderId(providerId);
				// S'inclouen els resultats obtinguts a la BDD per posteriors cerques
				ProviderRegistry providerAlbumRegistry = providerRegistryDAO.findProviderRegistryByProviderData(ProviderRegistryType.album, provider, recording.getAlbumSearchId());
				if(providerAlbumRegistry!=null){
					Album album = albumDAO.findAlbumById(providerAlbumRegistry.getEntityId());
					List<IRecordingSearchHitDTO> songs = new ArrayList<IRecordingSearchHitDTO>();
					songs.add(recording);
					contentAdministrationService.updateAlbumContent(album, songs, LinkedEntityStatus.incomplete);
				}
				else{
					contentAdministrationService.createOrUpdateSong(null, recording);
				}
				
				return recording;
			}
		}
	}
	
	/* LINKS */
	public GenericResultSetDTO<ILinkDTO> findLinks(String term) throws BussinessException
	{
		// Es fa un bypass cap al servei de cerca de links ja que no s'ha de consultar / guardar cap contingut local.
		return linkSearchService.findLinks(term);
	}
	
	@Transactional
	public GenericResultSetDTO<ILinkDTO> findSongLinksBySongProviderData(String songProvider, String songProviderId) throws BussinessException
	{
		Song song = null;
		if(CoreConstants.CONTENT_PROVIDER_DATABASE.equals(songProvider)){
			song = songDAO.findSongById(Long.valueOf(songProviderId));
		}
		else{
			ProviderRegistry registry = providerRegistryDAO.findProviderRegistryByProviderData(ProviderRegistryType.song, songProvider, songProviderId);
			if(registry!=null){
				song = songDAO.findSongById(registry.getEntityId());
			}
		}
		
		List<ILinkDTO> resultList = new ArrayList<ILinkDTO>();
		if(song==null){
			throw new IllegalArgumentException("El registre amb provider: " + songProvider + "; providerId: " + songProviderId + " no es troba a la base de dades");
		}
		else{
			List<EntityRelationship> relationships = entityRelationshipDAO.findRelationshipsByEntity1Type(EntityRelationshipType.songLinks, song.getId());
			for(EntityRelationship relationship:relationships){
				Link link = linkDAO.findLinkById(relationship.getEntity2()); 
				resultList.add(new LinkDTOAdapter(link));
			}
			
			if(!resultList.isEmpty()){
				return new GenericResultSetDTO<ILinkDTO>(resultList, resultList.size(), 0);
			}
			/* Si no s'ha trobat res a base de dades es busca a través del servei de cerca de links 
			 * i s'associen per la pròxima cerca
			 */
			List<ILinkDTO> linkList = linkSearchService.findSongLinks(song.getAlbum().getArtist().getName(), song.getName()).getResultList();
			Iterator<ILinkDTO> iterator = linkList.iterator();
			while(iterator.hasNext()){
				ILinkDTO elem = iterator.next();
				if(isValidLink(elem.getTitle(), song.getAlbum().getArtist().getName(), song.getName())){
					Link link = contentAdministrationService.createOrUpdateLink(elem);
					// En aquest cas no és possible que hi hagi cap l'associació entre la cançó i el link
					contentAdministrationService.associateLinkToSong(song, link);
				}
				else{
					iterator.remove();
				}
			}
			
			return new GenericResultSetDTO<ILinkDTO>(linkList, linkList.size(), 0);
		}
	}
	
	private boolean isValidLink(String title, String artist, String song)
	{
		if(log.isInfoEnabled()){
			log.info("[ContentSearchServiceImpl::isValidLink] Validant link title: " + title + " / artist: " + artist + " / song: " + song);
		}
		
		StrMatcher delimMatcher = StrMatcher.charSetMatcher(" \n\t-_.,:;\\/¿?!|<>");
		StrTokenizer titleTokenizer = new StrTokenizer();
		titleTokenizer.setDelimiterMatcher(delimMatcher);
		titleTokenizer.setIgnoreEmptyTokens(true);
		titleTokenizer.reset(title);
		@SuppressWarnings("unchecked")
		List<String> titleTokens = titleTokenizer.getTokenList();
		
		StrTokenizer whiteSpaceTokenizer = new StrTokenizer();
		@SuppressWarnings("unchecked")
		List<String> artistTokens = whiteSpaceTokenizer.reset(artist).getTokenList();
		int artistScore = computeLinkScore(titleTokens, artistTokens);
		if(artistScore>artistTokens.size()/2){
			if(log.isInfoEnabled()){
				log.info("[ContentSearchServiceImpl::isValidLink] Artista vàlid; Score: " + artistScore + " / total: " + artistTokens.size());
			}
			@SuppressWarnings("unchecked")
			List<String> songTokens = whiteSpaceTokenizer.reset(song).getTokenList();
			int songScore = computeLinkScore(titleTokens, songTokens);
			if(songScore>songTokens.size()/2){
				if(log.isInfoEnabled()){
					log.info("[ContentSearchServiceImpl::isValidLink] Cançó vàlida; Score: " + songScore + " / total: " + songTokens.size());
				}
				return true;
			}
		}
		log.info("[ContentSearchServiceImpl::isValidLink] Link invàlid!!");
		
		return false;
	}
	
	private int computeLinkScore(List<String> inputChunks, List<String> referenceChunks)
	{
		int score = 0;
		for(String keyword:referenceChunks){
			boolean matches = false;
			Iterator<String> iterator = inputChunks.iterator();
			while(!matches && iterator.hasNext()){
				String token = iterator.next();
				int distance = StringUtils.getLevenshteinDistance(keyword.toLowerCase(), token.toLowerCase());
				if(distance<keyword.length()/2){
					score += 1;
					iterator.remove();
					matches = true;
				}
			}
		}
		
		return score;
	}
}
