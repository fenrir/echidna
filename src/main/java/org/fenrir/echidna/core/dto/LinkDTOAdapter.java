package org.fenrir.echidna.core.dto;

import org.fenrir.echidna.common.dto.ILinkDTO;
import org.fenrir.echidna.core.CoreConstants;
import org.fenrir.echidna.core.entity.Link;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120911
 */
public class LinkDTOAdapter implements ILinkDTO 
{

	private Link link;
	
	public LinkDTOAdapter(Link link)
	{
		this.link = link;
	}
	
	public String getId()
	{
		return link.getId().toString();
	}
	
	public String getProvider()
	{
		return CoreConstants.CONTENT_PROVIDER_DATABASE;
	}

	public String getLinkSearchId()
	{
		return link.getLinkSearchId();
	}

	public String getUrl()
	{
		return link.getUrl();
	}

	public String getTitle()
	{
		return link.getTitle();
	}

	public String getDescription() 
	{
		return link.getDescription();
	}

	public String getAuthor() 
	{
		return link.getAuthor();
	}

	public String getThumbnailUrl() 
	{
		return link.getThumbnail();
	}
}
