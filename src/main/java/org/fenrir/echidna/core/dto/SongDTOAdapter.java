package org.fenrir.echidna.core.dto;

import org.apache.commons.lang.StringUtils;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.core.CoreConstants;
import org.fenrir.echidna.core.entity.Song;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120703
 */
public class SongDTOAdapter implements IRecordingSearchHitDTO 
{
	private Song song;
	
	public SongDTOAdapter(Song song)
	{
		this.song = song;
	}

	public String getProvider()
	{
		return CoreConstants.CONTENT_PROVIDER_DATABASE;
	}
	
	public String getId() 
	{
		return song.getId().toString();				
	}

	public String getTitle() 
	{
		return song.getName();
	}

	public String getLength() 
	{
		if(song.getLength()!=null){
			try{
				// La llargada de la pista es troba en ms
				long length = song.getLength() / 1000;
				
				return StringUtils.leftPad(Long.toString(length / 60), 2, "0") 
						+ ":" + StringUtils.leftPad(Long.toString(length % 60), 2, "0");
			}
			catch(NumberFormatException e){
				return "--:--";
			}
		}
		
		return "--:--";
	}
	
	public Long getLengthMillis()
	{
		return song.getLength();
	}

	public String getArtistSearchId() 
	{
		if(song.getAlbum()!=null && song.getAlbum().getArtist()!=null){
			return song.getAlbum().getArtist().getId().toString();
		}
		
		return null;
	}

	public String getArtistName() 
	{
		if(song.getAlbum()!=null && song.getAlbum().getArtist()!=null){
			return song.getAlbum().getArtist().getName();
		}
		
		return null;
	}

	public String getAlbumSearchId() 
	{
		if(song.getAlbum()!=null){
			return song.getAlbum().getId().toString();
		}
		
		return null;
	}

	public String getAlbumName() 
	{
		if(song.getAlbum()!=null){
			return song.getAlbum().getName();
		}
		
		return null;
	}
	
	public Integer getOrder()
	{
		return song.getOrder();
	}
}
