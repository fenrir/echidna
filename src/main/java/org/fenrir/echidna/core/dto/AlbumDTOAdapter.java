package org.fenrir.echidna.core.dto;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.fenrir.echidna.common.dto.IAlbumSearchHitDTO;
import org.fenrir.echidna.core.CoreConstants;
import org.fenrir.echidna.core.entity.Album;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120708
 */
public class AlbumDTOAdapter implements IAlbumSearchHitDTO 
{
	private Album album;
	
	public AlbumDTOAdapter(Album album)
	{
		this.album = album;
	}
	
	public String getProvider() 
	{
		return CoreConstants.CONTENT_PROVIDER_DATABASE;
	}

	public String getId() 
	{
		return album.getId().toString();
	}

	public String getName() 
	{
		return album.getName();
	}

	public String getDescription() 
	{
		return album.getName();
	}

	public String getFirstReleaseDate() 
	{
		return album.getFirstReleaseDate();
	}

	public String getFirstReleaseYear() 
	{
		String releaseDate = getFirstReleaseDate();
		Pattern pattern = Pattern.compile("(\\d{4})");
		Matcher matcher = pattern.matcher(releaseDate);
		if(matcher.find()){
			releaseDate = matcher.group(0);
		}
		
		return releaseDate;
	}

	public String getArtistSearchId() 
	{
		if(album.getArtist()!=null){
			return album.getArtist().getId().toString();
		}
		
		return null;
	}

	public String getArtistName() 
	{
		if(album.getArtist()!=null){
			return album.getArtist().getName();
		}
		
		return null;
	}
}
