package org.fenrir.echidna.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.echidna.core.dao.IAlbumDAO;
import org.fenrir.echidna.core.entity.Album;
import org.fenrir.echidna.core.entity.Artist;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120705
 */
public class AlbumDAOImpl implements IAlbumDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
	 */
	@Inject 
	private Provider<EntityManager> entityManagerProvider;
	
	public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
	{
		this.entityManagerProvider = entityManagerProvider;
	}
	
	@Transactional
	public List<Album> findAlbumsByArtist(Artist artist)
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from Album where artist=:artist", Album.class)
				.setParameter("artist", artist)
				.getResultList();
	}
	
	@Transactional
	public Album findAlbumById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
		return em.find(Album.class, id);
	}

	@Transactional
	public Album createAlbum(Album album) 
	{
		EntityManager em = entityManagerProvider.get();
		em.persist(album);
		
		return album;
	}

	@Transactional
	public Album updateAlbum(Album album) 
	{
		/* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
		 * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
		 */
		Album managedAlbum = findAlbumById(album.getId());
		managedAlbum.setName(album.getName());
		managedAlbum.setArtist(album.getArtist());
		managedAlbum.setCreationDate(album.getCreationDate());
		
		return managedAlbum;
	}

	@Transactional
	public void deleteAlbum(Album album) 
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(album);
	}
}
