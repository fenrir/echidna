package org.fenrir.echidna.core.dao;

import java.util.List;
import org.fenrir.echidna.core.entity.Album;
import org.fenrir.echidna.core.entity.Song;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120702
 */
public interface ISongDAO 
{
	public List<Song> findSongsByAlbum(Album album);
	public Song findSongById(Long id);
	
	public Song createSong(Song song);
	public Song updateSong(Song song);
	public void deleteSong(Song song);
}
