package org.fenrir.echidna.core.dao;

import java.util.List;
import org.fenrir.echidna.core.entity.User;
import org.fenrir.echidna.core.entity.UserBookmark;
import org.fenrir.echidna.core.entity.BookmarkType;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120721
 */
public interface IUserBookmarkDAO 
{
	public List<UserBookmark> findBookmarksByUser(User user);
	public List<UserBookmark> findBookmarksByUser(BookmarkType type, User user);
	public UserBookmark findBookmarkById(Long id);
	
	public UserBookmark createBookmark(UserBookmark bookmark);
	public UserBookmark updateBookmark(UserBookmark bookmark);
	public void deleteBookmark(UserBookmark bookmark);
}
