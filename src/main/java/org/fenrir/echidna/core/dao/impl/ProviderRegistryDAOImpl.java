package org.fenrir.echidna.core.dao.impl;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.echidna.core.dao.IProviderRegistryDAO;
import org.fenrir.echidna.core.entity.ProviderRegistry;
import org.fenrir.echidna.core.entity.ProviderRegistryType;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120817
 */
public class ProviderRegistryDAOImpl implements IProviderRegistryDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
	 */
	@Inject 
	private Provider<EntityManager> entityManagerProvider;
	
	public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
	{
		this.entityManagerProvider = entityManagerProvider;
	}
	
	@Transactional
	public List<ProviderRegistry> findProviderRegistriesByEntity(Long entityId)
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from ProviderRegistry where entityId=:entityId", ProviderRegistry.class)
				.setParameter("entityId", entityId)
				.getResultList();
	}
	
	@Transactional
	public List<ProviderRegistry> findProviderRegistriesByProviderTypeEntity(ProviderRegistryType type, Long entityId)
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from ProviderRegistry where type=:type and entityId=:entityId", ProviderRegistry.class)
				.setParameter("type", type)
				.setParameter("entityId", entityId)
				.getResultList();
	}
	
	@Transactional
	public ProviderRegistry findProviderRegistryById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
		return em.find(ProviderRegistry.class, id);
	}
	
	@Transactional
	public ProviderRegistry findProviderRegistryByProviderData(ProviderRegistryType type, String provider, String providerId)
	{
		EntityManager em = entityManagerProvider.get();
		List<ProviderRegistry> resultList = em.createQuery("from ProviderRegistry where type=:type and provider=:provider and providerId=:providerId", ProviderRegistry.class)
				.setParameter("type", type)
				.setParameter("provider", provider)
				.setParameter("providerId", providerId)
				.getResultList();
		
		/* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
		 * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
		 */
		if(resultList.isEmpty()){
			return null;
		}
		else{
			return resultList.get(0);
		}
	}

	@Transactional
	public ProviderRegistry createProviderRegistry(ProviderRegistry registry) 
	{
		EntityManager em = entityManagerProvider.get();
		em.persist(registry);
		
		return registry;
	}

	@Transactional
	public ProviderRegistry updateProviderRegistry(ProviderRegistry registry) 
	{
		/* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
		 * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
		 */
		ProviderRegistry managedRegistry = findProviderRegistryById(registry.getId());
		managedRegistry.setProvider(registry.getProvider());
		managedRegistry.setProviderId(registry.getProviderId());
		managedRegistry.setType(registry.getType());
		
		return managedRegistry;
	}

	@Transactional
	public void deleteProviderRegistry(ProviderRegistry registry) 
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(registry);
	}
}
