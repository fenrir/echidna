package org.fenrir.echidna.core.dao.impl;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.echidna.core.dao.ILinkDAO;
import org.fenrir.echidna.core.entity.Link;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120911
 */
public class LinkDAOImpl implements ILinkDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
	 */
	@Inject 
	private Provider<EntityManager> entityManagerProvider;
	
	public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
	{
		this.entityManagerProvider = entityManagerProvider;
	}
	
	@Transactional
	public Link findLinkById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
		return em.find(Link.class, id);
	}
	
	@Transactional
	public Link createLink(Link link) 
	{
		EntityManager em = entityManagerProvider.get();
		em.persist(link);
		
		return link;
	}

	@Transactional
	public Link updateLink(Link link) 
	{
		/* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
		 * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
		 */
		Link managedLink = findLinkById(link.getId());
		managedLink.setUrl(link.getUrl());
		managedLink.setLinkSearchId(link.getLinkSearchId());
		managedLink.setTitle(link.getTitle());
		managedLink.setDescription(link.getDescription());
		managedLink.setAuthor(link.getAuthor());
		managedLink.setThumbnail(link.getThumbnail());
		
		return managedLink;
	}

	@Transactional
	public void deleteLink(Link link) 
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(link);
	}
}
