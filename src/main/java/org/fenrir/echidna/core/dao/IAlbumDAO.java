package org.fenrir.echidna.core.dao;

import java.util.List;
import org.fenrir.echidna.core.entity.Album;
import org.fenrir.echidna.core.entity.Artist;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120702
 */
public interface IAlbumDAO 
{
	public List<Album> findAlbumsByArtist(Artist artist);
	public Album findAlbumById(Long id);
	
	public Album createAlbum(Album album);
	public Album updateAlbum(Album album);
	public void deleteAlbum(Album album);
}
