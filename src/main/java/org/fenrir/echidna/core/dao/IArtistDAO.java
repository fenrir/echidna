package org.fenrir.echidna.core.dao;

import org.fenrir.echidna.core.entity.Artist;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120529
 */
public interface IArtistDAO 
{
	public Artist findArtistById(Long id);
	
	public Artist createArtist(Artist artist);
	public Artist updateArtist(Artist artist);
	public void deleteArtist(Artist artist);
}
