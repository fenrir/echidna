package org.fenrir.echidna.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.echidna.core.dao.IUserDAO;
import org.fenrir.echidna.core.entity.User;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120602
 */
public class UserDAOImpl implements IUserDAO
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
	 */
	@Inject 
	private Provider<EntityManager> entityManagerProvider;
	
	public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
	{
		this.entityManagerProvider = entityManagerProvider;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> findAllUsers()
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from User").getResultList();
	}
	
	@Transactional
	public User findUserById(Long id)
	{
		EntityManager em = entityManagerProvider.get();
		return em.find(User.class, id);
	}
	
	@Transactional
	public User findUserByName(String name)
	{
		EntityManager em = entityManagerProvider.get();
		List<User> resultList = em.createQuery("from User where name=:name", User.class)
				.setParameter("name", name)
				.getResultList();
		/* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
		 * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
		 */
		if(resultList.isEmpty()){
			return null;
		}
		else{
			return resultList.get(0);
		}
	}
	
	@Transactional
	public User createUser(User user)
	{
		EntityManager em = entityManagerProvider.get();
		em.persist(user);
		
		return user;
	}
	
	@Transactional
	public User updateUser(User user)
	{
		/* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
		 * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
		 */
		User managedUser = findUserById(user.getId());
		managedUser.setName(user.getName());
		managedUser.setPassword(user.getPassword());
		managedUser.setCreationDate(user.getCreationDate());

		return managedUser;
	}
	
	@Transactional
	public void deleteUser(User user)
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(user);
	}
}
