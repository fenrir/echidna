package org.fenrir.echidna.core.dao;

import org.fenrir.echidna.core.entity.UserProviderRegistry;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120616
 */
public interface IUserProviderRegistryDAO 
{
	public UserProviderRegistry findUserProviderRegistryById(Long id);
	
	public UserProviderRegistry createUserProviderRegistry(UserProviderRegistry registry);
	public UserProviderRegistry updateUserProviderRegistry(UserProviderRegistry registry);
	public void deleteUserProviderRegistry(UserProviderRegistry registry);
}
