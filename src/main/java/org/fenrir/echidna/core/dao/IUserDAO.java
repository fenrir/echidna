package org.fenrir.echidna.core.dao;

import java.util.List;
import org.fenrir.echidna.core.entity.User;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120518
 */
public interface IUserDAO 
{
	public List<User> findAllUsers();
	public User findUserById(Long id);
	public User findUserByName(String name);
	
	public User createUser(User user);
	public User updateUser(User user);
	public void deleteUser(User user);
}
