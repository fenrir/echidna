package org.fenrir.echidna.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.echidna.core.dao.IUserBookmarkDAO;
import org.fenrir.echidna.core.entity.BookmarkType;
import org.fenrir.echidna.core.entity.User;
import org.fenrir.echidna.core.entity.UserBookmark;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120909
 */
public class UserBookmarkDAOImpl implements IUserBookmarkDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
	 */
	@Inject 
	private Provider<EntityManager> entityManagerProvider;
	
	public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
	{
		this.entityManagerProvider = entityManagerProvider;
	}
	
	public List<UserBookmark> findBookmarksByUser(User user) 
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from UserBoookmark where user=:user", UserBookmark.class)
				.setParameter("user", user)
				.getResultList();
	}

	public List<UserBookmark> findBookmarksByUser(BookmarkType type, User user) 
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from UserBoookmark where user=:user and type=:type", UserBookmark.class)
				.setParameter("user", user)
				.setParameter("type", type)
				.getResultList();
	}

	public UserBookmark findBookmarkById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
		return em.find(UserBookmark.class, id);
	}

	@Transactional
	public UserBookmark createBookmark(UserBookmark bookmark) 
	{
		EntityManager em = entityManagerProvider.get();
		em.persist(bookmark);
		
		return bookmark;
	}

	public UserBookmark updateBookmark(UserBookmark bookmark) 
	{
		/* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
		 * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
		 */
		UserBookmark managedBookmark = findBookmarkById(bookmark.getId());
		managedBookmark.setUser(bookmark.getUser());
		managedBookmark.setType(bookmark.getType());
		managedBookmark.setEntityId(bookmark.getEntityId());
		managedBookmark.setCreationDate(bookmark.getCreationDate());
		
		return managedBookmark;
	}

	@Transactional
	public void deleteBookmark(UserBookmark bookmark) 
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(bookmark);
	}
}
