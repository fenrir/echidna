package org.fenrir.echidna.core.dao;

import java.util.List;
import org.fenrir.echidna.core.entity.ProviderRegistry;
import org.fenrir.echidna.core.entity.ProviderRegistryType;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120817
 */
public interface IProviderRegistryDAO 
{
	public List<ProviderRegistry> findProviderRegistriesByEntity(Long entityId);
	public List<ProviderRegistry> findProviderRegistriesByProviderTypeEntity(ProviderRegistryType type, Long entityId);
	public ProviderRegistry findProviderRegistryById(Long id);
	public ProviderRegistry findProviderRegistryByProviderData(ProviderRegistryType type, String provider, String providerId);
	
	public ProviderRegistry createProviderRegistry(ProviderRegistry registry);
	public ProviderRegistry updateProviderRegistry(ProviderRegistry registry);
	public void deleteProviderRegistry(ProviderRegistry registry);
}
