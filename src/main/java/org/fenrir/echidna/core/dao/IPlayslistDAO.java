package org.fenrir.echidna.core.dao;

import java.util.List;
import org.fenrir.echidna.core.entity.Playlist;
import org.fenrir.echidna.core.entity.User;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120518
 */
public interface IPlayslistDAO 
{
	public List<Playlist> findPlaylistsByUser(User user);
	
	public Playlist findPlaylistById(Long id);
	
	public Playlist createPlaylist(Playlist playlist);
	public Playlist updatePlaylist(Playlist playlist);
	public void deletePlaylist(Playlist playlist);
}
