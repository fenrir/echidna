package org.fenrir.echidna.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.echidna.core.dao.ISongDAO;
import org.fenrir.echidna.core.entity.Album;
import org.fenrir.echidna.core.entity.Song;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120702
 */
public class SongDAOImpl implements ISongDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
	 */
	@Inject 
	private Provider<EntityManager> entityManagerProvider;
	
	public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
	{
		this.entityManagerProvider = entityManagerProvider;
	}

	@Transactional
	public List<Song> findSongsByAlbum(Album album)
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from Song where album=:album", Song.class)
				.setParameter("album", album)
				.getResultList();
	}
	
	@Transactional
	public Song findSongById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
		return em.find(Song.class, id);
	}
	
	@Transactional
	public Song createSong(Song song) 
	{
		EntityManager em = entityManagerProvider.get();
		em.persist(song);
		
		return song;
	}

	@Transactional
	public Song updateSong(Song song) 
	{
		/* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
		 * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
		 */
		Song managedSong = findSongById(song.getId());
		managedSong.setName(song.getName());
		managedSong.setAlbum(song.getAlbum());
		managedSong.setCreationDate(song.getCreationDate());
		
		return managedSong;
	}

	@Transactional
	public void deleteSong(Song song) 
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(song);
	}
}
