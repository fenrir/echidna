package org.fenrir.echidna.core.dao.impl;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.echidna.core.dao.IPlaylistSongDAO;
import org.fenrir.echidna.core.entity.PlaylistSong;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120529
 */
public class PlaylistSongDAOImpl implements IPlaylistSongDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
	 */
	@Inject 
	private Provider<EntityManager> entityManagerProvider;
	
	public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
	{
		this.entityManagerProvider = entityManagerProvider;
	}
	
	@Transactional
	public PlaylistSong findPlaylistSongById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
		return em.find(PlaylistSong.class, id);
	}

	@Transactional
	public PlaylistSong createPlaylistSong(PlaylistSong song) 
	{
		EntityManager em = entityManagerProvider.get();
		em.persist(song);
		
		return song;
	}

	@Transactional
	public PlaylistSong updatePlaylistSong(PlaylistSong song) 
	{
		/* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
		 * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
		 */
		PlaylistSong managedSong = findPlaylistSongById(song.getId());
		managedSong.setPlaylist(song.getPlaylist());
		managedSong.setSong(song.getSong());
		
		return managedSong;
	}

	@Transactional
	public void deletePlaylistSong(PlaylistSong song) 
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(song);
	}
}
