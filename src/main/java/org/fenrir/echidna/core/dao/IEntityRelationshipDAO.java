package org.fenrir.echidna.core.dao;

import java.util.List;
import org.fenrir.echidna.core.entity.EntityRelationship;
import org.fenrir.echidna.core.entity.EntityRelationshipType;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120826
 */
public interface IEntityRelationshipDAO 
{
	public List<EntityRelationship> findRelationshipsByEntity1(Long entityId);
	public List<EntityRelationship> findRelationshipsByEntity2(Long entityId);
	public List<EntityRelationship> findRelationshipsByEntity1Type(EntityRelationshipType type, Long entityId);
	public List<EntityRelationship> findRelationshipsByEntity2Type(EntityRelationshipType type, Long entityId);
	public EntityRelationship findRelationshipById(Long id);
	public EntityRelationship findRelationshipByEntitiesType(EntityRelationshipType type, Long entity1Id, Long entity2Id);
	
	public EntityRelationship createRelationship(EntityRelationship relationship);
	public EntityRelationship updateRelationship(EntityRelationship relationship);
	public void deleteRelationship(EntityRelationship relationship);
}
