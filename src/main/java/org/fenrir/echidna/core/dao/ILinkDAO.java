package org.fenrir.echidna.core.dao;

import org.fenrir.echidna.core.entity.Link;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120817
 */
public interface ILinkDAO 
{
	public Link findLinkById(Long id);
	
	public Link createLink(Link link);
	public Link updateLink(Link link);
	public void deleteLink(Link link);
}
