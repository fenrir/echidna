package org.fenrir.echidna.core.dao;

import org.fenrir.echidna.core.entity.PlaylistSong;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120529
 */
public interface IPlaylistSongDAO 
{
	public PlaylistSong findPlaylistSongById(Long id);
	
	public PlaylistSong createPlaylistSong(PlaylistSong song);
	public PlaylistSong updatePlaylistSong(PlaylistSong song);
	public void deletePlaylistSong(PlaylistSong song);
}
