package org.fenrir.echidna.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.echidna.core.dao.IEntityRelationshipDAO;
import org.fenrir.echidna.core.entity.EntityRelationship;
import org.fenrir.echidna.core.entity.EntityRelationshipType;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120912
 */
public class EntityRelationshipDAOImpl implements IEntityRelationshipDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
	 */
	@Inject 
	private Provider<EntityManager> entityManagerProvider;
	
	public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
	{
		this.entityManagerProvider = entityManagerProvider;
	}

	@Transactional
	public List<EntityRelationship> findRelationshipsByEntity1(Long entityId)
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from EntityRelationship where entity1=:entity1 order by score desc", EntityRelationship.class)
				.setParameter("entity1", entityId)
				.getResultList();
	}

	@Transactional
	public List<EntityRelationship> findRelationshipsByEntity2(Long entityId)
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from EntityRelationship where entity2=:entity2 order by score desc", EntityRelationship.class)
				.setParameter("entity2", entityId)
				.getResultList();
	}

	@Transactional
	public List<EntityRelationship> findRelationshipsByEntity1Type(EntityRelationshipType type, Long entityId) 
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from EntityRelationship where type=:type and entity1=:entity1 order by score desc", EntityRelationship.class)
				.setParameter("type", type)
				.setParameter("entity1", entityId)
				.getResultList();
	}

	@Transactional
	public List<EntityRelationship> findRelationshipsByEntity2Type(EntityRelationshipType type, Long entityId) 
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from EntityRelationship where type=:type and entity2=:entity2 order by score desc", EntityRelationship.class)
				.setParameter("type", type)
				.setParameter("entity2", entityId)
				.getResultList();
	}

	@Transactional
	public EntityRelationship findRelationshipById(Long id)
	{
		EntityManager em = entityManagerProvider.get();
		return em.find(EntityRelationship.class, id);
	}
	
	@Transactional
	public EntityRelationship findRelationshipByEntitiesType(EntityRelationshipType type, Long entity1Id, Long entity2Id) 
	{
		EntityManager em = entityManagerProvider.get();
		List<EntityRelationship> resultList = em.createQuery("from EntityRelationship where type=:type and entity1=:entity1 and entity2=:entity2", EntityRelationship.class)
				.setParameter("type", type)
				.setParameter("entity1", entity1Id)
				.setParameter("entity2", entity2Id)
				.getResultList();
		
		/* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
		 * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
		 */
		if(resultList.isEmpty()){
			return null;
		}
		else{
			return resultList.get(0);
		}
	}

	@Transactional
	public EntityRelationship createRelationship(EntityRelationship relationship)
	{
		EntityManager em = entityManagerProvider.get();
		em.persist(relationship);
		
		return relationship;
	}

	@Transactional
	public EntityRelationship updateRelationship(EntityRelationship relationship) 
	{
		/* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
		 * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
		 */
		EntityRelationship managedRelationship = findRelationshipById(relationship.getId());
		managedRelationship.setEntity1(relationship.getEntity1());
		managedRelationship.setEntity2(relationship.getEntity2());
		managedRelationship.setType(relationship.getType());
		managedRelationship.setScore(relationship.getScore());
		
		return managedRelationship;
	}

	@Transactional
	public void deleteRelationship(EntityRelationship relationship) 
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(relationship);
	}
}
