package org.fenrir.echidna.core.dao.impl;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.echidna.core.dao.IUserProviderRegistryDAO;
import org.fenrir.echidna.core.entity.UserProviderRegistry;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120616
 */
public class UserProviderRegistryDAOImpl implements IUserProviderRegistryDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
	 */
	@Inject 
	private Provider<EntityManager> entityManagerProvider;
	
	public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
	{
		this.entityManagerProvider = entityManagerProvider;
	}
	
	@Transactional
	public UserProviderRegistry findUserProviderRegistryById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
		return em.find(UserProviderRegistry.class, id);
	}

	@Transactional
	public UserProviderRegistry createUserProviderRegistry(UserProviderRegistry registry) 
	{
		EntityManager em = entityManagerProvider.get();
		em.persist(registry);
		
		return registry;
	}

	@Transactional
	public UserProviderRegistry updateUserProviderRegistry(UserProviderRegistry registry) 
	{
		/* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
		 * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
		 */
		UserProviderRegistry managedRegistry = findUserProviderRegistryById(registry.getId());
		managedRegistry.setUser(registry.getUser());
		managedRegistry.setRegistry(registry.getRegistry());
		
		return managedRegistry;
	}

	@Transactional
	public void deleteUserProviderRegistry(UserProviderRegistry registry) 
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(registry);
	}
}
