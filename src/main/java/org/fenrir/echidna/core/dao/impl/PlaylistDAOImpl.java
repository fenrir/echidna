package org.fenrir.echidna.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.echidna.core.dao.IPlayslistDAO;
import org.fenrir.echidna.core.entity.Playlist;
import org.fenrir.echidna.core.entity.User;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120519
 */
public class PlaylistDAOImpl implements IPlayslistDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
	 */
	@Inject 
	private Provider<EntityManager> entityManagerProvider;
	
	public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
	{
		this.entityManagerProvider = entityManagerProvider;
	}

	@Transactional
	public List<Playlist> findPlaylistsByUser(User user)
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from Playlist where user=:user", Playlist.class)
				.setParameter("user", user)
				.getResultList();
	}
	
	@Transactional
	public Playlist findPlaylistById(Long id)
	{
		EntityManager em = entityManagerProvider.get();
		return em.find(Playlist.class, id);
	}
	
	@Transactional
	public Playlist createPlaylist(Playlist playlist)
	{
		EntityManager em = entityManagerProvider.get();
		em.persist(playlist);
		
		return playlist;
	}
	
	@Transactional
	public Playlist updatePlaylist(Playlist playlist)
	{
		/* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
		 * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
		 */
		Playlist managedPlaylist = findPlaylistById(playlist.getId());
		managedPlaylist.setName(playlist.getName());
		managedPlaylist.setUser(playlist.getUser());
		managedPlaylist.setCreationDate(playlist.getCreationDate());
		
		return managedPlaylist;
	}
	
	@Transactional
	public void deletePlaylist(Playlist playlist)
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(playlist);
	}
}
