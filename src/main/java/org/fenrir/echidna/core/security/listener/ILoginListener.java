package org.fenrir.echidna.core.security.listener;

import org.fenrir.echidna.core.entity.User;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120408
 */
public interface ILoginListener 
{
	public void onLogin(User user);
	public void onLogout(User user);
}
