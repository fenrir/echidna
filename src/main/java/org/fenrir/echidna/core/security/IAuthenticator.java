package org.fenrir.echidna.core.security;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20130331
 */
public interface IAuthenticator 
{
	public boolean authenticate(String username, String password);
}
