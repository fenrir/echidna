package org.fenrir.echidna.core.security.impl;

import javax.inject.Inject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.core.dao.IUserDAO;
import org.fenrir.echidna.core.entity.User;
import org.fenrir.echidna.core.security.IAuthenticator;
import org.fenrir.echidna.core.security.KeyStoreUtils;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20130331
 */
public class SimpleAuthenticatorImpl implements IAuthenticator
{
	private final Log log = LogFactory.getLog(SimpleAuthenticatorImpl.class);
	
	@Inject
	private IUserDAO userDAO;
	
	public void setUserDAO(IUserDAO userDAO)
	{
		this.userDAO = userDAO;
	}
	
	public boolean authenticate(String username, String password)
	{
		try{
			User user = userDAO.findUserByName(username);
			String decryptedPassword = KeyStoreUtils.decrypt(user.getPassword());
			if(password!=null && password.equals(decryptedPassword)){
				return true;
			}
		}
		catch(Exception e){
			log.error("[SimpleAuthenticatorImpl::authenticate] Error autenticant usuari " + username + ": " + e.getMessage(), e);
		}
		
		return false;
	}
}
