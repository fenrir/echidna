package org.fenrir.echidna.core.security.exception;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120331
 */
@SuppressWarnings("serial")
public class LoginFailureException extends Exception 
{
	public LoginFailureException() 
	{
		super();	
	}

	public LoginFailureException(String message, Throwable cause) 
	{
		super(message, cause);
	}

	public LoginFailureException(String message) 
	{
		super(message);	
	}

	public LoginFailureException(Throwable cause) 
	{
		super(cause);	
	}	
}
