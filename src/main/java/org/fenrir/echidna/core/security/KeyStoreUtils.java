package org.fenrir.echidna.core.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyStore;
import java.security.KeyStore.SecretKeyEntry;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
// TODO Canviar per apache.commons.codec
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20130401
 */
public class KeyStoreUtils 
{
	// TODO Això s'hauria de posar en un altre lloc
	private static final String KEYSTORE_PASSWORD = "changeit";
	private static final String KEYSTORE_FILE = "keystore";
	private static final String ALIAS = "cypher";

	public static boolean existsKeyStoreFile()
	{
		return new File(KEYSTORE_FILE).exists();
	}
	
	public static void createKeyStore() throws Exception
	{
		try{
			// Les claus simètriques s'ha d'emmagatzemar en una keyStore de tipus JCEKS en comptes d'una JKS
			KeyStore keyStore = KeyStore.getInstance("JCEKS");
			// Encara que no existeixi s'ha de carregar indicant null com a stream d'entrada
			keyStore.load(null, KEYSTORE_PASSWORD.toCharArray());
			
			// Generació i emmazemament de la clau simètrica dins la keyStore
			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
			keyGenerator.init(new SecureRandom());
			SecretKey key = keyGenerator.generateKey();
			keyStore.setKeyEntry(ALIAS, key, KEYSTORE_PASSWORD.toCharArray(), null);
			FileOutputStream fos = new FileOutputStream(KEYSTORE_FILE);
			keyStore.store(fos, KEYSTORE_PASSWORD.toCharArray());
		}
		catch(Exception e){
			// TODO Log
			throw e;
		}	
	}
	
	public static SecretKey getSecretKey() throws Exception
	{
		// Les claus simètriques s'ha d'emmagatzemar en una keyStore de tipus JCEKS en comptes d'una JKS
		KeyStore keyStore = KeyStore.getInstance("JCEKS");
		FileInputStream fis = new FileInputStream(KEYSTORE_FILE);
		keyStore.load(fis, KEYSTORE_PASSWORD.toCharArray());
		
		KeyStore.PasswordProtection password = new KeyStore.PasswordProtection(KEYSTORE_PASSWORD.toCharArray());
		SecretKeyEntry entry = (SecretKeyEntry)keyStore.getEntry(ALIAS, password);
		
		return entry.getSecretKey();
	}
	
	public static String encrypt(String message) throws Exception 
	{
		SecretKey key = getSecretKey();
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		// Encriptació
		byte[] stringBytes = message.getBytes("UTF8");
		byte[] raw = cipher.doFinal(stringBytes);
		// Conversió a base64 per representar el resultat
		BASE64Encoder encoder = new BASE64Encoder();
		String base64 = encoder.encode(raw);
		
		return base64;
	}
	
	public static String decrypt(String encrypted) throws Exception
	{
		SecretKey key = getSecretKey();
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key);
		// Desencriptació
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] raw = decoder.decodeBuffer(encrypted);
		byte[] stringBytes = cipher.doFinal(raw);
		// Conversió a base64 per representar el resultat
		String clear = new String(stringBytes, "UTF8");
		
		return clear;
	}
}
