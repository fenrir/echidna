package org.fenrir.echidna.core.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120630
 */
@Entity
public class Artist 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Version
	private Long version;
	private String name;
	// Es persisteix l'atribut com a String en comptes de com a valor numèric (EnumType.ORDINAL)
	@Enumerated(EnumType.STRING)
	private LinkedEntityStatus albumsStatus;
	@Temporal(TemporalType.DATE)
	private Date creationDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated;
	
	public Long getId() 
	{
		return id;
	}
	
	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public Long getVersion()
	{
		return version;
	}
	
	public void setVersion(Long version)
	{
		this.version = version;
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public LinkedEntityStatus getAlbumsStatus()
	{
		return albumsStatus;
	}
	
	public void setAlbumsStatus(LinkedEntityStatus albumsStatus)
	{
		this.albumsStatus = albumsStatus;
	}
	
	public Date getCreationDate()
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}
	
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	/**
	 * Callback executat abans de realitzar l'inserció / actualització del registre. 
	 * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
	 */
	@PreUpdate
	@PrePersist
	public void updateTimestamps()
	{
		lastUpdated = new Date();
		if(creationDate==null){
			creationDate = new Date();
		}
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((albumsStatus == null) ? 0 : albumsStatus.hashCode());
		result = prime * result	+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result	+ ((lastUpdated == null) ? 0 : lastUpdated.hashCode());
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(getClass()!=obj.getClass()){
			return false;
		}
		
		Artist other = (Artist) obj;
		/* Id */
		if(id==null){
			if(other.id!=null){
				return false;
			}
		} 
		else if(!id.equals(other.id)){
			return false;
		}
		/* Name */
		if(name==null){
			if(other.name != null){
				return false;
			}
		} 
		else if(!name.equals(other.name)){
			return false;
		}
		/* AlbumsStatus */
		if(albumsStatus==null){
			if(other.albumsStatus != null){
				return false;
			}
		} 
		else if(!albumsStatus.equals(other.albumsStatus)){
			return false;
		}
		/* Creation date */
		if(creationDate==null){
			if(other.creationDate!=null){
				return false;
			}
		}
		else if(!creationDate.equals(other.creationDate)){
			return false;
		}
		/* Last updated */
		if(lastUpdated==null){
			if(other.lastUpdated!=null){
				return false;
			}
		} 
		else if(!lastUpdated.equals(other.lastUpdated)){
			return false;
		}
		
		return true;
	}
}
