package org.fenrir.echidna.core.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120911
 */
@Entity
public class Link 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Version
	private Long version;
	@NotNull
	@Column(nullable=false)
	private String url;
	@NotNull
	@Column(nullable=false)
	private String linkSearchId;
	private String title;
	@Lob
	@Column(length=1000)
	private String description;
	private String author;
	private String thumbnail;
	@Temporal(TemporalType.DATE)
	private Date creationDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated;
	
	public Long getId() 
	{
		return id;
	}
	
	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public Long getVersion()
	{
		return version;
	}
	
	public void setVersion(Long version)
	{
		this.version = version;
	}
	
	public String getUrl()
	{
		return url;
	}
	
	public void setUrl(String url)
	{
		this.url = url;
	}
	
	public String getLinkSearchId()
	{
		return linkSearchId;
	}
	
	public void setLinkSearchId(String linkSearchId)
	{
		this.linkSearchId = linkSearchId;
	}
	
	public String getTitle() 
	{
		return title;
	}

	public void setTitle(String title) 
	{
		this.title = title;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}

	public String getAuthor() 
	{
		return author;
	}

	public void setAuthor(String author) 
	{
		this.author = author;
	}

	public String getThumbnail()
	{
		return thumbnail;
	}
	
	public void setThumbnail(String thumbnail)
	{
		this.thumbnail = thumbnail;
	}
	
	public Date getCreationDate()
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}
	
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	/**
	 * Callback executat abans de realitzar l'inserció / actualització del registre. 
	 * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització.
	 * També s'encarrega de limitar el camp descripció a 1000 char en el cas que la seva longitud sigui 
	 * massa gran.
	 */
	@PreUpdate
	@PrePersist
	public void updateDefaultValues()
	{
		lastUpdated = new Date();
		if(creationDate==null){
			creationDate = new Date();
		}
		if(description!=null && description.length()>1000){
			description = description.substring(0, 999);
		}
	}

	/**
	 * Retorna el codi hash de l'objecte construit a partir de les seves dades significatives.
	 * En aquest cas es tindrà en compte la url
	 * @return int - Codi hash generat a partir de les dades de l'objecte
	 */
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(getClass()!=obj.getClass()){
			return false;
		}
		
		Link other = (Link)obj;
		/* Url */
		if(url==null){
			if(other.url!=null){
				return false;
			}
		} 
		else if(!url.equals(other.url)){
			return false;
		}
		
		return true;
	}
}
