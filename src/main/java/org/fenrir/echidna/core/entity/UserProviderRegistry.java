package org.fenrir.echidna.core.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120623
 */
@Entity
public class UserProviderRegistry 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Version
	private Long version;
	@ManyToOne
	@JoinColumn(name="ECHIDNA_USER", nullable=false)
	@NotNull
	private User user;
	@ManyToOne
	@JoinColumn(name="REGISTRY", nullable=false)
	@NotNull	
	private ProviderRegistry registry;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated;
	
	public Long getId() 
	{
		return id;
	}
	
	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public Long getVersion()
	{
		return version;
	}
	
	public void setVersion(Long version)
	{
		this.version = version;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public void setUser(User user) 
	{
		this.user = user;
	}
	
	public ProviderRegistry getRegistry()
	{
		return registry;
	}
	
	public void setRegistry(ProviderRegistry registry)
	{
		this.registry = registry;
	}
	
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	/**
	 * Callback executat abans de realitzar l'inserció / actualització del registre. 
	 * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
	 */
	@PreUpdate
	@PrePersist
	public void updateTimestamps()
	{
		lastUpdated = new Date();
	}

	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result	+ ((lastUpdated == null) ? 0 : lastUpdated.hashCode());
		result = prime * result	+ ((registry == null) ? 0 : registry.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(getClass()!=obj.getClass()){
			return false;
		}
		
		UserProviderRegistry other = (UserProviderRegistry)obj;
		/* Id */
		if(id==null){
			if(other.id!=null){
				return false;
			}
		} 
		else if(!id.equals(other.id)){
			return false;
		}
		/* User */
		if(user==null){
			if(other.user!=null){
				return false;
			}
		} 
		else if(!user.equals(other.user)){
			return false;
		}
		/* Registry */
		if(registry==null){
			if(other.registry!=null){
				return false;
			}
		} 
		else if(!registry.equals(other.registry)){
			return false;
		}
		/* Last updated */
		if(lastUpdated==null){
			if(other.lastUpdated!=null){
				return false;
			}
		} 
		else if(!lastUpdated.equals(other.lastUpdated)){
			return false;
		}
		
		return true;
	}
}
