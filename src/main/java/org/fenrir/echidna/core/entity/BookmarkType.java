package org.fenrir.echidna.core.entity;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120721
 */
public enum BookmarkType 
{
	artist,
	album
}
