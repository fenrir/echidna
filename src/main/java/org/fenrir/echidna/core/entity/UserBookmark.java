package org.fenrir.echidna.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120909
 */
@Entity
public class UserBookmark 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Version
	private Long version;
	@ManyToOne
	@JoinColumn(name="ECHIDNA_USER", nullable=false)
	@NotNull
	private User user;
	@NotNull
	@Column(nullable=false)
	private Long entityId;
	// Es persisteix l'atribut com a String en comptes de com a valor numèric (EnumType.ORDINAL)
	@Enumerated(EnumType.STRING)
	private BookmarkType type;
	@Temporal(TemporalType.DATE)
	private Date creationDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated;
	
	public Long getId() 
	{
		return id;
	}
	
	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public Long getVersion()
	{
		return version;
	}
	
	public void setVersion(Long version)
	{
		this.version = version;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public void setUser(User user) 
	{
		this.user = user;
	}
	
	public Long getEntityId()
	{
		return entityId;
	}
	
	public void setEntityId(Long entityId)
	{
		this.entityId = entityId;
	}
	
	public BookmarkType getType()
	{
		return type;
	}
	
	public void setType(BookmarkType type)
	{
		this.type = type;
	}
	
	public Date getCreationDate()
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}
	
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	/**
	 * Callback executat abans de realitzar l'inserció / actualització del registre. 
	 * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
	 */
	@PreUpdate
	@PrePersist
	public void updateDefaultValues()
	{
		lastUpdated = new Date();
		if(creationDate==null){
			creationDate = new Date();
		}
	}

	/**
	 * Retorna el codi hash de l'objecte construit a partir de les seves dades significatives.
	 * En aquest cas es tindran en compte l'ID, l'usuari, el tipus de marcador i el codi de l'entitat
	 * associada de l'objecte.
	 * @return int - Codi hash generat a partir de les dades de l'objecte
	 */
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());		
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		
		return result;
	}

	/**
	 * Determina si dos objectes són iguals seguint les regles especificades a la definició
	 * del mètode a {@link Object#equals(Object)}
	 * Dos objectes de tipus UserBookmark seran iguals si es compleix que les propietats 
	 * ID, USER, TYPE i ENTITY_ID són iguals.
	 * @param obj Object - Objecte a comparar amb l'actual
	 * @return boolean - true si els dos objectes són iguals d'acord amb les regles definides
	 * 					 false altrament.
	 */
	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(getClass()!=obj.getClass()){
			return false;
		}
		
		UserBookmark other = (UserBookmark)obj;
		/* Id */
		if(id==null){
			if(other.id!=null){
				return false;
			}
		} 
		else if(!id.equals(other.id)){
			return false;
		}
		/* User */
		if(user==null){
			if(other.user!=null){
				return false;
			}
		} 
		else if(!user.equals(other.user)){
			return false;
		}
		/* Entity id */
		if(entityId==null){
			if(other.entityId!=null){
				return false;
			}
		} 
		else if(!entityId.equals(other.entityId)){
			return false;
		}
		/* Type */
		if(type==null){
			if(other.type != null){
				return false;
			}
		} 
		else if(!type.equals(other.type)){
			return false;
		}
		
		return true;
	}
}
