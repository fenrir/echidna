package org.fenrir.echidna.core.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120820
 */
@Entity
public class EntityRelationship 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Version
	private Long version;
	private Long entity1;
	private Long entity2;
	// Es persisteix l'atribut com a String en comptes de com a valor numèric (EnumType.ORDINAL)
	@Enumerated(EnumType.STRING)
	private EntityRelationshipType type;
	private Long score;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated;
	
	public Long getId() 
	{
		return id;
	}
	
	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public Long getVersion()
	{
		return version;
	}
	
	public void setVersion(Long version)
	{
		this.version = version;
	}
	
	public Long getEntity1()
	{
		return entity1;
	}
	
	public void setEntity1(Long entity1)
	{
		this.entity1 = entity1;
	}
	
	public Long getEntity2()
	{
		return entity2;
	}
	
	public void setEntity2(Long entity2)
	{
		this.entity2 = entity2;
	}
	
	public EntityRelationshipType getType()
	{
		return type;
	}
	
	public void setType(EntityRelationshipType type)
	{
		this.type = type;
	}
	
	public Long getScore()
	{
		return score;
	}
	
	public void setScore(Long score)
	{
		this.score = score;
	}
	
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	/**
	 * Callback executat abans de realitzar l'inserció / actualització del registre. 
	 * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
	 */
	@PreUpdate
	@PrePersist
	public void updateDefaultValues()
	{
		lastUpdated = new Date();
		if(score==null){
			score = 0L;
		}
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((entity1 == null) ? 0 : entity1.hashCode());
		result = prime * result + ((entity2 == null) ? 0 : entity2.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((score == null) ? 0 : score.hashCode());
		result = prime * result	+ ((lastUpdated == null) ? 0 : lastUpdated.hashCode());
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(getClass()!=obj.getClass()){
			return false;
		}
		
		EntityRelationship other = (EntityRelationship)obj;
		/* Id */
		if(id==null){
			if(other.id!=null){
				return false;
			}
		} 
		else if(!id.equals(other.id)){
			return false;
		}
		/* Entity 1 */
		if(entity1==null){
			if(other.entity1!=null){
				return false;
			}
		} 
		else if(!entity1.equals(other.entity1)){
			return false;
		}
		/* Entity 2 */
		if(entity2==null){
			if(other.entity2!=null){
				return false;
			}
		} 
		else if(!entity2.equals(other.entity2)){
			return false;
		}
		/* Type */
		if(type==null){
			if(other.type!=null){
				return false;
			}
		} 
		else if(!type.equals(other.type)){
			return false;
		}
		/* Score */
		if(score==null){
			if(other.score!=null){
				return false;
			}
		}
		else if(!score.equals(other.score)){
			return false;
		}
		/* Last updated */
		if(lastUpdated==null){
			if(other.lastUpdated!=null){
				return false;
			}
		} 
		else if(!lastUpdated.equals(other.lastUpdated)){
			return false;
		}
		
		return true;
	}
}
