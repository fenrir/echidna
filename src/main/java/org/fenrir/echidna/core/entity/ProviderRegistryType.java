package org.fenrir.echidna.core.entity;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120528
 */
public enum ProviderRegistryType 
{
	artist,
	album,
	song,	
	link
}
