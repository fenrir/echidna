package org.fenrir.echidna.core.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120820
 */
@Entity
public class Song 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Version
	private Long version;
	private String name;
	@ManyToOne
	@JoinColumn(name="ALBUM")
	private Album album;
	private Long length;
	@Column(name="ALBUM_POSITION")
	private Integer order;
	@Temporal(TemporalType.DATE)
	private Date creationDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated;
	
	public Long getId() 
	{
		return id;
	}
	
	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public Long getVersion()
	{
		return version;
	}
	
	public void setVersion(Long version)
	{
		this.version = version;
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}

	public Album getAlbum()
	{
		return album;
	}
	
	public void setAlbum(Album album)
	{
		this.album = album;
	}
	
	public Long getLength()
	{
		return length;
	}
	
	public void setLength(Long length)
	{
		this.length = length;
	}
	
	public Integer getOrder()
	{
		return order;
	}
	
	public void setOrder(Integer order)
	{
		this.order = order;
	}
	
	public Date getCreationDate()
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}
	
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	/**
	 * Callback executat abans de realitzar l'inserció / actualització del registre. 
	 * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
	 */
	@PreUpdate
	@PrePersist
	public void updateDefaultValues()
	{
		lastUpdated = new Date();
		if(creationDate==null){
			creationDate = new Date();
		}
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((album == null) ? 0 : album.hashCode());
		result = prime * result + ((length == null) ? 0 : length.hashCode());
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result	+ ((creationDate == null) ? 0 : creationDate.hashCode());
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(getClass()!=obj.getClass()){
			return false;
		}
		
		Song other = (Song) obj;
		/* Id */
		if(id==null){
			if(other.id!=null){
				return false;
			}
		} 
		else if(!id.equals(other.id)){
			return false;
		}
		/* Name */
		if(name==null){
			if(other.name != null){
				return false;
			}
		} 
		else if(!name.equals(other.name)){
			return false;
		}
		/* Album */
		if(album==null){
			if(other.album!=null){
				return false;
			}
		} 
		else if(!album.equals(other.album)){
			return false;
		}
		/* Length */
		if(length==null){
			if(other.length!=null){
				return false;
			}
		} 
		else if(!length.equals(other.length)){
			return false;
		}
		/* Order */
		if(order==null){
			if(other.order!=null){
				return false;
			}
		} 
		else if(!order.equals(other.order)){
			return false;
		}
		/* Creation date */
		if(creationDate==null){
			if(other.creationDate!=null){
				return false;
			}
		}
		else if(!creationDate.equals(other.creationDate)){
			return false;
		}
		
		return true;
	}
}
