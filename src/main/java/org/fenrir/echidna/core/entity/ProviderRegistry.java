package org.fenrir.echidna.core.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120817
 */
@Entity
@Table(uniqueConstraints=@UniqueConstraint(columnNames = {"type", "provider", "providerId"})) 
public class ProviderRegistry 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Version
	private Long version;
	@NotNull
	@Column(nullable=false)
	private Long entityId;
	@NotNull
	@Column(nullable=false)
	// Es persisteix l'atribut com a String en comptes de com a valor numèric (EnumType.ORDINAL)
	@Enumerated(EnumType.STRING)
	private ProviderRegistryType type;
	@NotNull
	@Column(nullable=false)
	private String provider;
	@NotNull
	@Column(nullable=false)
	private String providerId;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated;
	
	public Long getId() 
	{
		return id;
	}
	
	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public Long getVersion()
	{
		return version;
	}
	
	public void setVersion(Long version)
	{
		this.version = version;
	}
	
	public Long getEntityId()
	{
		return entityId;
	}
	
	public void setEntityId(Long entityId)
	{
		this.entityId = entityId;
	}
	
	public ProviderRegistryType getType()
	{
		return type;
	}
	
	public void setType(ProviderRegistryType type)
	{
		this.type = type;
	}
	
	public String getProvider()
	{
		return provider;
	}
	
	public void setProvider(String provider)
	{
		this.provider = provider;
	}
	
	public String getProviderId()
	{
		return providerId;
	}
	
	public void setProviderId(String providerId)
	{
		this.providerId = providerId;
	}
	
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	/**
	 * Callback executat abans de realitzar l'inserció / actualització del registre. 
	 * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
	 */
	@PreUpdate
	@PrePersist
	public void updateDefaultValues()
	{
		lastUpdated = new Date();
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((provider == null) ? 0 : provider.hashCode());
		result = prime * result + ((providerId == null) ? 0 : providerId.hashCode());
		result = prime * result	+ ((lastUpdated == null) ? 0 : lastUpdated.hashCode());
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(getClass()!=obj.getClass()){
			return false;
		}
		
		ProviderRegistry other = (ProviderRegistry)obj;
		/* Id */
		if(id==null){
			if(other.id!=null){
				return false;
			}
		} 
		else if(!id.equals(other.id)){
			return false;
		}
		/* Entuty id */
		if(entityId==null){
			if(other.entityId!=null){
				return false;
			}
		} 
		else if(!entityId.equals(other.entityId)){
			return false;
		}
		/* Type */
		if(type==null){
			if(other.type != null){
				return false;
			}
		} 
		else if(!type.equals(other.type)){
			return false;
		}
		/* Provider */
		if(provider==null){
			if(other.provider!=null){
				return false;
			}
		} 
		else if(!provider.equals(other.provider)){
			return false;
		}
		/* Provider ID */
		if(providerId==null){
			if(other.providerId!=null){
				return false;
			}
		} 
		else if(!providerId.equals(other.providerId)){
			return false;
		}
		/* Last updated */
		if(lastUpdated==null){
			if(other.lastUpdated!=null){
				return false;
			}
		} 
		else if(!lastUpdated.equals(other.lastUpdated)){
			return false;
		}
		
		return true;
	}
}
