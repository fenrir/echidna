package org.fenrir.echidna.core.entity;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120630
 */
public enum LinkedEntityStatus 
{
	empty,
	incomplete,
	complete
}
