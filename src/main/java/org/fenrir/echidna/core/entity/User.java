package org.fenrir.echidna.core.entity;

import java.util.Date;
import java.util.Properties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.fenrir.echidna.core.CoreConstants;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120706
 */
@Entity
@Table(name="ECHIDNA_USER")
public class User 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Version
	private Long version;
	@NotNull
	@Column(unique=true, nullable=false)
	private String name;
	@NotNull
	@Column(nullable=false)
	private String password;
	@NotNull
	@Column(nullable=false)
	private String language;
		
	@Temporal(TemporalType.DATE)
	private Date creationDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated;
	
	public Long getId() 
	{
		return id;
	}
	
	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public Long getVersion()
	{
		return version;
	}
	
	public void setVersion(Long version)
	{
		this.version = version;
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public String getPassword()
	{
		return password;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}
	
	public String getLanguage()
	{
		return language;
	}
	
	public void setLanguage(String language)
	{
		this.language = language;
	}
	
	public Date getCreationDate()
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}
	
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	/**
	 * Callback executat abans de realitzar l'inserció / actualització del registre. 
	 * Servirà per actualitzar la data de creació en cas d'inserció, el timestamp en cas d'actualització
	 * i l'idioma de l'usuari en cas de no estar especificat.
	 */
	@PreUpdate
	@PrePersist
	public void updateDefaultValues()
	{
		lastUpdated = new Date();
		if(creationDate==null){
			creationDate = new Date();
		}
		if(language==null){
			Properties applicationProperties = (Properties)EchidnaServiceLocator.getInstance()
					.getInstance(Properties.class, CoreConstants.NAMED_INJECTION_APPLICATION_PROPERTIES);
			language = applicationProperties.getProperty(CoreConstants.CONFIGURATION_USER_LANGUAGE);
		}
	}

	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result	+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((language == null) ? 0 : language.hashCode());
		result = prime * result	+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((lastUpdated == null) ? 0 : lastUpdated.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}		
		if(getClass()!=obj.getClass()){
			return false;
		}
		
		User other = (User)obj;
		/* Id */
		if(id==null){
			if(other.id!=null){
				return false;
			}
		} 
		else if(!id.equals(other.id)){
			return false;
		}
		/* Name */
		if(name==null){
			if(other.name!=null){
				return false;
			}
		} 
		else if(!name.equals(other.name)){
			return false;
		}
		/* Password */
		if(password==null){
			if(other.password!=null){
				return false;
			}
		} 
		else if(!password.equals(other.password)){
			return false;
		}
		/* Language */
		if(language==null){
			if(other.language!=null){
				return false;
			}
		} 
		else if(!language.equals(other.language)){
			return false;
		}
		/* Creation date */
		if(creationDate==null){
			if(other.creationDate!=null){
				return false;
			}
		}
		else if(!creationDate.equals(other.creationDate)){
			return false;
		}
		/* Last updated */
		if(lastUpdated==null){
			if(other.lastUpdated!=null){
				return false;
			}
		}
		else if(!lastUpdated.equals(other.lastUpdated)){
			return false;
		}
		
		return true;
	}
}
