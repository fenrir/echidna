package org.fenrir.echidna.core.entity;

/**
 * Classe que conté els tipus de relacions entre entitats de l'aplicació que s'emmagatzemen a la taula ENTITY_RELATIONSHIP
 * @author Antonio Archilla Nava
 * @version v0.0.20120820
 */
public enum EntityRelationshipType 
{
	/**
	 * Relació entre cançons (Entity1) i links (Entity2)
	 */
	songLinks
}
