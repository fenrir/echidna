package org.fenrir.echidna.core.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120527
 */
@Entity
public class PlaylistSong 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Version
	private Long version;
	@ManyToOne
	@JoinColumn(name="PLAYLIST")
	private Playlist playlist;
	@ManyToOne
	@JoinColumn(name="SONG")
	private Song song;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated;
	
	public Long getId() 
	{
		return id;
	}
	
	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public Long getVersion()
	{
		return version;
	}
	
	public void setVersion(Long version)
	{
		this.version = version;
	}
	
	public Playlist getPlaylist()
	{
		return playlist;
	}
	
	public void setPlaylist(Playlist playlist)
	{
		this.playlist = playlist;
	}
	
	public Song getSong()
	{
		return song;
	}
	
	public void setSong(Song song)
	{
		this.song = song;
	}
	
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	/**
	 * Callback executat abans de realitzar l'inserció / actualització del registre. 
	 * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
	 */
	@PreUpdate
	@PrePersist
	public void updateTimestamps()
	{
		lastUpdated = new Date();
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((playlist == null) ? 0 : playlist.hashCode());
		result = prime * result + ((song == null) ? 0 : song.hashCode());
		result = prime * result	+ ((lastUpdated == null) ? 0 : lastUpdated.hashCode());
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(getClass()!=obj.getClass()){
			return false;
		}
		
		PlaylistSong other = (PlaylistSong) obj;
		/* Id */
		if(id==null){
			if(other.id!=null){
				return false;
			}
		} 
		else if(!id.equals(other.id)){
			return false;
		}
		/* Playlist */
		if(playlist==null){
			if(other.playlist != null){
				return false;
			}
		} 
		else if(!playlist.equals(other.playlist)){
			return false;
		}
		/* Song */
		if(song==null){
			if(other.song!=null){
				return false;
			}
		} 
		else if(!song.equals(other.song)){
			return false;
		}
		/* Last updated */
		if(lastUpdated==null){
			if(other.lastUpdated!=null){
				return false;
			}
		} 
		else if(!lastUpdated.equals(other.lastUpdated)){
			return false;
		}
		
		return true;
	}
}
