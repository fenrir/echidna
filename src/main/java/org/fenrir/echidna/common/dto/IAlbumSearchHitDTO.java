package org.fenrir.echidna.common.dto;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120623
 */
public interface IAlbumSearchHitDTO 
{
	public String getProvider();
	public String getId(); 
	public String getName(); 
	public String getDescription(); 
	public String getFirstReleaseDate();
	public String getFirstReleaseYear();
	public String getArtistSearchId();
	public String getArtistName();
}
