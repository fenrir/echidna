package org.fenrir.echidna.common.dto;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120703
 */
public interface IRecordingSearchHitDTO 
{
	public String getProvider();
	public String getId();
	public String getTitle();
	public String getLength();
	public Long getLengthMillis();
	public String getArtistSearchId();
	public String getArtistName();
	public String getAlbumSearchId();
	public String getAlbumName();
	public Integer getOrder();
}
