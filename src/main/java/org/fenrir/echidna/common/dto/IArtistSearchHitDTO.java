package org.fenrir.echidna.common.dto;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120623
 */
public interface IArtistSearchHitDTO 
{
	public String getProvider();
	public String getId();
	public String getName(); 
	public String getDescription();
}
