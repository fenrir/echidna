package org.fenrir.echidna.common.dto;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120911
 */
public interface ILinkDTO 
{
	public String getId();
	public String getProvider(); 
	public String getLinkSearchId();
	public String getUrl();
	public String getTitle();
	public String getDescription();
	public String getAuthor();
	
	public String getThumbnailUrl();
}