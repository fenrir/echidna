package org.fenrir.echidna.common.dto;

import java.util.Collections;
import java.util.List;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120319
 */
public class GenericResultSetDTO<T> 
{	
	private List<T> resultList;
	private int totalCount;
	private int offset;
	
	private GenericResultSetDTO()
	{
		resultList = Collections.emptyList();
		totalCount = 0;
		offset = 0;
	}
	
	public GenericResultSetDTO(List<T> resultList, int totalCount, int offset)
	{
		this.resultList = resultList;
		this.totalCount = totalCount;
		this.offset = offset;
	}
	
	public static <T> GenericResultSetDTO<T> emptyResultSet()
	{
		GenericResultSetDTO<T> resultSet = new GenericResultSetDTO<T>();
		
		return resultSet;
	}
	
	public List<T> getResultList()
	{
		return resultList;
	}
	
	public int getTotalCount()
	{
		return totalCount;
	}	
	
	public int getOffset()
	{
		return offset;
	}	
	
	public int size()
	{
		return resultList.size();
	}	
}