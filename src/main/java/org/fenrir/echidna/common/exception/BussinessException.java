package org.fenrir.echidna.common.exception;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120319
 */
@SuppressWarnings("serial")
public class BussinessException extends Exception 
{
	public BussinessException() 
	{
		super();	
	}

	public BussinessException(String message, Throwable cause) 
	{
		super(message, cause);
	}

	public BussinessException(String message) 
	{
		super(message);	
	}

	public BussinessException(Throwable cause) 
	{
		super(cause);	
	}	
}
