package org.fenrir.echidna.common.dao;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * TODO v1.0 Javadoc
 * TODO Posar com a constants les cadenes de connexió al webservice i el user-agent
 * @author Antonio Archilla Nava
 * @version v0.0.20120728
 */
public abstract class AbstractWebserviceDAO 
{
	private Log log = LogFactory.getLog(AbstractWebserviceDAO.class);
	
	protected Service createService(QName serviceQName, QName portQName, String endpointBaseAddress, String requestPath)
	{
		Service service = Service.create(serviceQName);
		service.addPort(portQName, HTTPBinding.HTTP_BINDING, endpointBaseAddress + requestPath);
		
		return service;
	}
	
	protected Source invokeDispatch(Service service, QName portQName, Map<String, String> params)
	{
		StringBuilder queryString = new StringBuilder();
		for(String key:params.keySet()){
			String value = params.get(key);
			if(queryString.length()!=0){
				queryString.append("&");
			}
			queryString.append(key).append("=").append(value);
		}
		
		return invokeDispatch(service, portQName, queryString.toString());
	}
	
	protected Source invokeDispatch(Service service, QName portQName, String query)
	{
		Dispatch<Source> d = service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

        Map<String, Object> requestContext = d.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, new String("GET"));
        if(StringUtils.isNotBlank(query)){
        	requestContext.put(MessageContext.QUERY_STRING, query);
        }
        Map<String, List<String>> headers = new HashMap<String, List<String>>();
        headers.put("user-agent", Arrays.asList("echidna/0.1"));
        requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        
        return d.invoke(null);
	}
	
	@SuppressWarnings("unchecked")
	protected <T> T buildResult(Source source, Class<T> resultClass) throws TransformerException, JAXBException
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
        StreamResult result = new StreamResult(bos);
        Transformer trans = TransformerFactory.newInstance().newTransformer();
        trans.transform(source, result);
     	
        Unmarshaller unmarshaller = null;
        InputStreamReader reader = null;
        try{
	        reader = new InputStreamReader(new ByteArrayInputStream(bos.toByteArray()), "UTF-8");
	        JAXBContext context = JAXBContext.newInstance(resultClass);
			unmarshaller = context.createUnmarshaller();
        }
        catch(UnsupportedEncodingException e){
        	log.error("[AbstractWebserviceDAO::buildEntity] Charset no suportat: " + e.getMessage(), e);
        }
		
		if(log.isDebugEnabled()){
			log.debug("[AbstractWebserviceDAO::buildEntity] Resposta WS: " + bos.toString());
		}
		
		return (T)unmarshaller.unmarshal(reader);				        	
	}
}
