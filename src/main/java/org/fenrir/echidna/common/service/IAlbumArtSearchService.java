package org.fenrir.echidna.common.service;

import java.util.List;
import org.fenrir.echidna.common.exception.BussinessException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120407
 */
public interface IAlbumArtSearchService 
{
	public List<String> findAlbumFrontCovers(String artist, String album) throws BussinessException;
	public String downloadImage(String url) throws BussinessException;
}
