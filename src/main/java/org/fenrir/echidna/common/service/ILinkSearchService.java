package org.fenrir.echidna.common.service;

import org.fenrir.echidna.common.dto.GenericResultSetDTO;
import org.fenrir.echidna.common.dto.ILinkDTO;
import org.fenrir.echidna.common.exception.BussinessException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120817
 */
public interface ILinkSearchService 
{
	public GenericResultSetDTO<ILinkDTO> findLinks(String term) throws BussinessException;
	public GenericResultSetDTO<ILinkDTO> findSongLinks(String groupName, String songName) throws BussinessException;
}