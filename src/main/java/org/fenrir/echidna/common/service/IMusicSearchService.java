package org.fenrir.echidna.common.service;

import org.fenrir.echidna.common.dto.IAlbumSearchHitDTO;
import org.fenrir.echidna.common.dto.IArtistSearchHitDTO;
import org.fenrir.echidna.common.dto.GenericResultSetDTO;
import org.fenrir.echidna.common.dto.IRecordingSearchHitDTO;
import org.fenrir.echidna.common.exception.BussinessException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120623
 */
public interface IMusicSearchService 
{
	public GenericResultSetDTO<IArtistSearchHitDTO> findArtistsByName(String name) throws BussinessException;
	public GenericResultSetDTO<IAlbumSearchHitDTO> findAlbumsByName(String name) throws BussinessException;
	public GenericResultSetDTO<IAlbumSearchHitDTO> findAlbumsByProviderArtistId(String id) throws BussinessException;
	public IRecordingSearchHitDTO findRecordingByProviderId(String id) throws BussinessException;
	public GenericResultSetDTO<IRecordingSearchHitDTO> findRecordingsByName(String name) throws BussinessException;
	public GenericResultSetDTO<IRecordingSearchHitDTO> findRecordingsByProviderAlbumId(String id) throws BussinessException;
}