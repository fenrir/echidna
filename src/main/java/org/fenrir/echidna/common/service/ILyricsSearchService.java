package org.fenrir.echidna.common.service;

import org.fenrir.echidna.common.exception.BussinessException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120728
 */
public interface ILyricsSearchService 
{
	public String findLyrics(String artistName, String songName) throws BussinessException;
}
