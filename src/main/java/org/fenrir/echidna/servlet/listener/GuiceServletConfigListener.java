package org.fenrir.echidna.servlet.listener;

import java.util.HashMap;
import java.util.Map;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.persist.PersistFilter;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;
import com.google.inject.servlet.ServletScopes;
import com.vaadin.Application;
import org.fenrir.echidna.EchidnaApplication;
import org.fenrir.echidna.servlet.GuiceApplicationServlet;
import org.fenrir.echidna.module.EchidnaModule;
import org.fenrir.echidna.core.service.EchidnaServiceLocator;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120406
 */
public class GuiceServletConfigListener extends GuiceServletContextListener 
{
    @Override
    protected Injector getInjector() 
    {
        ServletModule servletModule = new ServletModule() 
        {
            @Override
            protected void configureServlets() 
            {
            	Map<String, String> params = new HashMap<String, String>();
            	params.put("widgetset", "org.fenrir.echidna.widgetset.EchidnaWidgetset");
                serve("/*").with(GuiceApplicationServlet.class, params);
                // session-per-http-request
                filter("/*").through(PersistFilter.class);

                bind(Application.class).to(EchidnaApplication.class).in(ServletScopes.SESSION);
            }
        };

        Injector injector = Guice.createInjector(servletModule, new JpaPersistModule("echidnaJpa"), new EchidnaModule());
        // Es fa accessible l'injector
        EchidnaServiceLocator.getInstance().setInjector(injector);

        return injector;
    }
}