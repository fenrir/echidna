package org.fenrir.echidna.servlet.listener;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.xml.DOMConfigurator;
import org.fenrir.echidna.core.CoreConstants;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120502
 */
public class EchidnaServletListener implements ServletContextListener
{
	public void contextInitialized(ServletContextEvent event) 
	{
		try{
			File log4jFile = new File(getClass().getResource("/org/fenrir/echidna/conf/log4j.xml").toURI());
			DOMConfigurator.configure(log4jFile.getAbsolutePath());
		}
		catch(Exception e){
			// Ha fallat la configuració del log
			System.err.println("ERROR: " + e.getMessage());
		}
		
		Log log = LogFactory.getLog(EchidnaServletListener.class);
		
		/* Càrrega de la configuració */
		Properties properties = new Properties();
		try{
			FileInputStream fis = new FileInputStream(new File(getClass().getResource("/org/fenrir/echidna/conf/configuration.properties").toURI()));
			properties.load(fis);
			
			fis.close();
		}		
		catch(IOException e){
			// No s'hauria de donar mai
			log.fatal("[EchidnaServletListener::contextInitialized] Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
			throw new RuntimeException("Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
		}
		catch(URISyntaxException e){
			// No s'hauria de donar mai
			log.fatal("[EchidnaServletListener::contextInitialized] Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
			throw new RuntimeException("Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
		}
		
		/* Configuració del proxy */
		String httpProxyHost = properties.getProperty(CoreConstants.CONFIGURATION_HTTP_PROXY_HOST);
		if(StringUtils.isNotBlank(httpProxyHost)){
			String httpProxyPort = StringUtils.defaultString(properties.getProperty(CoreConstants.CONFIGURATION_HTTP_PROXY_PORT), CoreConstants.HTTP_PROXY_PORT_DEFAULT);
			System.setProperty("http.proxyHost", httpProxyHost);
			System.setProperty("http.proxyPort", httpProxyPort);
		}
		String httpsProxyHost = properties.getProperty(CoreConstants.CONFIGURATION_HTTPS_PROXY_HOST);
		if(StringUtils.isNotBlank(httpsProxyHost)){
			String httpsProxyPort = StringUtils.defaultString(properties.getProperty(CoreConstants.CONFIGURATION_HTTPS_PROXY_PORT), CoreConstants.HTTPS_PROXY_PORT_DEFAULT);
			System.setProperty("https.proxyHost", httpsProxyHost);
			System.setProperty("https.proxyPort", httpsProxyPort);
		}
		
		/* Es creen els directoris necessaris en el workspace */
		String workspaceDir = properties.getProperty(CoreConstants.CONFIGURATION_WORKSPACE_FOLDER);
		// Es crea el directori que emmagatzemarà les imatges dels covers
		File outputDir = new File(workspaceDir + "/" + CoreConstants.WORKSPACE_IMAGE_FOLDER);
		if(!outputDir.exists()){
			try{
				FileUtils.forceMkdir(outputDir);
			}
			catch(IOException e){
				throw new RuntimeException("Error creant el directori d'imatges: " + e.getMessage(), e);
			}
		}
		
		log.debug("[EchidnaServletListener::contextInitialized] Inicialitzat context");
	}
	
	public void contextDestroyed(ServletContextEvent event) 
	{
		
	}
}
