package org.fenrir.echidna.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.AbstractApplicationServlet;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120406
 */
@Singleton
public class GuiceApplicationServlet extends AbstractApplicationServlet 
{
	private static final long serialVersionUID = 8172272414689584190L;
	
	protected final Provider<Application> applicationProvider;

    @Inject
    public GuiceApplicationServlet(Provider<Application> applicationProvider) 
    {
        this.applicationProvider = applicationProvider;
    }

    @Override
    protected Class getApplicationClass() throws ClassNotFoundException 
    {
        return Application.class;
    }

    @Override
    protected Application getNewApplication(HttpServletRequest request) throws ServletException 
    {
        return applicationProvider.get();
    }

}