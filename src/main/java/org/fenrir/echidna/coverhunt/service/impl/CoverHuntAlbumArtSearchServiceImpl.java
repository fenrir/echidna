package org.fenrir.echidna.coverhunt.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.fenrir.echidna.common.exception.BussinessException;
import org.fenrir.echidna.common.service.IAlbumArtSearchService;
import org.fenrir.echidna.core.CoreConstants;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120407
 */
public class CoverHuntAlbumArtSearchServiceImpl implements IAlbumArtSearchService 
{
	private static final String SCHEME = "http";
	private static final String HOST = "www.coverhunt.com";
	private static final String PATH_INDEX = "/index.php";
	private static final String REQUEST_PARAMETER_QUERY = "query";
	private static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13";
	private static final String DEFAULT_ENCODING = "UTF-8";
	private static final Pattern COVER_IMAGE_URL_EXTRACTION_PATTERN = Pattern.compile("<td><a href=\"/go/\\w+\"><img src=\"(.+)\"></a></td>");
	private static final Pattern COVER_IMAGE_URL_PATTERN = Pattern.compile(".+(\\..+)");
	private static final DecimalFormat COMMA_FORMAT_NO_PRECISSION = new DecimalFormat("###,###");
	private static final int BUFFER_SIZE = 2048;
	
	private final Log log = LogFactory.getLog(CoverHuntAlbumArtSearchServiceImpl.class);

	@Inject
	@Named(CoreConstants.NAMED_INJECTION_APPLICATION_PROPERTIES)
	private Properties applicationProperties;
	
	public void setApplicationProperties(Properties applicationProperties)
	{
		this.applicationProperties = applicationProperties;
	}
	
	public List<String> findAlbumFrontCovers(String artist, String album) throws BussinessException
	{
		List<String> urls = new ArrayList<String>();
		
		Map<String, String> params = new HashMap<String, String>();
		params.put(REQUEST_PARAMETER_QUERY, artist + "+" + album);
		HttpEntity entity = doGetRequest(PATH_INDEX, params);
		try{
			String pageCode = EntityUtils.toString(entity, DEFAULT_ENCODING);
			if(pageCode!=null && pageCode.length()>0){
				Matcher matcher = COVER_IMAGE_URL_EXTRACTION_PATTERN.matcher(pageCode);
				while(matcher.find()){
					/* Es recupera la llista a partir del capturing group 1 i
					 * El 0 seria tota la regexp
					 */
					urls.add(matcher.group(1));
				}
			}
			else{
				throw new BussinessException("No s'ha pogut rebre contingut del servidor per " + artist + "/" + album);
			}
		}
		catch(IOException e){
			log.error("[CoverHuntAlbumArtSearchServiceImpl::findAlbumFrontCovers] Error buscant coberta per " + artist + "/" + album + ": " + e.getMessage(), e);
			throw new BussinessException("Error buscant coberta per " + artist + "/" + album + ": " + e.getMessage(), e);
		}
		
		return urls;
	}

	public String downloadImage(String url) throws BussinessException
	{
		String workspaceDir = applicationProperties.getProperty(CoreConstants.CONFIGURATION_WORKSPACE_FOLDER);
		File outputDir = new File(workspaceDir + "/" + CoreConstants.WORKSPACE_IMAGE_FOLDER);

		String extension;
		Matcher matcher = COVER_IMAGE_URL_PATTERN.matcher(url);
		if(matcher.find()){
			/* Es recupera l'extensió a partir del capturing group 1
			 * El 0 seria tota la regexp
			 */
			extension = matcher.group(1);
		}
		else{
			extension = ".tmp";
		}
		File outputFile;
		try{
			outputFile = File.createTempFile("img", extension, outputDir);
			URI uri = new URI(url);
			HttpEntity entity = doGetRequest(uri);
			double length = entity.getContentLength();
			// Per si de cas...
			if(length<=0){
				length = 1;
			}
			InputStream inputStream = entity.getContent();
			if(log.isInfoEnabled()){
				log.info("[CoverHuntAlbumArtSearchServiceImpl::downloadImage] Escrivint " + COMMA_FORMAT_NO_PRECISSION.format(length) + " bytes a " + outputFile);
			}
			if(outputFile.exists()){
				outputFile.delete();
			}
			FileOutputStream outstream = new FileOutputStream(outputFile);
			try{
				byte[] buffer = new byte[BUFFER_SIZE];
				int size = -1;
				while((size = inputStream.read(buffer))!=-1){
					outstream.write(buffer, 0, size);
				}
				outstream.flush();
			}
			finally{
				outstream.close();
			}
		}
		catch(URISyntaxException e){
			log.error("[CoverHuntAlbumArtSearchServiceImpl::downloadImage] Error descarregant de la url " + url + ": " + e.getMessage(), e);
			throw new BussinessException("Error descarregant de la url " + url + ": " + e.getMessage(), e);
		}
		catch(IOException e){
			log.error("[CoverHuntAlbumArtSearchServiceImpl::downloadImage] Error descarregant de la url " + url + ": " + e.getMessage(), e);
			throw new BussinessException("Error descarregant de la url " + url + ": " + e.getMessage(), e);
		}

		return CoreConstants.WORKSPACE_IMAGE_FOLDER + "/" + FilenameUtils.getName(outputFile.getAbsolutePath());
	}

	private HttpEntity doGetRequest(String path, Map<String, String> params) throws BussinessException
	{
		List<NameValuePair> qparams = new ArrayList<NameValuePair>();
		for(String key:params.keySet()){
			qparams.add(new BasicNameValuePair(key, params.get(key)));
		}
		try{
			URI uri = URIUtils.createURI(SCHEME, HOST, -1, path, URLEncodedUtils.format(qparams, DEFAULT_ENCODING), null);
			return doGetRequest(uri);
		}
		catch(URISyntaxException e){
			log.error("[CoverHuntAlbumArtSearchServiceImpl::doGetRequest] Error descarregant desde el path " + path + ": " + e.getMessage(), e);
			throw new BussinessException("Error descarregant desde el path " + path + ": " + e.getMessage(), e);
		}
	}
	
	private HttpEntity doGetRequest(URI uri) throws BussinessException
	{
		try{
			CookieStore cookieStore = new BasicCookieStore();
			HttpContext localContext = new BasicHttpContext();
			localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpget = new HttpGet(uri);
			httpget.setHeader("User-Agent", DEFAULT_USER_AGENT);

			HttpResponse response = httpclient.execute(httpget, localContext);
			HttpEntity entity = response.getEntity();
			if(entity!=null && response.getStatusLine().getStatusCode()==200){
				return entity;
			}
			else{
				throw new BussinessException("No s'ha pogut contactar amb el servidor " + uri);
			}
		}
		catch(IOException e){
			log.error("[CoverHuntAlbumArtSearchServiceImpl::doGetRequest] Error descarregant desde la url " + uri + ": " + e.getMessage(), e);
			throw new BussinessException("Error descarregant desde la url " + uri + ": " + e.getMessage(), e);
		}
	}
}
