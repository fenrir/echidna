package org.fenrir.echidna.module;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fenrir.echidna.common.service.IAlbumArtSearchService;
import org.fenrir.echidna.common.service.ILinkSearchService;
import org.fenrir.echidna.common.service.ILyricsSearchService;
import org.fenrir.echidna.common.service.IMusicSearchService;
import org.fenrir.echidna.core.CoreConstants;
import org.fenrir.echidna.core.dao.IAlbumDAO;
import org.fenrir.echidna.core.dao.IArtistDAO;
import org.fenrir.echidna.core.dao.IEntityRelationshipDAO;
import org.fenrir.echidna.core.dao.ILinkDAO;
import org.fenrir.echidna.core.dao.IPlaylistSongDAO;
import org.fenrir.echidna.core.dao.IPlayslistDAO;
import org.fenrir.echidna.core.dao.IProviderRegistryDAO;
import org.fenrir.echidna.core.dao.ISongDAO;
import org.fenrir.echidna.core.dao.IUserBookmarkDAO;
import org.fenrir.echidna.core.dao.IUserDAO;
import org.fenrir.echidna.core.dao.IUserProviderRegistryDAO;
import org.fenrir.echidna.core.dao.impl.AlbumDAOImpl;
import org.fenrir.echidna.core.dao.impl.ArtistDAOImpl;
import org.fenrir.echidna.core.dao.impl.EntityRelationshipDAOImpl;
import org.fenrir.echidna.core.dao.impl.LinkDAOImpl;
import org.fenrir.echidna.core.dao.impl.PlaylistDAOImpl;
import org.fenrir.echidna.core.dao.impl.PlaylistSongDAOImpl;
import org.fenrir.echidna.core.dao.impl.ProviderRegistryDAOImpl;
import org.fenrir.echidna.core.dao.impl.SongDAOImpl;
import org.fenrir.echidna.core.dao.impl.UserBookmarkDAOImpl;
import org.fenrir.echidna.core.dao.impl.UserDAOImpl;
import org.fenrir.echidna.core.dao.impl.UserProviderRegistryDAOImpl;
import org.fenrir.echidna.core.security.IAuthenticator;
import org.fenrir.echidna.core.security.impl.SimpleAuthenticatorImpl;
import org.fenrir.echidna.core.service.IContentAdministrationService;
import org.fenrir.echidna.core.service.IContentSearchService;
import org.fenrir.echidna.core.service.IUserContentAdministrationService;
import org.fenrir.echidna.core.service.IUserAdministrationService;
import org.fenrir.echidna.core.service.impl.ContentAdministrationServiceImpl;
import org.fenrir.echidna.core.service.impl.ContentSearchServiceImpl;
import org.fenrir.echidna.core.service.impl.UserContentAdministrationServiceImpl;
import org.fenrir.echidna.core.service.impl.UserAdministrationServiceImpl;
import org.fenrir.echidna.coverhunt.service.impl.CoverHuntAlbumArtSearchServiceImpl;
import org.fenrir.echidna.lyricwiki.dao.ILyricWikiWSDAO;
import org.fenrir.echidna.lyricwiki.dao.impl.LyricWikiWSDAOImpl;
import org.fenrir.echidna.lyricwiki.service.impl.LyricWikiSearchServiceImpl;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzArtistWSDAO;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzRecordingWSDAO;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzReleaseGroupWSDAO;
import org.fenrir.echidna.musicbrainz.dao.IMusicBrainzReleaseWSDAO;
import org.fenrir.echidna.musicbrainz.dao.impl.MusicBrainzArtistWSDAOImpl;
import org.fenrir.echidna.musicbrainz.dao.impl.MusicBrainzRecordingWSDAOImpl;
import org.fenrir.echidna.musicbrainz.dao.impl.MusicBrainzReleaseGroupWSDAOImpl;
import org.fenrir.echidna.musicbrainz.dao.impl.MusicBrainzReleaseWSDAOImpl;
import org.fenrir.echidna.musicbrainz.service.impl.MusicBrainzWSSearchServiceImpl;
import org.fenrir.echidna.youtube.service.impl.YoutubeSearchServiceImpl;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20120820
 */
public class EchidnaModule extends AbstractModule 
{
	private final Log log = LogFactory.getLog(EchidnaModule.class);
	
	@Override
	protected void configure()
	{
		Properties applicationProperties = loadApplicationProperties();
		bind(Properties.class).annotatedWith(Names.named(CoreConstants.NAMED_INJECTION_APPLICATION_PROPERTIES)).toInstance(applicationProperties);
		
		bind(IUserDAO.class).to(UserDAOImpl.class).in(Singleton.class);
		bind(IPlayslistDAO.class).to(PlaylistDAOImpl.class).in(Singleton.class);
		bind(IArtistDAO.class).to(ArtistDAOImpl.class).in(Singleton.class);
		bind(IAlbumDAO.class).to(AlbumDAOImpl.class).in(Singleton.class);
		bind(ISongDAO.class).to(SongDAOImpl.class).in(Singleton.class);
		bind(ILinkDAO.class).to(LinkDAOImpl.class).in(Singleton.class);
		bind(IPlaylistSongDAO.class).to(PlaylistSongDAOImpl.class).in(Singleton.class);
		bind(IProviderRegistryDAO.class).to(ProviderRegistryDAOImpl.class).in(Singleton.class);
		bind(IEntityRelationshipDAO.class).to(EntityRelationshipDAOImpl.class).in(Singleton.class);
		bind(IUserBookmarkDAO.class).to(UserBookmarkDAOImpl.class).in(Singleton.class);
		bind(IUserProviderRegistryDAO.class).to(UserProviderRegistryDAOImpl.class).in(Singleton.class);
		
		bind(IAuthenticator.class).to(SimpleAuthenticatorImpl.class).in(Singleton.class);
		bind(IUserAdministrationService.class).to(UserAdministrationServiceImpl.class).in(Singleton.class);
		bind(IUserContentAdministrationService.class).to(UserContentAdministrationServiceImpl.class).in(Singleton.class);
		bind(IContentSearchService.class).to(ContentSearchServiceImpl.class).in(Singleton.class);
		bind(IContentAdministrationService.class).to(ContentAdministrationServiceImpl.class).in(Singleton.class);
		
		bind(IMusicBrainzArtistWSDAO.class).to(MusicBrainzArtistWSDAOImpl.class).in(Singleton.class);
		bind(IMusicBrainzReleaseWSDAO.class).to(MusicBrainzReleaseWSDAOImpl.class).in(Singleton.class);
		bind(IMusicBrainzReleaseGroupWSDAO.class).to(MusicBrainzReleaseGroupWSDAOImpl.class).in(Singleton.class);
		bind(IMusicBrainzRecordingWSDAO.class).to(MusicBrainzRecordingWSDAOImpl.class).in(Singleton.class);
		bind(IMusicSearchService.class).to(MusicBrainzWSSearchServiceImpl.class).in(Singleton.class);
		
		bind(ILyricWikiWSDAO.class).to(LyricWikiWSDAOImpl.class).in(Singleton.class);
		bind(ILyricsSearchService.class).to(LyricWikiSearchServiceImpl.class).in(Singleton.class);
		
		bind(IAlbumArtSearchService.class).to(CoverHuntAlbumArtSearchServiceImpl.class).in(Singleton.class);
		
		bind(ILinkSearchService.class).to(YoutubeSearchServiceImpl.class).in(Singleton.class);
	}
	
	private Properties loadApplicationProperties()
	{
		Properties properties = new Properties();
		try{
			FileInputStream fis = new FileInputStream(new File(getClass().getResource("/org/fenrir/echidna/conf/configuration.properties").toURI()));
			properties.load(fis);
			
			fis.close();
		}		
		catch(IOException e){
			// No s'hauria de donar mai
			log.fatal("[EchidnaServletListener::contextInitialized] Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
			throw new RuntimeException("Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
		}
		catch(URISyntaxException e){
			// No s'hauria de donar mai
			log.fatal("[EchidnaServletListener::contextInitialized] Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
			throw new RuntimeException("Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
		}
		
		return properties;
	}
}
